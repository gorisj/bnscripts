# BioNumerics script repository

This repository is used for "standalone" custom BioNumerics scripts, either in Python or in the legacy BNS script language.


## Adding scripts

Each script (*.py or *.bns file) is supposed to have a corresponding description in a markdown file (*.md) with the same name. 
The description file contains a yaml header with meta data about the script. For example: 

```yaml
---
title: "Axima_import_txt"   # Script title, defaults to filename without extension
date: 2018-03-07T15:01:51+01:00   # Creation date of the script
draft: false   # Whether or not the script should be published (draft should be true for scripts we don't want to include in the website)
author: "Koen Janssens"   # Script author
scriptlanguage: "bns"   # Script language, either "bns" or "py"
attachment: "Anto_ASCII.txt"   # Optional example data as attachment (should be made available in the 'static/attachment' directory)
---
```

Run following command in a full installation to auto-generate the markdown description for a NewDatabaseScript.py:

```
hugo new database/NewDatabaseScript.md
```

Next, add the remaining description in e.g. Notepad++. 

Note: On regular intervals, descriptions will be automatically generated for orphaned script files.


## Searching for scripts (for geeks)

The `git grep <regex>` command works nicely to search from the command line. Examples:
```
$ git grep -i kodon
```
Searches case-insensitive for "kodon", in scripts and descriptions.
```
$ git grep -i "similarity matrix" -- '*.py'
```
Searches case-insensitive for "similarity matrix", in Python scripts only.
```
$ git grep "Shannon[ -]Weiner"
```
Searches for "Shannon-Weiner" and "Shannon Weiner", in scripts and descriptions.

For more information, see http://travisjeffery.com/b/2012/02/search-a-git-repo-like-a-ninja/


## Static site generation

The markdown, script and example data files are used to generate a Hugo static site.

```
hugo --verbose --cleanDestinationDir
```

The `--verbose` option shows more info on the build, useful for troubleshooting. `--cleanDestinationDir` make sure that nothing from a previous build lingers around in the website directory.


## Searching for scripts (for biologists)

Use the search box in the website :-)
This searches scripts and their descriptions.

Tech note: The search funtionality is provided by ElasticSearch, indexed via the script /bin/index.py.



## More info

In case of doubt, contact Johan.