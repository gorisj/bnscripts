// UPDATE MLST SCRIPT
//   FOR DATABASES BASED IN CORK, IRELAND
//   INCLUDING E.COLI AND SALMONELA
//
// The MLST database tables will be first cleared,
//   then populated again with the on-line info.
//
//$MENU window=main;popup=MLST;insertpopupafter=Scripts;name=Download alleles and profiles (Cork);separator

Function integer locitodb(string locname, string locfile, string vartable, stringarray varfields)
{
   string st,tempfile,err,lname,lvariant,seqvar;
   file fl;
   
   tempfile=DbGetPath+"\temp\loci.txt";
   lname=locname;
   upcase(locname);
   UrlGetFile(locfile,tempfile,err);
   fileopenread(fl,tempfile);
   st=fileread(fl,1000);
   while st<>"" do {
      if find(st,">",1)=1 then {
         seqvar="";
         splitstring(st,locname);
         if find(st,"-",1)=1 then splitstring(st,"-");
         lvariant=st;
         st=fileread(fl,1000);
      }
      seqvar=seqvar+st;
      st=fileread(fl,1000);
      if st="" then {st=fileread(fl,1000);}
      if find(st,">",1)=1 then {
         CDBAddRecord(vartable,varfields,CollBuild(lvariant,"",lname,seqvar));
      }
   }
   
   filedelete(tempfile);
}
Function integer sttodb(string types,string sttable, stringarray stfields, string cctable, stringarray ccfields,stringarray loci, string organism)
{
   integer numfields,f;
   string tempfile,st,stt,err,cc;
   file fl;
   
   tempfile=DbGetPath+"\temp\types.txt";
   UrlGetFile(types,tempfile,err);
   fileopenread(fl,tempfile);
   st=fileread(fl,1000);//header line
   st=fileread(fl,1000);//read first dataline
   while st<>"" do {
      stt=splitstring(st,"	");//ST
      if organism="E.coli" then {
         cc=splitstring(st,"	");//CC
         splitstring(st,"	");//ancestral group
         CDBAddRecord(cctable,ccfields,CollBuild(stt, cc));
      }
      for f=1 to Length(loci) do {
         CDBAddRecord(sttable,stfields,CollBuild(stt, Get(loci,f), splitstring(st, "	")));
      }
      st=fileread(fl,1000);
   }
}

Function integer getschemainfo(string *vartable, string *sttable, string *cctable,string *errortable, stringarray *varfields,stringarray *stfields,stringarray *ccfields,stringarray *errfields)
{
   
   vartable="MLSTSEQVARIANTS";
   sttable="MLSTSEQTYPES";
   cctable="MLSTCC";
   errortable="MLST_ERROR";

   varfields=StrArrBuild("VARIANTID","VARIANTNAME","VARIANTLOCUS","VARIANTSEQ");
   stfields=strarrbuild("ST","VARIANTLOCUS","VARIANTID");
   ccfields=strarrbuild("ST","CC");
   errfields=StrArrBuild("KEY","EXPERIMENT","BATCHNR","MESSAGE","STATUS","CODE");
}

DIALOG dlg;
integer i,j,expernr,numloci,ok;
string st,st1,st2,errorst,globerror,sql,xmlst,res,organism,loc[],Field[],fieldlst,own;
string vartable,sttable,errortable,cctable,infourl,ecurl,xmldbfile,err,path,types,stschema,version,schema,compcode;
COLLECTION orgabbrs, orgloci;
string orglist, abbr, url,locus;
stringarray varfields,stfields,ccfields,dbloci,loci,lociloc,errfields,soaporgs,soapdesc;
XMLNODE rootnode,doc2,root;
FILE fl;

// general info
URL = "http://mlst.ucc.ie/mlst/mlst/dbs/"+abbr+"/handlers/getFileData/var/www/html/research/mlst/zope/Extensions/gadfly/"+abbr+"/DB/";
getschemainfo(vartable,sttable,cctable,errortable,varfields,stfields,ccfields,errfields);

Clear(orgabbrs);
Clear(orgloci);

Set(orgabbrs, "E.coli", "Ecoli");
Set(orgloci, "E.coli", StrArrBuild("adk","fumC","gyrB","icd","mdh","purA","recA"));

Set(orgabbrs, "M.catarrhalis", "Mcatarrhalis");
Set(orgloci, "M.catarrhalis", StrArrBuild("abcZ","adk","efp","fumC","glyBETA","mutY","ppa","trpE"));

Set(orgabbrs, "S.enterica", "Senterica");
Set(orgloci, "S.enterica", StrArrBuild("aroC","dnaN","hemD","hisD","purE","sucA","thrA"));

Set(orgabbrs, "Y.pseudotuberculosis", "Ypseudotuberculosis");
Set(orgloci, "Y.pseudotuberculosis", StrArrBuild("adk","argA","aroA","glnA","thrA","tmk","trpE"));

orglist = "E.coli~tM.catarrhalis~tS.enterica~tY.pseudotuberculosis";

// get organism and loci from database
st = DbLoadSettings("MLST",1);
if st="" then {
   MessageBox("Error", "Please install the MLST-online plugin first!", "exclamation");
   stop;
}
organism = splitstring(st, "</db>");
splitstring(organism, "<db>");

Clear(dbloci);
splitstring(st, "<numloci>");
numloci = intval(splitstring(st, "</numloci>"));
for i=1 to numloci do {
   splitstring(st, "<locus>");
   Add(dbloci, splitstring(st, "</locus>"));
}

try {
   Get(orgloci, organism, loci);
   Get(orgabbrs, organism, abbr);
} catch {
   DlgReset(dlg);
   DlgAddText(dlg,"Select MLST organism:",15,25,120,20);
   DlgAddList(dlg,orglist, organism,140,22,150,20,"drop");
   if DlgShow(dlg,"Select organism",430,205)<>1 then stop;
   Get(orgloci, organism, loci);
   Get(orgabbrs, organism, abbr);
}

// check loci
ok = 1;
if length(dbloci)<>length(loci) then {
   ok = 0;
} else {
   ok = 0;
   i = 0;
   while not(ok) and i<length(dbloci) do {
      i = i+1;
      locus = Get(dbloci, i);
      j = 0;
      while not(ok) and j<length(loci) do {
         j = j+1;
         ok = (locus = Get(loci, j));
      }
   }
}
if not(ok) then {
   MessageBox("Error", "The installed MLST scheme is incompatible with the chosen organism.", "exclamation");
   stop;
}

//clear VARIANT, ST and CC TABLES
CDBDelRecord(vartable,Not(CDBLXIsNull("VARIANTID")));
CDBDelRecord(sttable,Not(CDBLXIsNull("ST")));
CDBDelRecord(cctable,Not(CDBLXIsNull("ST")));

//install loci in MLSTSEQVARIANTS
for i=1 to Length(loci) do {
   setbusy("Importing variants for "+Get(loci,i));
   locus=Get(loci,i);
   upcase(locus);
   locitodb(Get(loci,i),url+locus+".fas",vartable,varfields);
}
setbusy("");

//install ST and CC and profiles
setbusy("Importing known profiles for "+organism);
types=url+"publicSTs.txt";
sttodb(types,sttable,stfields,cctable,ccfields,loci,organism);
setbusy("");

//save settings
st="<MLST><db>"+organism+"</db><soap>0</soap><numloci>"+str(Length(loci))+"</numloci><loci>";
for i=1 to Length(loci) do st=st+"<locus>"+Get(loci,i)+"</locus>";
st=st+"</loci></MLST>";
DbSaveSettings("MLST",st,1);
MessageBox("MLST update", "Update complete!", "information");
