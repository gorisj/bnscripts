---
title: "InstallMLST_SALM"
date: 2008-07-09T16:44:23+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---



Workflow:

1. Create a new Salmonella database
2. Install the MLST online plugin and choose "Use custom allele numbering" in the first step of the wizard. Press <Next>.
3. Leave all settings blank in the next step of the wizard. Click <Next>.
4. Click <OK>.

An empty version of the MLST plugin is now installed. In a next step, the online MLST information of Salmonella will be downloaded. Please make sure both scripts are located in the same folder.

5. Select Scripts > Run script from file. Select InstallMLST_Salm.bns.
6. Choose which fields you want to use to store the ST and CC information.
7. The script downloads all information in the database tables.
8. A message pops up saying that there already exists a MLST character type. Press <OK>.

For all 7 housekeeping genes, BioNumerics creates a sequence type. 

You can now use the Batch assembly import plugin for the import of your data and all menu-items under the menu "MLST"  for the analysis of your data. For more information, see the "MLST online" manual.

