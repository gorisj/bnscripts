---
title: "mlst_types_to_sequences"
date: 2011-12-02T11:36:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---



Converts MLST types to sequences.

Requires the MLST online plugin to be installed.

