#//$menu window=main;popup=MLST;insertpopupafter=Scripts;name=Convert MLST types to profile info...
import bns
Db = bns.Database.Db
dlgbuilder = bns.Windows.XmlDlgBuilder

class MlstFieldDlg(dlgbuilder.Dialogs):
	def __init__(self):
		dlgbuilder.Dialogs.__init__(self, "MlstProfileConversionFieldSelectDlg", remembersettings='global')
		
		values = dlgbuilder.ListValues([dlgbuilder.ListItem(fld.Name) for fld in Db.Fields])
		self.SelectFld = dlgbuilder.SimpleList('fldslist', values, 10, 30, multi=False)
		self.SelectFld.default = dlgbuilder.ExplicitValue(Db.Fields[0].Name)
		
		grid = dlgbuilder.Grid([
				[dlgbuilder.StaticText("Select the MLST profile info field:")],
				[self.SelectFld],
			])
		simple = dlgbuilder.SimpleDialog(grid)
		dlg = dlgbuilder.Dialog("dlg0", "Select info field", simple)
		self.AddDialog(dlg)

class MessageBoxDlg(dlgbuilder.Dialogs):
	mboxcnt = -1
	maxwidth = 50
	def __init__(self, title, msg, flags):
		self.mboxcnt += 1
		mboxid = "MlstConvert_MessageBox_{0}".format(self.mboxcnt)
		hasok = False
		oktext = ""
		cctext = "OK"
		if "okcancel" in flags:
			hasok = True
			oktext = "OK"
			cctext = "Cancel"
		if "yesno" in flags:
			hasok = True
			oktext = "Yes"
			cctext = "No"
		textpieces = msg.split("\r\n")
		grid = dlgbuilder.Grid([[dlgbuilder.StaticText(text, True, self.maxwidth)] for text in textpieces])
		simple = dlgbuilder.SimpleDialog(grid, oktext, cctext)
		dlg = dlgbuilder.Dialog("{0}_Dlg".format(mboxid), title, simple, hasokbutton=hasok)
		dlgbuilder.Dialogs.__init__(self, "{0}_Dlgs".format(mboxid))
		self.AddDialog(dlg)

def MessageBox(title, msg, flags):
	dlg = MessageBoxDlg(title, msg, flags)
	dlg.Show()
	return int(dlg.IsOk())

mlstexp = 'MLST'
if Db.ExperimentTypes[mlstexp] is None:
	MessageBox("Error", "The MLST online plugin is not installed.", "exclamation")
	__bnscontext__.Stop()
if not len(Db.Selection):
	MessageBox("Error", "No entries selected.", "exclamation")
	__bnscontext__.Stop()

# get the loci
chrtype = bns.Characters.CharSetType(mlstexp)
loci = [chrtype.GetChar(nr) for nr in xrange(chrtype.GetCount()) if chrtype.GetActChar(nr)]
if not loci:
	MessageBox("Error", "No MLST loci present", 'exclamation')
	__bnscontext__.Stop()

dlg = MlstFieldDlg()
if not dlg.Show():
	__bnscontext__.Stop()
mlstfld = dlg.GetValue(dlg.SelectFld.Id)

# loop over selected entries
chrset = bns.Characters.CharSet()
for entry in Db.Selection:
	if bns.Database.Experiment(entry, mlstexp).ExperID:
		chrset.Load(entry.Key, mlstexp)
		profile = '-'.join(str(int(chrset.GetVal(chrnr))) if chrset.GetPresent(chrnr) else '?' for chrnr in (chrset.FindName(locus) for locus in loci))
	else:
		profile = ""
	entry.Field(mlstfld).Content = profile
Db.Fields.Save()
# done
