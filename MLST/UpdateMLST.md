---
title: "UpdateMLST"
date: 2011-10-06T09:45:00+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---



Workflow 5.1 database: 

1. Select Scripts > Run script from file. Select UpdateMLST.bns. The MLST database tables will be first cleared, then populated again with the on-line info.

