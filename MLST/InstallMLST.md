---
title: "InstallMLST"
date: 2009-03-04T14:58:51+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---



Workflow 5.1 database: 

1. Create a new database
2. Install the MLST online plugin and choose "Use custom allele numbering" in the first step of the wizard. Press <Next>.
3. Leave all settings blank in the next step of the wizard. Click <Next>.
4. Click <OK>.

An empty version of the MLST plugin is now installed.

5. Select Scripts > Run script from file. Select InstallMLST.bns.
6. Choose which fields you want to use to store the ST and CC information and select the MLST organism from the list (E. coli, M. catarrhalis, S. enteric, Y. pseudotuberculosis).
7. The script downloads all information in the database tables.
8. A message pops up saying that there already exists a MLST character type. 
9. Press <OK>.

