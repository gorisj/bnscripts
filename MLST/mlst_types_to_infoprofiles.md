---
title: "mlst_types_to_infoprofiles"
date: 2010-04-10T15:50:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---



Converts MLST types to profile info in an information field.

Requires the MLST online plugin to be installed.

