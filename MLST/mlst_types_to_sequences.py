#//$menu window=main;popup=MLST;insertpopupafter=Scripts;name=Convert MLST types to sequences...
import bns
Db = bns.Database.Db
dlgbuilder = bns.Windows.XmlDlgBuilder

class MlstFieldDlg(dlgbuilder.Dialogs):
	def __init__(self):
		dlgbuilder.Dialogs.__init__(self, 'MlstConversionFieldSelectDlg', remembersettings='global')
		
		values = dlgbuilder.ListValues([dlgbuilder.ListItem(fld.Name) for fld in Db.Fields])
		self.SelectFld = dlgbuilder.SimpleList('fldslist', values, 10, 30, multi=False)
		self.SelectFld.default = dlgbuilder.ExplicitValue(Db.Fields[0].Name)
		self.SelectUnknown = dlgbuilder.Check('SelectUnknown', dlgbuilder.StaticText("Select entries with unknown type"))
		self.SelectUnknown.default = dlgbuilder.ExplicitValue(1)
		self.OverwriteSeq = dlgbuilder.Check('OverwriteSeq', dlgbuilder.StaticText("Overwrite existing sequences"))
		self.OverwriteSeq.default = dlgbuilder.ExplicitValue(0)
		
		grid = dlgbuilder.Grid([
				[dlgbuilder.StaticText("Select the MLST type info field:")],
				[self.SelectFld],
				[self.SelectUnknown],
				[self.OverwriteSeq],
			])
		simple = dlgbuilder.SimpleDialog(grid)
		dlg = dlgbuilder.Dialog("dlg0", "Select info field", simple)
		self.AddDialog(dlg)

class MessageBoxDlg(dlgbuilder.Dialogs):
	mboxcnt = -1
	maxwidth = 50
	def __init__(self, title, msg, flags):
		self.mboxcnt += 1
		mboxid = "MlstConvert_MessageBox_{0}".format(self.mboxcnt)
		hasok = False
		oktext = ""
		cctext = "OK"
		if "okcancel" in flags:
			hasok = True
			oktext = "OK"
			cctext = "Cancel"
		if "yesno" in flags:
			hasok = True
			oktext = "Yes"
			cctext = "No"
		textpieces = msg.split("\r\n")
		grid = dlgbuilder.Grid([[dlgbuilder.StaticText(text, True, self.maxwidth)] for text in textpieces])
		simple = dlgbuilder.SimpleDialog(grid, oktext, cctext)
		dlg = dlgbuilder.Dialog("{0}_Dlg".format(mboxid), title, simple, hasokbutton=hasok)
		dlgbuilder.Dialogs.__init__(self, "{0}_Dlgs".format(mboxid))
		self.AddDialog(dlg)

def MessageBox(title, msg, flags):
	dlg = MessageBoxDlg(title, msg, flags)
	dlg.Show()
	return int(dlg.IsOk())

mlstchrexp = 'MLST'
if Db.ExperimentTypes[mlstchrexp] is None:
	MessageBox("Error", "The MLST online plugin is not installed.", "exclamation")
	__bnscontext__.Stop()
if not len(Db.Selection):
	MessageBox("Error", "No entries selected.", "exclamation")
	__bnscontext__.Stop()

dlg = MlstFieldDlg()
if not dlg.Show():
	__bnscontext__.Stop()
mlstfld = dlg.GetValue(dlg.SelectFld.Id)
mlstselectunknown = int(dlg.GetValue(dlg.SelectUnknown.Id))
mlstseqoverwrite = int(dlg.GetValue(dlg.OverwriteSeq.Id))

# get the loci
chrtype = bns.Characters.CharSetType(mlstchrexp)
loci = [chrtype.GetChar(nr).lower() for nr in xrange(chrtype.GetCount()) if chrtype.GetActChar(nr)]
if not loci:
	MessageBox("Error", "No MLST loci present", 'exclamation')
	__bnscontext__.Stop()

# some variables to use with recordsets
lx_all = bns.ConnectedDb.DbWhere.All().Expression
rc = bns.ConnectedDb.ObjectReadContext()
rset = bns.ConnectedDb.ObjectRecordSet()
flds = []

# read alleles
alleleIDs = dict((locus, {}) for locus in loci)
ot = bns.ConnectedDb.ObjectType('MLSTSEQVARIANTS')
cols = ['VARIANTLOCUS', 'VARIANTID', 'VARIANTSEQ']
rset.Create(rc, ot, cols, lx_all, cols[0], 0)
while rset.GetNextRecord(flds):
	locus = flds[0].lower()
	id = flds[1].lower()
	seq = flds[2]
	alleleIDs[locus][id] = seq
rset.Close()
if not alleleIDs:
	MessageBox("Error", "No MLST alleles found. Make sure the MLST alleles and pofiles are updated.", 'exclamation')
	__bnscontext__.Stop()

# read types
mlstcomplist = []
ot = bns.ConnectedDb.ObjectType('MLSTSEQTYPES')
cols = ['ST', 'VARIANTLOCUS', 'VARIANTID']
types = {}
rset.Create(rc, ot, cols, lx_all, cols[0], 0)
while rset.GetNextRecord(flds):
	mlstcomplist.append([f.lower() for f in flds])
rset.Close()
if not mlstcomplist:
	MessageBox("Error", "No MLST profiles found. Make sure the MLST alleles and pofiles are updated.", 'exclamation')
	__bnscontext__.Stop()

sts = set(mlst[0] for mlst in mlstcomplist)
types = dict((st, {}) for st in sts)
for st, locus, id in mlstcomplist:
	try:
		seq = alleleIDs[locus][id]
	except KeyError:
		seq = None
	types[st][locus] = seq

# loop over selected entries
unknown_entries = set()
unknown_types = set()
unknown_alleles = set()
seq = bns.Sequences.Sequence()
for entry in Db.Selection:
	if not mlstseqoverwrite:
		if any(bns.Database.Experiment(entry, locus).ExperID for locus in loci):
			continue
	
	mlst = entry.Field(mlstfld).Content
	try:
		alleles = types[mlst]
	except KeyError:
		unknown_types.add(mlst)
		unknown_entries.add(entry)
		continue
	for locus in loci:
		allele_seq = alleles[locus]
		experlink = bns.Database.Experiment(entry, locus)
		if allele_seq is None:
			if experlink.ExperID:
				experlink.Delete()
			unknown_alleles.add((mlst, locus))
			unknown_entries.add(entry)
			continue
		
		if experlink.ExperID:
			seq.Load(entry.Key, locus)
		else:
			seq.Create(locus, "", entry.Key)
		seq.Set(allele_seq)
		seq.Save()

if unknown_types or unknown_alleles:
	if unknown_types:
		MessageBox("Warning", "There where unknown mlst types: '{0}'.".format("', '".join(unknown_types)), 'exclamation')
	if unknown_alleles:
		MessageBox("Warning", "There were mlst types with unknown alleles: '{0}'.".format("', '".join('{0}: {1}'.format(mlst, locus) for mlst, locus in unknown_alleles)), 'exclamation')
	if mlstselectunknown:
		Db.Selection.Clear()
		for entry in unknown_entries:
			entry.Selection = True
		Db.Entries.OrderBySel()
# done

