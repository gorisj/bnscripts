---
title: "Shannonweiner_index_of_diversity"
date: 2008-06-11T16:49:59+01:00
draft: false
author: "Jill Dombrecht"
scriptlanguage: "bns"
---

In a comparison with groups defined, this tool will calculate the Shannon-Weiner's Index of Diversity for each group and the total diversity. 

It returns the total number of entries, the number of entries in each group, and the Index of Diversity for each group and the total index of diversity, based on natural logarithms.

