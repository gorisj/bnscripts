---
title: "ExportSimilMat"
date: 2010-06-22T12:22:52+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "py"
---


This script will run in a comparison window and export a similarity matrix for a selected experiment and fields, open it in Excel by default.

