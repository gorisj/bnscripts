#//$menu window=comparison;popup=File;name=Export Similarity Matrix;
import bns

# shorthands
Db = bns.Database.Db
Dlg = bns.Windows.XmlDlgBuilder

class ExportMatDlg(Dlg.Dialogs):
	def __init__(self, experList, fieldList, minBandCount=20):
		Dlg.Dialogs.__init__(self, "ExportMatDlg", remembersettings="global")
		
		# list values
		experVals = Dlg.ListValues([Dlg.ListItem(exp) for exp in experList])
		fieldVals = Dlg.ListValues([Dlg.ListItem(fld) for fld in fieldList])
		
		# controls - set defaults
		self.selectExper = Dlg.Drop("selectExper", experVals, 5, 30, canEdit=False, default=Dlg.ExplicitValue(experList[0]))
		self.selectFields = Dlg.SimpleList("selectFields", fieldVals, 8, 30, multi=True)
		self.fileOutput = Dlg.File("fileOutput", [Dlg.Extension("txt", "Text file")], 30, "Output file")
		
		# layout
		grid = Dlg.Grid([[Dlg.StaticText("Select a fingerprint experiment:"), self.selectExper],
		                 [Dlg.StaticText("Information fields:"), self.selectFields],
		                 [Dlg.StaticText("Specify an export file:"), self.fileOutput]])
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		dlg = Dlg.Dialog("theOne", "Export Similarity matrix", simple)
		self.AddDialog(dlg)
	
	def onOk(self, args):
		# Validations - if failed, set args["result"] = "0"
		errors = []
		self.exper = self.selectExper.GetValue()
		if not self.selectExper.GetValue():
			errors.append("Select an experiment!")
		if not self.selectFields.GetValue():
			errors.append("Select an information field!")
		self.flName = self.fileOutput.GetValue()
		if len(self.flName):
			try:
				open(self.flName, "w").close()
			except IOError as e:
				errors.append("Cannot open file:\n   "+str(e))
		else:
			errors.append("Specify an export file!")
		if errors:
			bns.Util.Program.MessageBox("Error", "\n".join(errors), "exclamation")
			args["result"] = "0"

# list of fingerprint experiment types
experList = [exper.Name for exper in Db.ExperimentTypes]
if not experList:
	bns.Util.Program.MessageBox("Error", "No experiments present!", "")
	__bnscontext__.Stop()

# list of information fields
fieldList = [field.Name for field in Db.Fields]
if not fieldList:
	bns.Util.Program.MessageBox("Error", "No entry information fields present!", "")
	__bnscontext__.Stop()

#attach script to current comp window
comp=bns.Comparison.Comparison(__bnswinid__)


# show dialog
dlg = ExportMatDlg(experList, fieldList)
if not(dlg.Show()):
	__bnscontext__.Stop()

# dialog results
exper = dlg.exper
flName = dlg.flName
fields = dlg.selectFields.Multi

comp=bns.Comparison.Comparison(__bnswinid__)

##if no similarity matrix present calculate one
if comp.GetSimil(exper,1,1)== -200:
	comp.CalcClust(exper)

entries = []
simils = []
lines = []
for i in range(comp.GetEntryCount()):
	key = comp.GetEntryKey(i)
	myentry=bns.Database.Entry(key)
	entries.append(key)
	comp.GetSimilRow(exper,i,simils)
	line = [myentry.Key]
	line.extend(myentry.Field(fld).Content for fld in fields)
	line.extend('{0:.2f}'.format(s) for s in simils)
	lines.append('\t'.join(line) + '\n')

fl = file(flName, "w")
fl.write('\t'.join(["Key"] + fields + entries) + '\n')
fl.writelines(lines)
fl.close()

# a trick to open in Excel
xlsFile = Db.Info.TempDir + r"\result.xls"
open(xlsFile, "w").close()
exeName = bns.Util.Program.findexecutable(xlsFile)
if not exeName:
	# if Excel not installed on this PC: open in Notepad
	exeName = "notepad"
bns.CallBnsFunction("execute", exeName+" "+flName)

