---
title: "Read_applied_text_files"
date: 2018-03-07T15:32:18+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
attachment: "123921 exported spectrum.txt"
---


Step 1: Create Fingerprint type with reference system
1)	Select Experiments > Create fingerprint type
2)	Specify settings (densitometric curves and OD range) 

 

3)	After having created the Fingerprint type, open the Fingerprint type window by double-clicking on it in the Experiments panel. 
4)	Select Settings > New reference system (positions). Give a name to the reference system, and enter a few positions. In the window below, I have entered two positions, corresponding to the limits of the text files. Press <OK>. 

 

5)	Select Settings > Edit Reference system, select Metrics > Copy markers from reference system. Unselect Logaritmic dependence, select First degree fit.

 

6)	The reference system is shown in the fingerprint type window.
 
7)	Select Settings > General settings, select the third tab Normalization and change the resolution of the normalized tracks to 4096pts. This is for the visualization of the peaks in the Comparison window (see further).
8)	Close the windows.
 
Step 2: Import AB text files
1)	Copy the scripts to the scripts folder of BioNumerics. The default path of this directory is: C:\Program Files\GelCompar II\scripts.
2)	Once the scripts are copied to the scripts folder of your database, re-open the database. In the main window, the menu-item MALDI appears, with some sub-items.
3)	Select MALDI > Import AB text file(s) for the import of AB text file(s)
4)	In the next window, select your text files. NOTE: More than one text file can be selected at the same time. 

 

5)	In the next window, select the Fingerprint type, enter a File name and specify the OD range. NOTE: The File name must be unique (no duplicates allowed in BioNumerics). 
 
6)	BioNumerics imports the data…. The file names are used as keys in the database (see screenshot below).
 

