---
title: "Show_curves"
date: 2018-03-07T15:32:58+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
attachment: "123921 exported spectrum.txt"
---



Show curves
1)	Select a couple of entries in the database (CTRL + click) and select MALDI > Show. Select the fingerprint type and press <OK>. 
2)	The spectral analysis window pops up. 
 
3)	With the menu commands you can play around with the sizes of the curves.

 
Show curves in comparison
1)	Select a couple of entries in the main window. Make a comparison (Comparison > Create new comparison).
2)	In the comparison window, show the lanes of the Fingerprint type (click on the fingerprint icon), show the metrics lane and the curves (under Layout > Show…). 
 

Average curves
1)	In the main window, select the entries that you want to create an averaged densitometric curve from (to select entries use the CTRL or SHIFT key).
2)	Select Scripts > Browse Internet. 
3)	Select Fingerprint related tools.

 

4)	Scroll down the page and select Create averaged fingerprint (for more information press the <Description> button).
5)	Select the fingerprint to average, enter the key for the new entry and enter a file name. 

 

6)	The new entry is created containing the averaged curve. 

