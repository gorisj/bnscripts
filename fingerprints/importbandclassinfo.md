---
title: "Importbandclassinfo"
date: 2009-03-10T16:21:07+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---


Imports bandclass information stored in a text file (works in combination with "export bandclass information" script).

