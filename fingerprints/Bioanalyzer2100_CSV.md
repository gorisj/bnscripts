---
title: "Bioanalyzer2100_CSV"
date: 2010-03-12T16:52:04+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "bns"
---


This script allows the import of an Agilent 2100 BioAnalyzer CSV file. The data can be linked to an existing fingerprint type, or a new fingerprint type and reference system can be created while running the script.

