"""export_bandmatching.py

This script is attached to the Comparison window.
It exports the current bandmatching table to a tab-delimited file.
If a band is present, the name of the band class is written,
else a '0' is written.

DISCLAIMER:

Sample script provided "as is" by Applied Maths.
You are free to use and modify this script for your own purposes.
Please notice that improper use of script commands may corrupt your database. 
Running this script is entirely at your own responsibility. 
Applied Maths accepts no liability for any damage that results from using this script.

"""

#//$menu window=comparison;popup=Fingerprints;insertafter=Export band matching...;name=Export band matching...
import bns
MessageBox = bns.Util.Program.MessageBox

def main(winID):
	cmp = bns.Comparison.Comparison(winID)
	exp = bns.Database.Db.ExperimentTypes[cmp.GetSelExper()]
	if exp.Class != 'FPR':
		MessageBox("Error", "Please select a fingerprint type experiment.", 'exclamation')
		return
	
	bm = bns.Comparison.BandMatching(winID, exp.Name)
	if not bm.IsPresent():
		MessageBox("Error", "Perform a bandmatching first.", 'exclamation')
		return	
	
	bc = tuple(bm.GetClassName(bcIdx) for bcIdx in xrange(bm.GetClassCount()))
	
	flName = bns.Database.Db.Info.HomeDir + r'\RESULT.TXT'
	results = open(flName, 'w')
	results.write('\t{0}\n'.format('\t'.join(bc)))
	for eIdx in xrange(cmp.GetEntryCount()):
		results.write(cmp.GetEntryKey(eIdx))
		if bm.IsBandsLoaded(eIdx):
			bands = []
			for bcIdx, bcName in enumerate(bc):
				b = bm.GetBandByClass(eIdx, bcIdx)
				if b < 0:
					bands.append('0')
				else:
					bands.append(bcName)
		else:
			bands = ['0'] * len(bc)
		results.write('\t{0}\n'.format('\t'.join(bands)))
	results.close()
	bns.CallBnsFunction('execute', 'notepad "{0}"'.format(flName))

if __name__ == '__main__':
	main(__bnswinid__)

