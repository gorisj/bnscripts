---
title: "Export_bandmatching"
date: 2010-08-19T12:13:16+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "py"
---


When running the script in the Comparison window of BioNumerics 6.1 or higher, the script will export the bandmatching table, with rows corresponding to the entries in the comparison, and columns corresponding to the bandclasses that have been defined in the comparison window for the selected fingerprint type experiment. The header row contains the band class names (default this is the band class position). When a band is present in a bandclass, the band class name (default this is the band class position) will be exported; when the band is not present in a bandclass, the value 0 will be exported. 

1. Close any instance of BioNumerics. 
2. Paste the attached python script in the correct python folder. 
3. Open a comparison window, select a fingerprint type experiment and perform a bandmatching. 
4. Optionally change the band class assignments, delete/add band classes, change the band class label names, etc. 
5. Make sure the correct fingerprint type experiment is selected in the comparison window and select the script item "Fingerprints > Export band matching" (this script menu item should be proceeded with a hammer, indicating that it will launch a script). 

The band class information is exported to a text file.

