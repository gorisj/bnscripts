---
title: "Convert_refsys"
date: 2007-03-12T16:34:00+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "bns"
---


This script will automatically convert all gels that were normalised using a particular reference system to another reference system. Note that such a conversion is only valid in certain cases (e.g. to correct a typing mistake in a band name).
The script can't determine whether or not this action is valid. This is up to the user

