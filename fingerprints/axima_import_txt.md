---
title: "Axima_import_txt"
date: 2018-03-07T15:01:51+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
attachment: "Anto_ASCII.txt"
---


Shimadzu Biotech Axima

1.	Open an existing BioNumerics connected database or create a new connected database.
2.	In the BioNumerics database, add a new information field (Database > Add new information field, enter a name e.g. Sample name and press <OK>).
3.	Select Scripts > Run script from file. 
4.	Navigate to the path where the attached script is stored and select the script.
5.	Select the Axima data text file.
6.	A new dialog box pops up.
7.	In the right column, select the information field for the storage of the sample name (select the information field created in step 2). 
8.	In the left column, select an existing fingerprint type, or let the software create a new fingerprint type.
9.	Press <OK> to import the information into the database.

A new entry is created with a unique (randomly generated) key. The sample name is parsed from the text file (the script looks in the text file for the #Data line and uses the name that stands after #Data) and the name is stored in the information field that was selected.

10.	If a new fingerprint type has been created, change the following fingerprint type settings (this only needs to be done once).

General settings: 
a.	Double-click on the fingerprint type experiment in the Experiments panel. 
b.	Select Settings > General Settings.
c.	Check if the OD range in the Raw data tab is set to 1 000 000.
d.	Select the Normalization tab and change the resolution to 18000.
e.	Close the Settings dialog box.

Reference system:
f.	Select Settings > New reference system (positions).
g.	Enter a name for the new reference system (eg. R01).
h.	Add two points: 0% = 2000 and 100% = 20000
i.	Close the window.
 
j.	Create a calibration curve for the reference system: select Settings > Edit reference system.
k.	Select Metrics > Copy markers from reference system and confirm the action.
l.	Select Metrics > Logarithmic dependence and confirm.
m.	Close the Reference window and Fingerprint type window.

11.	Click on the colored dot in the Experiment presence panel to view the experiment card of that entry. 
12.	Use the + and – signs to zoom in or out.



