---
title: "Exportbandclassinfo"
date: 2009-03-10T16:30:52+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---


Exports the bandclass information for all fingerprint types in the database that contain bandclass information. The results are stored in a text file in the Export folder of the database.

