---
title: "Read_bruker_text_files"
date: 2018-03-07T15:37:28+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
attachment: "D19_0_D19_2.txt"
---


### Step 1: Create Fingerprint type with reference system ###

  1. Select Experiments > Create fingerprint type
  2. Specify settings (densitometric curves and OD range) 
  3. After having created the Fingerprint type, open the Fingerprint type window by double-clicking on it in the Experiments panel. 
  4. Select Settings > New reference system (positions). Give a name to the reference system, and enter at least two positions. In the window below, I have entered two positions, corresponding to the limits of the text files. Press <OK>. 
  5. Select Settings > Edit Reference system, select Metrics > Copy markers from reference system. Confirm the action.
  6. Unselect Logaritmic dependence, select First degree fit.
  7. The reference system is shown in the fingerprint type window.
  8. Close the windows.

### Step 2: Import Bruker text files ###

  1. Copy the scripts read_bruker_text_file.bns and show_curves.bns to the scripts folder of the database (BioNumerics\data\name_db\scripts).
  2. Once the scripts are copied to the scripts folder of your database, re-open the database. In the main window, the menu-item MALDI appears, with some sub-items.
  3. Select MALDI > Import Bruker text file(s) for the import of the bruker text file(s)
  4. In the next window, select your text files. NOTE: More than one text file can be selected at the same time. 
  5. In the next window, select the Fingerprint type, enter a File name and specify the OD range. NOTE: The File name must be unique (no duplicates allowed in BioNumerics). 
  6. BioNumerics imports the data…. The file names are used as keys in the database (see screenshot below).


### Show curves ###

  1. Select a couple of entries in the database (CTRL + click) and select MALDI > Show. Select the fingerprint type and press <OK>. 
  2. The spectral analysis window pops up. 
  3. With the menu commands you can play around with the sizes of the curves.


#### Show curves in comparison ####

  1. Select a couple of entries in the main window. Make a comparison (Comparison > Create new comparison).
  2. In the comparison window, show the lanes of the Fingerprint type (click on the fingerprint icon).
  3. Show the metrics lane and the curves. 


#### Average curves ####

  1. In the main window, select the entries that you want to create an averaged densitometric curve from (to select entries use the CTRL or SHIFT key).
  2. Select Scripts > Browse Internet. 
  3. Select Fingerprint related tools.
  4. Scroll down the page and select Create averaged fingerprint (for more information press the <Description> button).
  5. Select the fingerprint to average, enter the key for the new entry and enter a file name. 
  6. The new entry is created containing the averaged curve. 


