---
title: "Spa_import_cc"
date: 2008-06-02T12:19:53+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "bns"
---


This 5.1 script appears as a menu-item when present in the scripts folder: Spa-Typing > Import clonal complexes...

The script imports clonal complexes in the SPA_TYPES table.
The import file is assumed to be a tab-delimited file with a header row. The first column should be the spa type and the second the clonal complex. Error checking is minimal.

ONLY WORKS IN BN 5.1!!! DOES NOT WORK IN 6.X (6.X uses objects....)

