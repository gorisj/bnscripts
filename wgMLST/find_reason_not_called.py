import bns
from wgMLST_Client import AlleleCalls
from collections import defaultdict

from wgMLST_Client.wgMLSTSchema import Schema
currSchema = Schema.GetCurrent()
wgmlstExperType = bns.Database.ExperimentType(currSchema.WgMLSTExperTypeID)

lociToIgnore1  = set()
#lociToIgnore1 = set([line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\ecoli_cdc\cleanup2\am_loci_to_ignore.txt')])
#lociToIgnore1 = set(['SALM_'+line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\salmonella_cdc\phages_to_remove.txt')])
#lociToIgnore1.update(set(['SALM_'+line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\salmonella_cdc\am_ext_to_remove_salm.txt')]))
#lociToIgnore1.update(set([''+line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\salmonella_cdc\to_ignore_bad_frame_salm.txt')]))

#wgmlstExperType = bns.Database.ExperimentType("wgmlst_enterobase_clean_2")
#wgmlstExperType = bns.Database.ExperimentType("wgmlst_eb")

#lociToIgnore1.update(set(['ECOLI_'+line.strip().split()[0] for line in open(r'C:\Users\koenro\Documents\Leg_stats\am_eb_combined_to_remove.txt')]))
#lociToIgnore1.update(set(['ECOLI_'+str(k) for k in range(17440+1, 30000)]))
#lociToIgnore1.update(set(['ECOLI_'+str(k) for k in [2583, 3825, 4002, 4362, 4367, 5105, 6098, 6525, 7060, 7497, 7648, 8144, 8752, 9675, 10351, 11318, 11386, 11946, 12888, 13140, 13279, 13483]]))



#the final scheme
#remove all loci 
lociToIgnore1 = set()
#- from EB that we dont care about
#lociToIgnore1.update(set(['EC_'+str(k) for k in range(17350+1, 17400)]))

#- that are in overlap, but not in the core
#lociToIgnore1.update(set(['SALM_'+line.strip().split()[0] for line in open(r'C:\Users\koenro\Documents\Leg_stats\am_eb_combined_to_remove.txt')]))

#- that are labeled phage, but are not in the core
#lociToIgnore1.update(set([line.strip().split()[0] for line in open(r'C:\Users\koenro\Documents\Leg_stats\am_eb_combined_phages.txt')]))


class Counts(object):
	def __init__(self):
		self.nCalled = 0
		self.nNotCalled = 0
		self.nLowSimil = 0
		self.nNonACGT = 0
		self.nNoCDS = 0
	def add(self, nCalled, nNotCalled, nLowSimil, nNonACGT, nNoCDS):
		self.nCalled+=nCalled
		self.nNotCalled+=nNotCalled
		self.nLowSimil+=nLowSimil
		self.nNonACGT+=nNonACGT
		self.nNoCDS+=nNoCDS
	
	def addCalled(self):
		self.nCalled+=1
		
	def addNotCalled(self):
		self.nNotCalled+=1

	def addLowSimil(self):
		self.nLowSimil+=1

	def addNonACGT(self):
		self.nNonACGT+=1

	def addNoCDS(self):
		self.nNoCDS+=1		
		
	def toStr(self):
		return '{}\t{}\t{}\t{}\t{}'.format(self.nCalled, self.nNotCalled, self.nLowSimil, self.nNonACGT, self.nNoCDS)
		
		

locusReasons = defaultdict(Counts)
entryReasons = defaultdict(Counts)

nEntries = 0
for e in bns.Database.Db.Selection:

	nEntries += 1
	nCalled = 0
	nNotCalled = 0
	nLowSimil = 0
	nNonACGT = 0
	nNoCDS = 0
	nBasesNotCalled = 0

	attach = bns.Database.ExperAttachments(e.Key, wgmlstExperType.Name)
	content = attach['_BLASTAlleleFinderResult_']
	calls = AlleleCalls.LocusCalls.CallsFromString(content)
	for locus, locusCalls in calls.iteritems():
		if locus in lociToIgnore1: continue
		for call in locusCalls.calls:
			if call.Similarity==100.0: 
				nCalled+=1
				locusReasons[locus].addCalled()
			else:
				nNotCalled+=1
				nBasesNotCalled+=abs(call.Stop-call.Start)
				
				locusReasons[locus].addNotCalled()
				if int(call.qualities.get('no', 777))>=1: 
					nNonACGT+=1
					locusReasons[locus].addNonACGT()
				elif call.qualities.get('rss')=='Y' and (call.qualities.get('sc')!='Y' or call.qualities['ec']!='Y'):
					nNoCDS += 1					
					locusReasons[locus].addNoCDS()
				elif call.Similarity<70.0:
					nLowSimil+=1
					locusReasons[locus].addLowSimil()					
					
	entryReasons[e.Key].add(nCalled, nNotCalled, nLowSimil, nNonACGT, nNoCDS)	
	print '{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(e.Key, nCalled, nNotCalled, nLowSimil, nNonACGT, nNoCDS, nBasesNotCalled)

loci = 	locusReasons.keys()
loci.sort(key=lambda x: - (locusReasons[x].nNotCalled / (locusReasons[x].nNotCalled + locusReasons[x].nCalled)) )

for locus in loci:
	reason = locusReasons[locus]
	if reason.nNotCalled >0:
		print '{}\t{}'.format(locus, locusReasons[locus].toStr())
	
	
print
print

for locus in loci:
	reason = locusReasons[locus]
	if reason.nNoCDS > 0.5 * (reason.nCalled + reason.nNotCalled):
		print '{}\t{}'.format(locus, locusReasons[locus].toStr())
	
	
