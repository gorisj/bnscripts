#//$menu window=main;popup=WGS tools;insertafter=Synchronize;name=Redo submission of new alleles for selected entries...
import bns
from wgMLST_Client import AlleleCalls
from wgMLST_Client import ClientSettings
from wgMLST_Client.wgMLSTSchema import Schema
import xml.etree.cElementTree as ET

from wgMLST_Client.SchedulerCommonTools.CheckMultipleCalcs import CheckMultipleCalcs, CheckMultipleRefresh

MessageBox = bns.Util.Program.MessageBox

def Resubmit(locusCalls):
	
	def AllowCalc():

		hasScriptFunc = hasattr(bns.Windows.BnsWindow(1), 'IsCalculationBusy')
		if hasScriptFunc:
			return not bns.Windows.BnsWindow(1).IsCalculationBusy()
		else:
			return True
			
	def startAsync():
		"""Submits selected new alleles
		"""
		if AllowCalc:
			
			bns.Windows.BnsWindow(1).StartAsyncCalculation(_Resubmit,_Refresh, False)
			

	def _Resubmit(dct):
	
		calcComm = dct['communication']
		calcComm.SetMessage('Checking alleles to be submitted')
		submitSettings = ClientSettings.WgMLSTClientSettings.GetCurrent()
		toSubmit = locusCalls.attachmentContents['_NewAllelesOnHold_']
		locusIdsSeq = {}
		root = ET.fromstring(toSubmit)
		for xLocus in root.iter('SubmitNewAllele'):
			
			locusId = xLocus.find('LocusId').text
			seq = xLocus.find('Sequence').text
			locusIdsSeq[locusId] = seq

		result = locusCalls.attachmentContents['_BLASTAlleleFinderResult_']
		
		locusIds = locusCalls.CallsFromString(result)
		locusIdsToSubmit = {}
		
		
		for call,allele in locusIds.items():
			if ">"+call+"<" in toSubmit:
				allele.calls[0].sequences['s'] = locusIdsSeq[call]
				if	 allele.calls[0].CanSubmit():
					if allele.calls[0].CanAutoSubmit(submitSettings):
						locusIdsToSubmit[call]=allele.calls
				
		if len(locusIdsToSubmit)>0:	
			calcComm.SetMessage('Submitting {0} alleles for entry {1}'.format(len(locusIdsToSubmit),locusCalls.entry))
			locusCalls.SubmitNewAlleles('_BLASTAlleleFinderResult_', locusIdsToSubmit,calcComm)
			
			if not calcComm.MustContinue():
				raise RuntimeError("Script has been stopped by user.")
			return
			
	

	def _Refresh(dct):
			
		
		locusCalls.InterSectCalls()	

	#bns.Windows.BnsWindow(1).StartAsyncCalculation(_Resubmit,_Refresh,async=False)
	startAsync()
		
if __name__=='__main__':
	from wgMLST_Client.wgMLSTSchema import GetCurrentSchema

	
	if not bns.Database.Db.Selection:
		MessageBox("Error", "No entries selected.", "exclamation")
	for entry in bns.Database.Db.Selection:
		if bns.Database.Experiment(entry, Schema.GetCurrent().WgMLSTExperTypeName).IsPresent():
			
			locusCalls = AlleleCalls.LocusCalls("", entry, Schema.GetCurrent().WgMLSTExperTypeName)
			Resubmit(locusCalls)
