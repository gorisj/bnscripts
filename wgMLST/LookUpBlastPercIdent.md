---
title: "LookUpBlastPercIdent"
date: 2018-09-24T12:18:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


Determines the BLAST percent identity threshold for entries for which a BLASTAlleleFinder job was performed. 


