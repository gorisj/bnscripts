#//$menu window=main;popup=WGS tools;insertafter=Synchronize;name=Calculate core percentage for selected entries...
import bns
from wgMLST_Client.CommonTools.Settings import StoredSettings

Dlg = bns.Windows.XmlDlgBuilder

class SelectAspectDlg(Dlg.Dialogs):
	def __init__(self, expID, expName):
		Dlg.Dialogs.__init__(self, 'SelectAspectDlg')
		
		# the values to show in the list control
		cst = bns.Characters.CharSetType(expID)
		self.aspects = [cst.ViewGet(id) for id in cst.ViewGetList()]
		self.values = [aspect.GetInfo()['DisplayName'] for aspect in self.aspects]
		coreAspect = [value for value in self.values if 'Core' in value or 'core' in value][0]
		
		# the list controls
		self.coreList = Dlg.SimpleList('core', self.values, len(self.values)+1, 40, multi=False, default=coreAspect)
		
		# now define the dialog layout
		grid = [["Select 'core' loci:", self.coreList]]
		simple = Dlg.SimpleDialog(grid, onStart=self.onStart, onOk=self.onOk)
		self.AddSimpleDialog(expName, simple)
	
	def onStart(self, args):
		"""Set the selected items"""
		pass
	
	def onOk(self, args):
		"""Get the selected items"""
		self.coreAspect = self.aspects[self.values.index(self.coreList.GetValue())].GetID()

def CalculateCorePercent(entry, experId, coreAspectId):
	hasAlleleIds = False
	nCore = None
	
	exper = bns.Database.Experiment(entry, experId)
	if exper.IsPresent():
		cst = bns.Characters.CharSetType(experId)
		try:
			coreAspect = cst.ViewGet(coreAspectId).GetCharacters()
		except RuntimeError as e:
			raise RuntimeError("Isolate {0}: invalid core aspect {2} for {1}.".format(entry.DispName, bns.Database.ExperimentType(experId).DispName, coreAspectId))
			
		hasAlleleIds = True
		
		coreLocusIdxs = set(cst.FindChar(locus) for locus in coreAspect)
		values = []
		presences = []
		
		chrSet = exper.LoadOrCreate()
		chrSet.GetData(values, presences)
		nPresent = sum(p for i, p in enumerate(presences) if i in coreLocusIdxs)
		nLoci = len(coreLocusIdxs)
	
	corePercent = float(nPresent)/float(nLoci)*100 if hasAlleleIds else '?'
	return corePercent

def SaveCharVal(entry, chrExpName, chrName, chrValue):
	changed = 0
	chrSet = bns.Characters.CharSet()
	if bns.Database.Experiment(entry.Key, chrExpName).ExperID:
		chrSet.Load(entry.Key, chrExpName)
	else:
		chrSet.Create(chrExpName, '', entry.Key)
	idx = chrSet.FindName(chrName)
	if idx<0:
		idx=bns.Characters.CharSetType(chrExpName).AddChar('CorePercent',100)
	if chrValue == '?':
		chrSet.SetAbsent(idx)
	else:
		chrSet.SetVal(idx, float(chrValue))
		changed = 1
	chrSet.Save()
	return changed
	
def main(corePercentChar):
	changes = 0
	settings = StoredSettings('WGMLST_CLIENT_SCHEMA_SETTINGS',
												'wgMLSTExperTypeName', 'wgMLSTExperTypeID',
												'qualityExperTypeName', 'qualityExperTypeID')		
	dlg = SelectAspectDlg(settings.wgMLSTExperTypeID, settings.wgMLSTExperTypeName)
	if not dlg.Show():
		bns.Stop()
	for i, entry in enumerate(bns.Database.Db.Selection):
		bns.SetBusy("Processing {0} entries...".format(i))
		corePercent = CalculateCorePercent(entry, settings.wgMLSTExperTypeID, dlg.coreAspect)
		changed = SaveCharVal(entry, settings.qualityExperTypeID, corePercentChar, corePercent)
		changes += changed
	bns.SetBusy("")
	bns.Util.Program.MessageBox("Report", "{0} core percentages saved.".format(changes), 'information')
	
if __name__ == '__main__':
	corePercentChar = 'CorePercent'
	main(corePercentChar)
