---
title: "find_reason_not_called"
date: 2017-01-18T10:19:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


Finds the reason why wgMLST loci have not been called. 

