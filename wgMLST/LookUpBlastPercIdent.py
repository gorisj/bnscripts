import bns
import datetime





def try_strptime(logContent):
	format = "%Y-%m-%d %H:%M:%S"
	try:
		s = logContent.split("\n",1)[1].split("\t",1)[0]
		date = datetime.datetime.strptime(s, format)
	except ValueError:
		date = None
	return date
	
entriesWithBlast="Key\tBlast perc ident"
for entry in bns.Database.Db.Selection:
	attachments = entry.GetAttachmentList()
	
	
	previousDate = datetime.datetime(1,1,1)
	for log in attachments:
		
		if "detailed log" in log["Description"]:
			logContent = log["Attach"].Content
			if "BLASTAlleleFinder" in logContent:
				date = try_strptime(logContent)
				if date > previousDate:
					i = logContent.find("perc_identity ")
					perc_ident = logContent[i+14:i+16]
	entriesWithBlast = entriesWithBlast + "\n{0}\t{1}".format(entry.Key,perc_ident)
				
if len(entriesWithBlast) > 21:
	bns.Util.IO.ExportAndView(entriesWithBlast)

