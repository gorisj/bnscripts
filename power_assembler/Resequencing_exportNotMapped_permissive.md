---
title: "Resequencing_exportNotMapped_permissive"
date: 2019-06-17T13:59:00+01:00
draft: false
author: "Margo Diricks"
scriptlanguage: "xml"
attachment: "Scripts and changes to power assembler.docx"
---


A pipeline template for a resequencing assembly which also exports the non-mapped sequence reads.
The attached document describes the workflow to execute the pipeline. 



