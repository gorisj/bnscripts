---
title: "Resequencing_exportNotMapped_pipeline_final4_MD"
date: 2018-01-31T09:08:00+01:00
draft: false
author: "Margo Diricks"
scriptlanguage: "xml"
attachment: "Scripts and changes to power assembler.docx"
---


A pipeline template for a resequencing assembly which also exports the non-mapped sequence reads.
The attached document describes the workflow to execute the pipeline. 



