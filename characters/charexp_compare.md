---
title: "charexp_compare"
date: 2019-06-05T15:36:00+01:00
draft: false
author: "Jan Deneweth"
scriptlanguage: "py"
---


Compares two character experiments, specified through dialog.
Search for value differences for multiple entries.

Reports:
    - For each character, % of entries with difference
    - All entries with discrepency (per character)

