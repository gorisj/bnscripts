---
title: "FAME_import_from_Excel_adjusted"
date: 2007-07-20T17:05:34+01:00
draft: false
author: "Jill Dombrecht"
scriptlanguage: "bns"
---


The script will import FAME profiles from an Excel file obtained with Sherlock 6.0. See attached example.

Example data: [FAME_Data_Yajun.xls](../FAME_Data_Yajun.xls)


