---
title: "copy_simil_to_matrix"
date: 2018-03-07T15:52:03+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "bns"
---


Copies a similarity matrix to a matrix experiment type

1. Create matrix type in the databases consisting of the name of the experiment type followed by _mtr (eg. Spa-typing -> Spa-typing_mtr). 
2. Calculate a matrix for the experiment type in the comparison window. 
3. Make sure the experiment type is selected and run the script.
4. The script looks for the corresponding matrix type and copies the similarity matrix to the matrix type if present in the database.
