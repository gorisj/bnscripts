import bns
import csv
import os
import time

Dlg = bns.Windows.XmlDlgBuilder
MessageBox = bns.Util.Program.MessageBox

class TransposeDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'TransposeDlg')
		self.path = ""
		self.outputFile = ""
		self.delimiter = '\t'
		self.inputRow = 0
		self.inputColumns = 0
		
		exts = [Dlg.Extension("txt", "Tab separated file"),
		        Dlg.Extension("csv", "Comma separated file"),
		        Dlg.Extension("*", "Any file")]
		self.selFile = Dlg.File('f1', exts, 30, "Select a file")

		grid = [[Dlg.StaticText("Select the file that you want to transpose. The file has to be tab or comma delimited.", maxWidth=50)], 
		        [self.selFile]]
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Select files", simple)
	
	def onOk(self, args):
		"""Get the path"""
		self.path = self.selFile.GetValue()

	def openFile(self, path):
		"""open tab of comma delimitted file and return list of lists
		
		Argument:
			- path: string, location of the inputfile (tab or comma delimited data)
		Returns:
			list of lists
		"""
		self.outputFile = "{0}_transposed{1}".format(*os.path.splitext(path))
		try:
			with open(path) as f:
				dialect = csv.Sniffer().sniff(f.read())
				self.delimiter = dialect.delimiter
				f.seek(0)
				data = f.readlines()
		except Exception as e:
			MessageBox("Error", str(e), 'exclamation')
			args['result'] = '0'
		return [row.strip().split(self.delimiter) for row in data]

		
	def writeFile(self, result):
		"""Writes a delimited file from list of lists
		Argument:
			- result: list of lists
		"""
		with open(self.outputFile, 'w') as f:
			for line in result:
				f.write(self.delimiter.join(line) + '\n')
				
	def transpose(self, matrix):
		"""transpose m x n matrix to n x m matrix
		
		Argument:
			- matrix: list of lists representing m x n matrix
		Returns:
			- list of lists, representing n x m matrix
		"""
		self.inputRows = len(matrix)
		self.inputColumns = len(matrix[0])
		return zip(*matrix)
		
def main(setbusy):
	"""Main function"""
	dlg = TransposeDlg()
	if dlg.Show():
		try:
			startTime = time.clock()
			setbusy("Transposing...")
			matrix = dlg.openFile(dlg.path)
			transposed = dlg.transpose(matrix)
			dlg.writeFile(transposed)
			MessageBox("Success", "Transposed a {0} x {1} matrix in {2:.2f} seconds.\n\n".format(
			                       dlg.inputRows, dlg.inputColumns, (time.clock() - startTime)) + \
			                      "The outputfile '{0}' can be found in\n"
			                      "the same directory as the input file.".format(os.path.basename(dlg.outputFile)), 
			                      'exclamation')
		except Exception as e:
			MessageBox("Error", str(e), 'exclamation')
		finally:
			setbusy("")
	
if __name__ == '__main__':
	# testdata in C:\TFS\PythonCode\DEV\CustomerCode\General\internal\Transpose_testfiles
	main( __bnscontext__.SetBusy)
		
		
