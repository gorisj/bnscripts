"""ImportCharacterInfo.py

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Import characters and character information fields from a tab-delimited file

The file is expercted to have 
* a header row
* the character names in the first column
* the character information fields in all the other columns

Newly created characters will have a default range of 0.0 - 100.0
 (except for binary character types with have a range of 0.0 - 1.0)

When new character information fields are imported, it may be necessary to re-open the experiment window

"""

#//$MENU window=chartype;popup=characters;insertafter=Add array of characters...;name=Import characters and character information...
import os
import bns

Dlg = bns.Windows.XmlDlgBuilder
MessageBox = bns.Util.Program.MessageBox
ErrorBox = lambda e: MessageBox("Error", '{0}'.format(e), 'exclamation')

def GetFileContent(filename):
	fl = open(filename, 'r')
	data = fl.read()
	fl.close()
	return data

def GetFileContentTable(filename):
	data = GetFileContent(filename)
	return [row.split('\t') for row in data.split('\n') if row.strip()]

class ImportCharacterInfoDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'ImportCharacterInfoDlg', remembersettings='global')
		
		self.chrData = None
		
		exts = [Dlg.Extension('*', "Tab-delimited text file")]
		self.chrInfoFileInput = Dlg.File('chrFile', exts, 30, "Select character info file", rememberDir=True, mustExist=True, hasfocus=True)
		
		grid = [
				["Select character info file:"],
				[self.chrInfoFileInput],
			]
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Import characters and character inforamtion", simple)
	
	def ShowError(self, e, args):
		ErrorBox(e)
		args['result'] = '0'
	
	def onOk(self, args):
		fileName = self.chrInfoFileInput.GetValue()
		if not fileName:
			return self.ShowError("Please provide a file name", args)
		if not os.path.isfile(fileName):
			return self.ShowError("Invalid file name", args)
		data = GetFileContentTable(fileName)
		if len(data) <= 1:
			return self.ShowError("Invalid file format, expecting at least two rows.", args)
		if len(data[0]) < 1:
			return self.ShowError("Invalid file format, expecting at least one column.", args)
		if any(len(row) != len(data[0]) for row in data):
			return self.ShowError("Invalid file format, expecting a full table.", args)
		d = dict((fld.lower(), fld) for fld in data[0])
		if "" in d:
			return self.ShowError("Some file headers are empty.", args)
		if len(d) < len(data[0]):
			return self.ShowError("There are duplicate file headers.", args)
		d = dict((chr[0].lower(), chr[0]) for chr in data[1:])
		if "" in d:
			return self.ShowError("Some character names are empty.", args)
		if len(d) < (len(data) - 1):
			return self.ShowError("There are duplicate character names.", args)
		self.chrData = data

def ImportCharacterInfo(experName, chrData):
	experType = bns.Database.ExperimentType(experName)
	if 'Binary="1"' in experType.Settings:
		maxRange = 1.0
	else:
		maxRange = 100.0
	
	charType = bns.Characters.CharType(experName)
	charSetType = bns.Characters.CharSetType(experName)
	
#	# set existing characters to inactive
#	for charNr in xrange(charSetType.GetCount()):
#		charSetType.ActChar(charNr, 0)
	
	header = chrData[0]
	characters = chrData[1:]
	
	# add custom character fields
	charTypeFlds = set(charType.FldName(idx).lower() for idx in xrange(charType.FldCount()))
	for fld in header[1:]:
		if fld.lower() not in charTypeFlds:
			charType.FldAdd(fld)
	
	# create characters and set fields
	charSetType.NoSave()
	for row in characters:
		charName = row[0]
		charNr = charSetType.FindChar(charName)
		if charNr < 0:
			charSetType.AddChar(charName, maxRange)
		else:
			charSetType.ActChar(charNr, 1)
		for field, content in zip(header[1:], row[1:]):
			charType.FldSet(charName, field, content)
	charSetType.Save()

def main(setbusy, cmdLine):
	chrExper, charName = cmdLine.split('\t')
	
	dlg = ImportCharacterInfoDlg()
	if not dlg.Show():
		return
	
	setbusy("Importing character information...")
	try:
		ImportCharacterInfo(chrExper, dlg.chrData)
	finally:
		setbusy("")

if __name__ == '__main__':
	main(__bnscontext__.SetBusy, __bnsargument__)


