#//$menu window=main;popup=Internal Scripts;insertpopupafter=Scripts;name=Compare character experiments...;
"""
Compare two character experiments, specified through dialog.
Search for value differences for multiple entries.
Report:
    - For each character, % of entries with difference
    - All entries with discrepency (per character)

Requested by Margo Diricks for GenotypingMTBC plugin work.

Author: Jan Deneweth
Date: 02/03/2019
"""

import os

import bns
Dlg = bns.Windows.XmlDlgBuilder

# Global Variables
DATABASE_DIR = bns.Database.Db.Info.Path
REPORT_PATH_BASE = os.path.join(DATABASE_DIR, "charexp_comparison_reports")
CSS_REPORT_STYLE = """
html {
    font-family: Arial, "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    font-size: 10pt;
    line-height: 1.4em;
    background: #f8f8f8;
    padding: 4em 2em;
    /*color: #039;*/
}
body > header h1 {
    font-size: 2.18em;
    border-bottom: 1pt solid black;
}
table {
    border-width: 1px 0;
    border-style: solid;
    border-color: #666;
    border-collapse: collapse;
    border-spacing: 0;
}
thead th {
    border-bottom: 1px solid #888;
    background: #ccc;
}
td, th {
    text-align: left;
    padding: 0.20em 0.5em;
    vertical-align: top;
    min-width: 6.5em;
}
tr:nth-child(even) {
    background: #ddd;
}
"""
HTML_REPORT_BASE = """
<html>
    <head>
        <style>
            {css_style}
        </style>
    </head>
    </body>
        <h1>Comparison of '{charexp_a}' and '{charexp_b}'</h1>
        <p>Compared {useable_entries} (/{total_entries}) entries with data in both experiments, across {mutual_chars} mutual characters</p>
        <p>A grand total of {grand_total_mismatches} mismatches was detected.</p>
        {html_table}
    </body>
</html>
"""  # Taken from genotyping plugin reports


# -- Selection Dialog --

class CharExpSelectDlg(Dlg.Dialogs):

    def __init__(self):
        """
        Create a dialog for selecting two character experiments
        """
        super(CharExpSelectDlg, self).__init__('DropListDlg')

        # Get character experiment names
        self.exp_dict = {exp.DispName: exp for exp in bns.Database.Db.ExperimentTypes if exp.Class == "CHR"}
        exp_names = sorted(list(self.exp_dict.keys()))

        # member variables to hold the results
        self.exp_a = None
        self.exp_b = None

        # the list controls
        self.exp_a_drop = Dlg.Drop('exp_a', exp_names, 10, 30, canEdit=False)
        self.exp_b_drop = Dlg.Drop('exp_b', exp_names, 10, 30, canEdit=False)

        # now define the dialog layout
        grid = [
            ["Character experiment A", self.exp_a_drop],
            ["Character experiment B", self.exp_b_drop],
        ]
        simple = Dlg.SimpleDialog(grid, onStart=self.on_start, onOk=self.on_ok)
        self.AddSimpleDialog("Select experiments", simple)

    def on_start(self, args):
        """Set the selected items"""
        self.exp_a_drop.SetValue("")
        self.exp_b_drop.SetValue("")

    def on_ok(self, args):
        """Get the selected items"""
        self.exp_a = self.exp_dict[self.exp_a_drop.GetValue()]
        self.exp_b = self.exp_dict[self.exp_b_drop.GetValue()]


# -- Script Execution --

def main():
    """Ask for and compare two character experiment values."""

    # Make sure that entries were selected
    entries = bns.Database.Db.Selection
    if len(entries) == 0:
        raise RuntimeError("No entries selected.")

    # Get the Character Experiment objects
    selection_dlg = CharExpSelectDlg()
    if not selection_dlg.Show():
        return  # Cancelled out
    charexp_a = selection_dlg.exp_a
    charexp_b = selection_dlg.exp_b
    if not (charexp_a and charexp_b):
        return  # Invalid input (f.e. dialog error)

    # Get data for entries and experiments
    charset = bns.Characters.CharSet()
    useable_entries = list(entries)
    charexp_a_entrydata = dict()
    charexp_b_entrydata = dict()
    for entry in entries:
        for exp, data_dict in ((charexp_a, charexp_a_entrydata), (charexp_b, charexp_b_entrydata)):
            vals_list = []
            charset.Load(entry.Key, exp.ID)
            charset.GetData(vals_list)
            # Remove entry if data is missing in any experiment
            if vals_list:
                data_dict[entry.Key] = vals_list
            elif entry in useable_entries:
                useable_entries.remove(entry)

    # Check if any useable entries remain
    if len(useable_entries) == 0:
        raise RuntimeError("No entries have data in both experiments.")

    # Get experiment metadata
    charexp_a_chars = []
    charexp_b_chars = []
    for exp, chars_list in ((charexp_a, charexp_a_chars), (charexp_b, charexp_b_chars)):
        charsettype = bns.Characters.CharSetType(exp.ID)
        for char_nr in range(charsettype.GetCount()):
            chars_list.append(charsettype.GetCharDispName(char_nr))
    mutual_chars = {char for char in charexp_a_chars if char in charexp_b_chars}
    mutual_charmapping = {char: (charexp_a_chars.index(char), charexp_b_chars.index(char)) for char in mutual_chars}  # Character name -> (index in A, index in B)

    # Compare characters and build report
    entries_total = len(useable_entries)
    mismatches_grand_total = 0
    html_table = "<table><tr><th>Character</th><th>Mismatch (%)</th><th>Mismatched entries</th></tr>"
    report_row_base = "<tr><th>{char}</th><th>{mismatch_perc:.0%}</th><th>{mismatch_entries}</th></tr>"
    for char, indexes in mutual_charmapping.items():
        # Get character comparison results
        a_index, b_index = indexes
        mismatches_total = 0
        mismatching_entries = []
        for entry in useable_entries:
            exp_a_data, exp_b_data = charexp_a_entrydata[entry.Key], charexp_b_entrydata[entry.Key]
            val_a, val_b = exp_a_data[a_index], exp_b_data[b_index]
            # Detect value mismatches
            if val_a != val_b:
                mismatches_grand_total += 1
                mismatches_total += 1
                mismatching_entries.append(entry.Key)
        mismatch_perc = float(mismatches_total)/float(entries_total)
        # Report mismatches
        if mismatches_total > 0:
            mismatching_entries_str = ", ".join(mismatching_entries)
            html_table += report_row_base.format(char=char, mismatch_perc=mismatch_perc, mismatch_entries=mismatching_entries_str)
    html_table += "</table>"

    # Build full report
    html_report = HTML_REPORT_BASE.format(
        total_entries=len(entries), useable_entries=len(useable_entries), mutual_chars=len(mutual_chars),
        charexp_a=charexp_a.Name, charexp_b=charexp_b.Name, html_table=html_table, css_style=CSS_REPORT_STYLE,
        grand_total_mismatches=mismatches_grand_total
    )

    # Write report
    if not os.path.exists(REPORT_PATH_BASE):
        os.makedirs(REPORT_PATH_BASE)
    report_path = os.path.join(REPORT_PATH_BASE, "report_{0}_{1}.html".format(charexp_a.Name, charexp_b.Name))
    with open(report_path, 'w') as fh:
        fh.write(html_report)

    # Open report
    os.startfile(report_path)


if __name__ == '__main__':
    main()


#
#
# END OF FILE
