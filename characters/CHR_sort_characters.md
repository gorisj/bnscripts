---
title: "CHR_sort_characters"
date: 2012-01-31T11:16:56+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "py"
---


MENU: Character experiment type window > Characters > Sort characters…

Sort characters according to an information field in ascending or descending order. Optionally, sort numerically.

