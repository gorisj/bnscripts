#//$autorun;event=CreateMainWin

import bns
import itertools
import collections
import math
Dlg = bns.Windows.XmlDlgBuilder

## index of diversity functions
indexFactory = []
def si_corr(counts):
	total = sum(counts)
	if total < 2:
		return 1.0
	
	denom = 1.0 * total * (total - 1)
	return sum(c * (c - 1) for c in counts) / denom

def si(counts):
	total = sum(counts)
	return sum(c * c for c in counts) / float(total * total)

def _s(counts, l):
	if len(counts) == 1:
		return 0.0
	
	itotal = 1.0 / sum(counts)
	props = (c * itotal for c in counts)
	return - sum(p * l(p) for p in props)

def sh(counts):
	return _s(counts, math.log10)

def sw(counts):
	return _s(counts, math.log)

class diHunterGaston(object):
	ID = 'diHunterGaston'
	name = "Diversity index (Hunter-Gaston)"
	def __call__(self, counts):
		return si_corr(counts)
indexFactory.append(diHunterGaston())

class diSimpson(object):
	ID = 'diSimpson'
	name = "Diversity index (Simpson)"
	def __call__(self, counts):
		return si(counts)
indexFactory.append(diSimpson())

class diShannon(object):
	ID = 'diShannon'
	name = "Diversity index (Shannon)"
	def __call__(self, counts):
		return sh(counts)
indexFactory.append(diShannon())

class diShannonWiener(object):
	ID = 'diShannonWiener'
	name = "Diversity index (Shannon-Wiener)"
	def __call__(self, counts):
		return sw(counts)
indexFactory.append(diShannonWiener())

class diHill0(object):
	ID = 'diHill0'
	name = "Diversity index (Number of groups)"
	def __call__(self, counts):
		return len(counts)
indexFactory.append(diHill0())

class diHill1(object):
	ID = 'diHill1'
	name = "Diversity index (Hill 1)"
	def __call__(self, counts):
		return math.exp(sw(counts))
indexFactory.append(diHill1())

class diHill2(object):
	ID = 'diHil2'
	name = "Diversity index (Hill 2)"
	def __call__(self, counts):
		i = si(counts)
		if i == 0:
			return 0
		else:
			return 1.0 / i
indexFactory.append(diHill2())

class diGiniSimpson(object):
	ID = 'diGiniSimpson'
	name = "Diversity index (Gini-Simpson)"
	def __call__(self, counts):
		return 1.0 - si(counts)
indexFactory.append(diGiniSimpson())

class diBergerParker(object):
	ID = 'diBergerParker'
	name = "Diversity index (Berger-Parker)"
	def __call__(self, counts):
		return 1.0 / max(counts) 
indexFactory.append(diBergerParker())

class diBrillouin(object):
	ID = 'diBrillouin'
	name = "Diversity index (Brillouin)"
	def __call__(self, counts):
		if len(counts) == 1:
			return 0
		
		total = sum(counts)
		itotal = 1.0 / total
		fact = math.factorial(total)
		prod = reduce(lambda x, y: x * y, counts, 1.0)
		return itotal * math.log(fact/prod)
indexFactory.append(diBrillouin())

class srMargalef(object):
	ID = 'srMargalef'
	name = "Species richness (Margalef)"
	def __call__(self, counts):
		S = len(counts)
		n = sum(counts)
		if n == 1:
			return 0
		return (S - 1) / math.log(n)
indexFactory.append(srMargalef())

class srMenhinick(object):
	ID = 'srMenhinick'
	name = "Species richness (Menhinick)"
	def __call__(self, counts):
		S = len(counts)
		n = sum(counts)
		return S / math.sqrt(n)
indexFactory.append(srMenhinick())

class srChao1(object):
	ID = 'srChao1'
	name = "Species richness (Chao1)"
	def __call__(self, counts):
		S = len(counts)
		n1 = counts.count(1)
		n2 = counts.count(2)
		denom = 2 * (n2+1)
		return S + n1*(n1-1) / denom
indexFactory.append(srChao1())

class srJack1(object):
	ID = 'srJack1'
	name = "Species richness (First-order jacknife)"
	def __call__(self, counts):
		S = len(counts)
		n1 = counts.count(1)
		return S + n1
indexFactory.append(srJack1())

class srJack2(object):
	ID = 'srJack2'
	name = "Species richness (Second-order jacknife)"
	def __call__(self, counts):
		S = len(counts)
		n1 = counts.count(1)
		n2 = counts.count(2)
		return S + 2*n1 - n2
indexFactory.append(srJack2())

class sePielou(object):
	ID = 'sePielou'
	name = "Species evenness (Pielou)"
	def __call__(self, counts):
		S = len(counts)
		if S == 1:
			return 0
		return sw(counts) / math.log(S)
indexFactory.append(sePielou())

class seHeip(object):
	ID = 'seHeip'
	name = "Species evenness (Heip)"
	def __call__(self, counts):
		S = len(counts)
		if S == 1:
			return 0
		H = sw(counts)
		return (math.exp(H) - 1) / (S - 1)
indexFactory.append(seHeip())

class seSheldon(object):
	ID = 'seSheldon'
	name = "Species evenness (Sheldon)"
	def __call__(self, counts):
		S = len(counts)
		H = sw(counts)
		return math.exp(H) / S
indexFactory.append(seSheldon())

class seHill(object):
	ID = 'seHill'
	name = "Species evenness (Hill)"
	def __call__(self, counts):
		i = si(counts)
		if i == 0:
			return 0
		return 1.0 / (i * math.exp(sw(counts)))
indexFactory.append(seHill())

class seHillmond(object):
	ID = 'seHillmond'
	name = "Species evenness (Hillmond)"
	def __call__(self, counts):
		i1 = si(counts)
		if i1 == 0:
			return 0
		i2 = sw(counts)
		if i2 == 0:
			return 0
		return ((1.0 / i1) - 1) / (math.exp(i2) - 1)
indexFactory.append(seHillmond())

class DiversityIndexChartsAndStatisticsWnd(object):
	def __init__(self, indexes, results, data):
		self.data = data
		dataSetID = 'IoD'
		dataSetName = 'Index of diversity'
		dataSetGroup = 'Index of diversity'
		self.dataSet = bns.DataSets.DataSetDefinition(dataSetID, dataSetID, dataSetID, dataSetName, dataSetGroup)
		self.dataSetItf = self.dataSet.Interface
		
		charFields = self.data.GetCharFieldNames()
		self.dataSet.AddProperty('String', 'name', "Character name", '', 1)
		self.dataSet.AddProperty('String', 'dispname', "Character display name", '', 0)
		if charFields:
			self.dataSet.AddProperty('Category', 'infofields', "Information fields", '', 0)
		self.dataSet.AddProperty('Category', 'diversities', "Diversity indexes", '', 0)
		for field in charFields:
			self.dataSet.AddProperty('String', 'field_{0}'.format(field), field, 'infofields', 0)
		for idx in indexes:
			self.dataSet.AddProperty('Value', idx.ID, idx.name, 'diversities', 0)
		
		self.handlingCharChanged = False
		self.dataSetItf.SetRecordSelChangedNotification(self.HandleChartSelChanged, {})
		
		self.dataSet.SetData('name', [self.data.GetCharName(charNr) for charNr in xrange(self.data.charCount)], 0)
		self.dataSet.SetData('dispname', [self.data.GetCharDispName(charNr) for charNr in xrange(self.data.charCount)], 0)
		for fieldNr, fieldName in enumerate(charFields):
			fieldContent = [self.data.GetCharFieldContent(charNr, fieldNr) for charNr in xrange(self.data.charCount)]
			self.dataSet.SetData('field_{0}'.format(fieldName), fieldContent, 0)
		for i, idx in enumerate(indexes):
			self.dataSet.SetData(idx.ID, [r[i] for r in results], 0)
		self.dataSet.SetDataModified()
		
		for charNr in xrange(self.data.charCount):
			self.dataSetItf.SetRecordSel(charNr, self.data.GetCharSel(charNr))
		
		self.wnd = self.dataSetItf.CreateChartWindow()
		cmpChrChartWindows[bns.Windows.BnsWindow.GetLastCreatedID()] = self
		
		chartType = 'BarGraph2D'
		chartName = indexes[0].name
		chartComponents = {'label': 'dispname', 'value': indexes[0].ID, 'sort': indexes[0].ID}
		chartOptions = {}
		self.wnd.CreateChart(chartType, chartName, chartComponents, chartOptions)
		self.wnd.Refresh()
	
	def HandleChartSelChanged(self, args):
		if self.handlingCharChanged:
			return
		
		self.handlingCharChanged = True
		for charNr in xrange(self.data.charCount):
			self.data.SetCharSel(charNr, self.dataSetItf.GetRecordSel(charNr))
		self.data.UpdateCharSel()
		self.handlingCharChanged = False
	
	def HandleCharSelChanged(self, args):
		if self.handlingCharChanged:
			return
		
		self.handlingCharChanged = True
		for charNr in xrange(self.data.charCount):
			self.dataSetItf.SetRecordSel(charNr, self.data.GetCharSel(charNr))
		self.dataSetItf.UpdateRecordSel()
		self.handlingCharChanged = False


## Dialog boxes
dialogFactory = []
class GetDiversityIndexDlg(Dlg.Dialogs):
	_id = 'CmpChrGetDiversityIndexDlg'
	_title = "Index of diversity"
	_description = "Compute index of diversity for characters and open a Charts and Statistics window."
	def __init__(self):
		Dlg.Dialogs.__init__(self, self._id)
		self.diversityIndexes = None
		self.selectionOnly = None
		self.ignoreAbsent = None
		self.diversityDict = dict((idx.ID, idx) for idx in indexFactory)

		items = [Dlg.ListItem(idx.name, idx.ID) for idx in indexFactory]
		values = Dlg.ListValues(items, translate=True)
		self._inputDiversity = Dlg.SimpleList('diversity', values, 10, 35, multi=True, useChecks=True, multiHighlight=True, hasfocus=True)
		self._checkSelectionOnly = Dlg.Check('selectionOnly', "Selected entries only", default=1)
		self._checkIgnoreAbsent = Dlg.Check('ignoreAbsent', "Ignore absent values", default=1)
		
		grid1 = [["Diversity index:", self._inputDiversity]]
		grid2 = [[self._checkIgnoreAbsent], [self._checkSelectionOnly]]
		grid = [[grid1], [grid2]]
		
		validations = [Dlg.ThrowValidate(self._Validate)]
		simple = Dlg.SimpleDialog(grid, onOk=self._onOk, validations=validations)
		
		self.AddSimpleDialog(self._title, simple)
	
	def _Validate(self):
		if not self._inputDiversity.Multi:
			raise RuntimeError("Please select at least one diversity index.")
	def _onOk(self, args):
		self.diversityIndexes = [self.diversityDict[idx] for idx in self._inputDiversity.Multi]
		self.ignoreAbsent = self._checkIgnoreAbsent.Checked
		self.selectionOnly = self._checkSelectionOnly.Checked
dialogFactory.append(GetDiversityIndexDlg)

class _FilterCharactersDlg(Dlg.Dialogs):
	_id = None
	_title = None
		
	def __init__(self):
		Dlg.Dialogs.__init__(self, self._id)
		self.threshold = None
		self.selectionOnly = None
		self.switchAspect = None
		
		self._inputThreshold = Dlg.Input('threshold', 10, default=100.0, hasfocus=True)
		self._checkSelectionOnly = Dlg.Check('selectionOnly', "Selected entries only", default=1)
		self._checkSwitchAspect = Dlg.Check('switchAspect', "Change to <Selected characters> aspect", default=1)
		
		grid1 = []
		self.AddThresholdRow(grid1, self._inputThreshold)
		grid2 = [[self._checkSelectionOnly], [self._checkSwitchAspect]]
		self.AddChecks(grid2)
		grid = [[grid1], [grid2]]
		
		validations = [Dlg.ThrowValidate(self._Validate)]
		simple = Dlg.SimpleDialog(grid, onOk=self._onOk, validations=validations)
		
		self.AddSimpleDialog(self._title, simple)
	
	def AddThresholdRow(self, grid, inputthreshold):
		grid.append([inputThreshold])
	def AddChecks(self, grid):
		pass
	def Validate(self):
		pass
	def _Validate(self):
		th = self._inputThreshold.GetValue()
		try:
			self.threshold = float(th) / 100.0
			if not (0.0 <= self.threshold <= 1.0):
				raise ValueError("Invalid percentage")
		except ValueError:
			raise RuntimeError("Please provide a valid percentage.")
		self.Validate()
	def onOk(self, args):
		pass
	def _onOk(self, args):
		self.selectionOnly = self._checkSelectionOnly.Checked
		self.switchAspect = self._checkSwitchAspect.Checked
		self.onOk(args)

class GetAbsentDlg(_FilterCharactersDlg):
	_id = 'CmpChrGetAbsentDlg'
	_title = "Select absent characters"
	_description = "Select absent characters"
	def AddThresholdRow(self, grid, inputThreshold):
		grid.append(["At least", inputThreshold, "% of the values are absent."])
dialogFactory.append(GetAbsentDlg)

class GetPresentDlg(_FilterCharactersDlg):
	_id = 'CmpChrGetPresentDlg'
	_title = "Select present characters"
	_description = "Select present characters"
	def AddThresholdRow(self, grid, inputThreshold):
		grid.append(["At least", inputThreshold, "% of the values are present."])	
dialogFactory.append(GetPresentDlg)

class _GetMorphicDlg(_FilterCharactersDlg):
	def __init__(self):
		self.ignoreAbsent = None
		self._checkIgnoreAbsent = Dlg.Check('ignoreAbsent', "Ignore absent values", default=1)
		_FilterCharactersDlg.__init__(self)
	def AddChecks(self, grid):
		grid.insert(0, [self._checkIgnoreAbsent])
	def onOk(self, args):
		self.ignoreAbsent = self._checkIgnoreAbsent.Checked

class GetPolymorphicDlg(_GetMorphicDlg):
	_id = 'CmpChrGetPolymorphicDlg'
	_title = "Select polymorphic characters"
	_description = "Select polymorphic characters"
	def AddThresholdRow(self, grid, inputThreshold):
		grid.append(["Less than", inputThreshold, "% of the values are the same."])
dialogFactory.append(GetPolymorphicDlg)

class GetMonomorphicDlg(_GetMorphicDlg):
	_id = 'CmpChrGetMonomorphicDlg'
	_title = "Select monomorphic characters"
	_description = "Select monomorphic characters"
	def AddThresholdRow(self, grid, inputThreshold):
		grid.append(["At least", inputThreshold, "% of the values are the same."])
dialogFactory.append(GetMonomorphicDlg)

## Character filtering
def _IsPolymorph(t, counts, th):
	return bool(counts) and (max(counts) < th)

def _IsMonomorph(t, counts, th):
	return bool(counts) and (max(counts) >= th)

def _IsAbsent(t, c, th):
	return (t - c) >= th

def _IsPresent(t, c, th):
	return c >= th

class CharExperimentData(object):
	def __init__(self, winId):
		self._wnd = bns.Windows.BnsWindow(winId)
		self._data = bns.Comparison.ExperimentData(winId)
		self._experNr = self._data.GetSelExperNr()
		if (self._experNr < 0) or (self._data.GetExperClass(self._experNr) != 'CHR'):
			raise RuntimeError("Please select a character experiment type.")
		self._aspectNr = self._data.GetSelAspectNr(self._experNr)
		
		self.charCount = self._data.GetExperCharCount(self._experNr, self._aspectNr)
		
	def GetEntryList(self, selectedOnly=True):
		entryCount = self._data.GetEntryCount()
		if selectedOnly:
			return [entryNr for entryNr in xrange(entryCount) if bns.Database.Entry(self._data.GetEnKey(entryNr)).Selection]
		else:
			return range(entryCount)
	
	def GetExperName(self):
		return self._data.GetExperName(self._experNr)
	def GetCharName(self, charNr):
		return self._data.GetExperCharName(self._experNr, charNr, self._aspectNr)
	def GetCharDispName(self, charNr):
		return self._data.GetExperCharDispName(self._experNr, charNr, self._aspectNr)
	def GetCharFieldNames(self):
		return self._data.GetExperCharFieldNames(self._experNr, self._aspectNr)
	def GetCharFieldContent(self, charNr, fieldNr):
		return self._data.GetExperCharField(self._experNr, charNr, fieldNr, self._aspectNr)
	
	def SetSelAspect(self, aspect):
		self._data.SetCurrentAspect(self._experNr, aspect)
		self._aspectNr = self._data.GetSelAspectNr(self._experNr)
		self.charCount = self._data.GetExperCharCount(self._experNr, self._aspectNr)
		
	def GetCharValues(self, entryNr):
		values = []
		self._data.GetExperCharData(self._experNr, entryNr, values, self._aspectNr)
		return values
	
	def GetCharPresences(self, entryNr):
		presences = []
		self._data.GetExperCharPresent(self._experNr, entryNr, presences, self._aspectNr)
		return presences
	
	def GetCharData(self, entryNr):
		values = self.GetCharValues(entryNr)
		presences = self.GetCharPresences(entryNr)
		return [v if p else None for v, p in itertools.izip(values, presences)]
	
	def GetCharSel(self, charNr):
		return self._data.GetExperCharSel(self._experNr, charNr, self._aspectNr)
	def SetCharSel(self, charNr, selState):
		self._data.SetExperCharSel(self._experNr, charNr, selState, self._aspectNr)
	
	def UpdateCharSel(self):
		self._data.UpdateExperCharSel()
	
	def _FilterCharacters(self, selectFunc, selectedOnly, threshold, switchAspect):
		selection = []
		
		def Calc(args):
			calcComm = args['communication']
			calcComm.SetMessage("Collection character data...")
			
			presences = [0] * self.charCount
			entryList = self.GetEntryList(selectedOnly)
			for entryNr in entryList:
				if not calcComm.MustContinue():
					return
				for charNr, p in enumerate(self.GetCharPresences(entryNr)):
					presences[charNr] += p
			
			calcComm.SetMessage("Filtering characters...")
			entryCount = len(entryList)
			th = threshold * entryCount
			for charNr in xrange(self.charCount):
				if not calcComm.MustContinue():
					return
				select = selectFunc(entryCount, presences[charNr], th)
				selection.append(select)
		
		def Refresh(args):
			for charNr, select in enumerate(selection):
				self.SetCharSel(charNr, select)
			self.UpdateCharSel()
			if switchAspect:
				self.SetSelAspect('__Selected__')
		
		self._wnd.StartAsyncCalculation(Calc, Refresh, True)
		
	def _GetMorphicCharacters(self, IsMorphic, selectedOnly, threshold, ignoreAbsent, switchAspect):
		selection = []
		
		def Calc(args):
			calcComm = args['communication']
			calcComm.SetMessage("Collection character data...")
			
			references = [collections.defaultdict(int) for x in xrange(self.charCount)]
			entryList = self.GetEntryList(selectedOnly)
			for entryNr in entryList:
				if not calcComm.MustContinue():
					return
				for charNr, value in enumerate(self.GetCharData(entryNr)):
					references[charNr][value] += 1
			
			calcComm.SetMessage("Filtering characters...")
			absEntryCount = len(entryList)
			for charNr, reference in enumerate(references):
				if not calcComm.MustContinue():
					return
				if ignoreAbsent and None in reference:
					t = absEntryCount - reference[None]
					del reference[None]
				else:
					t = absEntryCount
				th = threshold * t
				select = IsMorphic(t, reference.values(), th)
				selection.append(select)
			
		def Refresh(args):
			for charNr, select in enumerate(selection):
				self.SetCharSel(charNr, select)
			self.UpdateCharSel()
			if switchAspect:
				self.SetSelAspect('__Selected__')
		
		self._wnd.StartAsyncCalculation(Calc, Refresh, True)

	def SelectClear(self):
		for charNr in xrange(self.charCount):
			self.SetCharSel(charNr, False)
		self.UpdateCharSel()

	def SelectAll(self):
		for charNr in xrange(self.charCount):
			self.SetCharSel(charNr, True)
		self.UpdateCharSel()
	
	def SelectInvert(self):
		for charNr in xrange(self.charCount):
			self.SetCharSel(charNr, not self.GetCharSel(charNr))
		self.UpdateCharSel()
	
	def GetSelected(self):
		return [self.GetCharName(charNr) for charNr in xrange(self.charCount) if self.GetCharSel(charNr)]
	
	def CreateView(self, viewName, viewMembers, switchAspect):
		charSetType = bns.Characters.CharSetType(self.GetExperName())
		viewID = charSetType.ViewCreateSubsetBased('', viewName)
		view = charSetType.ViewGet(viewID)
		view.SubsetAddCharacters(viewMembers)
		if switchAspect:
			self.SetSelAspect(viewID)
	
	def GetDiversityIndexes(self, DiversityIndexes, selectedOnly, ignoreAbsent):
		results = []
		
		def Calc(args):
			calcComm = args['communication']
			calcComm.SetMessage("Collection character data...")
			
			references = [collections.defaultdict(int) for x in xrange(self.charCount)]
			entryList = self.GetEntryList(selectedOnly)
			for entryNr in entryList:
				if not calcComm.MustContinue():
					return
				for charNr, value in enumerate(self.GetCharData(entryNr)):
					references[charNr][value] += 1
		
			calcComm.SetMessage("Computing diversity indexes...")
			for charNr, reference in enumerate(references):
				if not calcComm.MustContinue():
					return
				if ignoreAbsent and None in reference:
					del reference[None]
				counts = reference.values()
				result = [DiversityIndex(counts) for DiversityIndex in DiversityIndexes]
				results.append(result)
		
		def Refresh(arg):
			DiversityIndexChartsAndStatisticsWnd(DiversityIndexes, results, self)
		
		self._wnd.StartAsyncCalculation(Calc, Refresh, True)
			
	def GetPolymorphicCharacters(self, threshold=1.0, selectedOnly=True, ignoreAbsent=True, switchAspect=True):
		self._GetMorphicCharacters(_IsPolymorph, selectedOnly, threshold, ignoreAbsent, switchAspect)
			
	def GetMonomorphicCharacters(self, threshold=1.0, selectedOnly=True, ignoreAbsent=True, switchAspect=True):
		self._GetMorphicCharacters(_IsMonomorph, selectedOnly, threshold, ignoreAbsent, switchAspect)
	
	def GetAbsentCharacters(self, threshold=1.0, selectedOnly=True, switchAspect=True):
		self._FilterCharacters(_IsAbsent, selectedOnly, threshold, switchAspect)
	
	def GetPresentCharacters(self, threshold=1.0, selectedOnly=True, switchAspect=True):
		self._FilterCharacters(_IsPresent, selectedOnly, threshold, switchAspect)

## command handlers
def SelectPolymorphicCharacters(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	dlg = GetPolymorphicDlg()
	if not dlg.Show():
		return
	data.GetPolymorphicCharacters(dlg.threshold, dlg.selectionOnly, dlg.ignoreAbsent, dlg.switchAspect)

def SelectMonomorphicCharacters(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	dlg = GetMonomorphicDlg()
	if not dlg.Show():
		return
	data.GetMonomorphicCharacters(dlg.threshold, dlg.selectionOnly, dlg.ignoreAbsent, dlg.switchAspect)

def SelectAbsentCharacters(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	dlg = GetAbsentDlg()
	if not dlg.Show():
		return
	data.GetAbsentCharacters(dlg.threshold, dlg.selectionOnly, dlg.switchAspect)
	
def SelectPresentCharacters(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	dlg = GetPresentDlg()
	if not dlg.Show():
		return
	data.GetPresentCharacters(dlg.threshold, dlg.selectionOnly, dlg.switchAspect)

def SelectDiversityIndex(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	dlg = GetDiversityIndexDlg()
	if not dlg.Show():
		return
	data.GetDiversityIndexes(dlg.diversityIndexes, dlg.selectionOnly, dlg.ignoreAbsent)

def SelectClearCharacters(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	data.SelectClear()

def SelectInvertCharacters(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	data.SelectInvert()

def SelectAllCharacters(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	data.SelectAll()

class CreateCharacterViewDlg(Dlg.Dialogs):
	_id = 'CmpChrCreateViewDlg'
	_title = "New character view"
	_description = "Create new character view based on the currently selected characters."
	def __init__(self):
		Dlg.Dialogs.__init__(self, self._id)
		self.viewName = ''
		self.switchAspect = None
		
		self._inputViewName = Dlg.Input('viewName', 30, hasfocus=True, remembersettings=False)
		self._checkSwitchAspect = Dlg.Check('switchAspect', "Set as current aspect")
		grid1 = [["Name:", self._inputViewName]]
		grid2 = [[self._checkSwitchAspect]]
		grid = [[grid1], [grid2]]
		
		validations = [Dlg.ThrowValidate(self.ValidateName)]
		
		simple = Dlg.SimpleDialog(grid, validations=validations, onOk=self.onOk)
		self.AddSimpleDialog(self._title, simple)
		
	def ValidateName(self):
		if not self._inputViewName.GetValue():
			raise RuntimeError("Please provide a view name.")
		
	def onOk(self, args):
		self.viewName = self._inputViewName.GetValue()
		self.switchAspect = self._checkSwitchAspect.Checked
dialogFactory.append(CreateCharacterViewDlg)

def CreateCharacterView(args):
	winId = args['winid']
	data = CharExperimentData(winId)
	selected = data.GetSelected()
	if not selected:
		raise RuntimeError("No characters selected.")
	dlg = CreateCharacterViewDlg()
	if not dlg.Show():
		return
	data.CreateView(dlg.viewName, selected, dlg.switchAspect)

def IsChrTypeSelected(args):
	winId = args['winid']
	data = bns.Comparison.ExperimentData(winId)
	experNr = data.GetSelExperNr()
	args['result'] = (experNr > -1) and (data.GetExperClass(experNr) == 'CHR')

## global event handler for character selection changes
cmpChrChartWindows = {}
def CmpChrHandleCharSelChanged(args):
	bnsWinIds = set(bns.Windows.BnsWindow.GetID(nr) for nr in xrange(bns.Windows.BnsWindow.GetCount()))
	for winId in cmpChrChartWindows:
		if winId in bnsWinIds:
			cmpChrChartWindows[winId].HandleCharSelChanged(args)
		else:
			del cmpChrChartWindows[winId]


if __name__ == '__main__':
	# register Character selection change event handler
	bns.Util.Program.SetFunctorEvent(CmpChrHandleCharSelChanged, 'charselchanged')
	
	# register Comparison commands
	clsName = 'Comparison'
	pSectionId = 'Statistics'
	sectionId = 'CmpChrFilter'
	
	pSectionId2 = 'Characters'
	sectionId2 = 'CmpChrSelection'
	
	# character selection
	bns.Windows.BnsWindow.AddCustomSection(clsName, sectionId2, 'Select characters', pSectionId2, asSub=False)
	
	bmp = bns.Graphics.Bitmap()
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId2, 'CmpChrSelectAll', "Select all characters", "Select all characters in current aspect.", SelectAllCharacters, key='', bitmap=bmp, enable=IsChrTypeSelected)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId2, 'CmpChrSelectClear', "Clear character selection", "Clear character selection in current aspect.", SelectClearCharacters, key='', bitmap=bmp, enable=IsChrTypeSelected)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId2, 'CmpChrSelectInvert', "Invert character selection", "Invert character selection in current aspect.", SelectInvertCharacters, key='', bitmap=bmp, enable=IsChrTypeSelected)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId2, 'CmpChrCreateView', "Create character view...", "Create a character view from the selected characters in current aspect.", CreateCharacterView, key='', bitmap=bmp, enable=IsChrTypeSelected)
	
	# character statistics
	bns.Windows.BnsWindow.AddCustomSection(clsName, sectionId, 'Filter characters', pSectionId, asSub=False)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId, 'CmpChrFilterAbsent', "Select absent characters...", "Select absent characters in current aspect.", SelectAbsentCharacters, key='', bitmap=bmp, enable=IsChrTypeSelected)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId, 'CmpChrFilterPresent', "Select present characters...", "Select present characters in current aspect.", SelectPresentCharacters, key='', bitmap=bmp, enable=IsChrTypeSelected)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId, 'CmpChrFilterMonomorphic', "Select monomorphic characters...", "Select monomorphic characters in current aspect.", SelectMonomorphicCharacters, key='', bitmap=bmp, enable=IsChrTypeSelected)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId, 'CmpChrFilterPolymorphic', "Select polymorphic characters...", "Select polymorphic characters in current aspect.", SelectPolymorphicCharacters, key='', bitmap=bmp, enable=IsChrTypeSelected)
	bns.Windows.BnsWindow.AddCustomCommand(clsName, sectionId, 'CmpChrFilterDiversityIndex', "Character diversity indices...", "Compute diversity indices for characters in current aspect and open a Charts and Statistics window.", SelectDiversityIndex, key='', bitmap=bmp, enable=IsChrTypeSelected)
	
	# register dialog box lemmas
	for dlg in dialogFactory:
		Dlg.AddHelpLemma('DialogBox', dlg._id, '', dlg._title, dlg._description, 'dm', '', '2015/05/13')
	
	if __bnsdebug__:
		bns.Comparison.Comparison.Open('')
		winId = bns.Windows.BnsWindow.GetLastCreatedID()
		args = {'winid': winId}
		comp = bns.Comparison.Comparison(winId)
		comp.SetSelExper('wgMLST2')
		comp.ShowImage('wgMLST2', 1)
#		SelectMonomorphicCharacters(args)
#		SelectPolymorphicCharacters(args)
#		SelectAbsentCharacters(args)
#		SelectPresentCharacters(args)
#		SelectDiversityIndex(args)
		CreateCharacterView(args)
