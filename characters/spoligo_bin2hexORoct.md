---
title: "Spoligo_bin2hexORoct"
date: 2007-08-17T17:08:08+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---


This script will convert the 43 spoligo characters to a HEX or Octal code according to the paper from Dale et al, Int J Tuberc Lung dis 5(3):216-219, 2001.

Cutoff is an absolute value from which your characters should be counted as present. For binary charsets this should be 1. (=default value)

can be used in combination with import spoligo tiffs…

