---
title: "Import_matrix_data"
date: 2008-01-23T16:58:38+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---



Import matrix type data in a connected database.

Example data: [matrix_sample.txt](../matrix_sample.txt)

