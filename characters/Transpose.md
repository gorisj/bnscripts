---
title: "Transpose"
date: 2011-11-16T12:08:04+01:00
draft: false
author: "Jeroen Van Goey"
scriptlanguage: "py"
---


Transpose text file:
The script asks for a tab- or comma-delimited file, returns feedback whether or not the transpose action succeeded. The resulting file is saved to the same directory as the location of the input file.

