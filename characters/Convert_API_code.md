---
title: "Convert_API_code"
date: 2008-11-19T16:41:15+01:00
draft: false
author: "Jill Dombrecht"
scriptlanguage: "bns"
---



The information that is present in a database field (e.g. 157898468) is stored in a character type experiment. Each number will be linked to one character. If the information is linked to an existing character type, make sure the characters are defined for this character type experiment. A character type can be created when running the script.

