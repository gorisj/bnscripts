"""Sort characters by information field

CHARACTER TYPE WINDOW
  Sort characters according to an information field
  in ascending or descending order. Optionally, sort numerically.
"""

#//$MENU window=chartype;popup=characters;insertafter=Move character down;name=Sort characters...

import bns
dlgbuilder = bns.Windows.XmlDlgBuilder

def IsNumbers(lst):
	for f in lst:
		try:
			float(f)
		except ValueError:
			return False
	else:
		return True

# in the chartype window, the __bnsqrgument__ contains
#    the character type name and the currently selected character
#    separated by a tab
chr_exper, chr_name = __bnsargument__.split("\t", 1)

chr_type = bns.Characters.CharSetType(chr_exper)
chr_cnt = chr_type.GetCount()
if not chr_cnt:
	raise RuntimeError("No characters defined yet.")

chrs = tuple(chr_type.GetChar(c) for c in xrange(chr_cnt))

chr_info = bns.Characters.CharType(chr_exper)
fld_cnt = chr_info.FldCount()
flds = ("Character", ) + tuple(chr_info.FldName(f) for f in xrange(fld_cnt))
flds_content = (chrs, ) + tuple(tuple(chr_info.FldGetByNr(c, f) for c in xrange(chr_cnt)) for f in xrange(fld_cnt))
flds_isnumbers = tuple(IsNumbers(f) for f in flds_content)

class SortDlg(dlgbuilder.Dialogs):
	def __init__(self):
		dlgbuilder.Dialogs.__init__(self, "SortCharactersDlgs")
		
		values = dlgbuilder.ListValues([dlgbuilder.ListItem(f, nr) for nr, f in enumerate(flds)])
		self.fld = dlgbuilder.SimpleList("SortFld", values, 8, 30, multi=False)
		self.fld.default = dlgbuilder.ExplicitValue("0")
		self.fld.OnChange = self.__EnableNumFld
		
		values = dlgbuilder.ListValues([dlgbuilder.ListItem("Ascending order", "ascending"),
															dlgbuilder.ListItem("Descending order", "descending"),])
		self.order = dlgbuilder.Radio("SortOrder", values, 30, vertical=True)
		self.order.default = dlgbuilder.ExplicitValue("ascending")
		self.number = dlgbuilder.Check("SortNumeric", dlgbuilder.StaticText("Sort numerically"))
		self.number.default = dlgbuilder.ExplicitValue("1")
		
		grid = dlgbuilder.Grid([[dlgbuilder.Cell(dlgbuilder.Grid([[self.fld]]), "Select field to sort on"),
												 dlgbuilder.Cell(dlgbuilder.Grid([[self.order], [self.number]]), "Sorting settings")]])
		
		simple = dlgbuilder.SimpleDialog(grid, onStart=self.__EnableNumFld)
		dlg = dlgbuilder.Dialog("SortCharactersDlg", "Sort characters", simple)
		self.AddDialog(dlg)
		self.Show()
	def __EnableNumFld(self, args=None):
		self.number.SetValue(str(int(self.CheckNumFld())))
		self.number.Enabled = self.CheckNumFld()
	def CheckNumFld(self):
		return flds_isnumbers[self.GetFld()]
	def GetFld(self):
		return int(self.GetValue(self.fld.Id))
	def GetOrder(self):
		return self.GetValue(self.order.Id)
	def GetNumber(self):
		return int(self.CheckNumFld()) and int(self.GetValue(self.number.Id))

# START OF SCRIPT
dlg = SortDlg()
if dlg.IsOk():
	if dlg.GetNumber():
		fld_content = tuple(float(f) for f in flds_content[dlg.GetFld()])
	else:
		fld_content = flds_content[dlg.GetFld()]
	flds = zip(fld_content, flds_content[0])
	sort_flds = tuple(sorted(flds))
	if dlg.GetOrder() == "descending":
		sort_flds = tuple(reversed(sort_flds))
	ordering = list(chr for f, chr in sort_flds)
	chr_type.Orden(ordering)
