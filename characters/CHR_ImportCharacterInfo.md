---
title: "CHR_ImportCharacterInfo"
date: 2012-01-31T11:13:50+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "py"
---


Attached you can find two files. Copy both files to your [HOMEDIR]\Scripts Home\Python Home folder. The following menu items will appear in the character experiment type windows.

MENU: Character experiment type window > Characters > Import characters and character information…

This extra functionality will import characters and character information fields from a tab-delimited file text file. It is important that you create a file with the following format:

The file is expected to have 

- a header row
- the character names in the first column
- the character information fields in all the other columns

Newly created characters will have a default range of 0.0 - 100.0
(except for binary character types with have a range of 0.0 - 1.0)

When new character information fields are imported, it may be necessary to re-open the experiment window. Also check whether the new fields are visible by clicking on the gray arrow on the right side of all the columns: column properties > set active fields... and activate all the extra fields.

