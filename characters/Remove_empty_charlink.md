---
title: "Remove_empty_charlink"
date: 2009-11-03T16:06:34+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "py"
---


The script removes emtpy character card in a BioNumerics 6.0 database. 

1. Open the BioNumerics 6.0 database.
2. Select Scripts > Run script from file. 
3. Navigate to the path where the script is stored and select the script.
4. Select the character experiment from the list and press <OK>.
5. Close and re-open the database.

