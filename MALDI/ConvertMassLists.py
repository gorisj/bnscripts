"""ConvertMassLists.py
Join and convert mass list files to peak table files

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
! This script is provided "as is" by Applied Maths.              ! 
! You are free to use and modify this script for your own needs. ! 
! Redistribution or reproduction of the script is prohibited.    ! 
! DISCLAIMER:                                                    ! 
! Improper use of scripts may corrupt your database.             ! 
! Running this script is entirely at your own responsibility.    ! 
! Applied Maths accepts no lialibility for any consequences      ! 
! resulting from its use.                                        ! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

"""
#//$menu window=main;popup=File;insertafter=Manage import templates...;name=Join and convert mass list files...;
import os
import bns
MessageBox = bns.Util.Program.MessageBox
ErrorBox = lambda e: MessageBox("Error", '{0}'.format(e), 'exclamation')
Dlg = bns.Windows.XmlDlgBuilder

class SelectFilesDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'SelectFilesDlg', remembersettings='none')
		
		exts = [Dlg.Extension('txt', "Peak table file")]
		self.importFilesCtrl = Dlg.File('ImportFiles', exts, 40, "Select file(s) to import", multiple=True, mustExist=True)
		self.exportFileCtrl = Dlg.File('ExportFile', exts, 40, "Select export file", multiple=False, mustExist=False)
		grid = [["Import files:", self.importFilesCtrl],
		        ["Export file:", self.exportFileCtrl]]
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Join and convert peak tables", simple)
	
	def onOk(self, args):
		self.importFiles = self.importFilesCtrl.Multi
		self.exportFile = self.exportFileCtrl.GetValue()
		
		errors = []
		if not self.importFiles:
			errors.append("Please select file(s) to import.")
		if not self.exportFile:
			errors.append("Please provide an export file name.")
		elif os.path.isfile(self.exportFile):
			r = MessageBox("Confirmation", "The export file already exists.\n\nDo you want to overwrite this file?", 'question+yesno+defbutton2')
			if r != 1:
				args['result'] = '0'
				return
		else:
			try:
				open(self.exportFile, 'w').close()
			except IOError as e:
				errors.append("Invalid export file name: {0}".format(e))
		if errors:
			ErrorBox('\n'.join(errors))
			args['result'] = '0'
			return

def main(setbusy):
	dlg = SelectFilesDlg()
	if not dlg.Show():
		return
	
	inFiles = dlg.importFiles
	outFile = dlg.exportFile
	fp2 = open(outFile, 'w')
	try:
		fp2.write('SampleFile\tDye\tSize\tHeight\n')
		for flName in inFiles:
			name = os.path.splitext(os.path.split(flName)[1])[0]
			setbusy("Getting peaks from file {0}...".format(name))
			fp = open(flName, 'r')
			try:
				fp.readline() # skip header
				for l in fp:
					line = l.strip()
					if not line:
						return
					fp2.write('{0}\t\t{1}\n'.format(name, line))
			finally:
				fp.close()
	finally:
		fp2.close()

if __name__ == '__main__':
	main(__bnscontext__.SetBusy)


