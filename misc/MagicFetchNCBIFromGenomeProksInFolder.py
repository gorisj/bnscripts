import bns
from Bio import Entrez
import os
import gzip
from time import sleep

#define parameters for script
#most important parameter, this folder will be searched for files starting with genome_proks, create a subfolder per file, copy the file with prefix completed_ and download all assemblies to this folder
DownloadPath ="D:\SRRDownloads\NCBI" 

#GAFile=os.path.join(DownloadPath,"genomes_proks.csv")

# name of downloaded files, in case they change it
GAFileStart = "genomes_proks"


# headers from genome proks file used by the script, in case they change it
columnAcc = "Replicons"
columnWGS= "WGS"
RefSeqFtp= "RefSeq FTP"
GBFtp= "GenBank FTP"

#class to deal with the files containing the metadata of genome assemblies downloaded from NCBI (named either genome_proks.txt or .csv)
class genomeProks(object):
	
	def __init__(self,folder, GAFileStart):
		self.folder = folder
		self.GAFileStart = GAFileStart
		self.GAFiles = {}
		
	# parse genome assembly file (either as tab delimted txt or csv, delimited by ,) and store in dict with following structure [line number]:[metadatadict]. [metadict] is structured as followed [columnID]:[value in that line]
	# example: [1]:{Organism/Name:Vibrio mimicus}{Strain:ATCC 33654}...
	def parseGA(self,GAFile):
		if os.path.isfile(GAFile):
			with open(GAFile, 'r') as fl:
				sep = "\t" # use tab as default delimiter
				if "txt" in GAFile:
					sep = "\t"
				else:
					if "csv" in GAFile:
						sep = ","
				GAdict = {}	
				for i,line in enumerate(fl):
			
			#reading the headers and creating a list
					if i==0:
						headers = line.strip("\r\n").split(sep)
			
			#read line for sample and create list from values and then dict with key value pairs header:value
					else:
						values = line.strip("\r\n").split(sep)
						sampleDict = dict(zip(headers,values))
						GAdict[i]=sampleDict
				return GAdict
		else:
			return None

	#locate all files in the specified folder starting with genome_proks, look up the first organism and create a folder with this name as downloadpath
	def SearchGenomeProks(self):
		
		for name in os.listdir(self.folder):
			filename= os.path.join(self.folder,name)
			#if the file starts with genome_proks, we will open it, parse it and return the parsed dict with the Organism name on the first line as key, make a dir with this this downloadfolder and move the genome proks to it
			if name.startswith(self.GAFileStart):
				filename= os.path.join(self.folder,name)
				if os.path.isfile(filename):
					with open(filename,'rb') as fl:	
						try:
							GADict = self.parseGA(filename)	
							species = GADict[1]["#Organism/Name"]
							downloadpath = os.path.join(self.folder,species)
							if not os.path.exists(downloadpath):
								try:
									os.mkdir(downloadpath)
								except:
									raise RuntimeError("Unsufficient privileges to add subfolder 'Converted' in selected folder. Please create this folder manually.")
							self.GAFiles[downloadpath]=GADict
							fileCopy = os.path.join(downloadpath,"completed_" + name)
							with open(fileCopy,'w') as flC:
								flC.write(fl.read())
								fl.close()
								os.remove(filename)
								flC.close()
									
						except:
							pass
			if os.path.isdir(filename):
				
				#check for genome_proks in subfolders, unless the folder has been added as a downloadpath already
				if not self.GAFiles.has_key(filename):
					self.folder = filename
					self.SearchGenomeProks()
					
						
				
	
	


#NCBI only accepts 3 requests per second, so we need to add a delay
def delay_decorator(func, delay_default=None):
    """Decorate a function for added 'delay' functionality"""
    def delayed_func(*args, **kwargs):
        """
        Function with additional sleep delay.
        Uses method '{0}'.
    
        :param args: positional arguments for method
        :param kwargs: keyword arguments for method
        :param delay: time to delay after executing method (default {1})
        """.format(func.__name__, delay_default)

        # Get delay parameter
        delay = kwargs.pop('delay', delay_default)
        # Execute function
        results = func(*args, **kwargs)
        # Delay if requested
        if delay is not None:
            sleep(delay)
        # Return result
        return results

    return delayed_func



ncbi_urlgetfile = delay_decorator(bns.Util.UrlTools.urlgetfile)



		
#takes the complete ftp, downloads and unzips the file in case of gz
def downloadftp(ftpurl,filename,downloadPath):
	
	# if no filename is provided, revert to the original assembly ID
	if filename == "":
		filename = ftpurl.rpartition("/")[2]

	tmp_downloadFileName = os.path.join(downloadPath, 'tmp_{0}'.format(filename))
	downloadFileName = os.path.join(downloadPath, '{0}'.format(filename))
	if not os.path.exists(downloadFileName):
		ncbi_urlgetfile(ftpurl, tmp_downloadFileName, delay=0.34)
	
		try:
	# Try to interpret as gzipped file
		
			with gzip.open(tmp_downloadFileName, 'rb') as f_in:
				content = f_in.read()
				with open(downloadFileName, 'wb') as f_out:
					f_out.write(content)
			os.remove(tmp_downloadFileName)
		except IOError as e:
		# If not gzipped, treat as normal file
			if "gzipped file" in str(e):
				if os.path.exists(downloadFileName):
 					os.remove(downloadFileName)
				os.rename(tmp_downloadFileName, downloadFileName)
			else:
				raise e	
		

#class to deal with single sample
class sample(object):
	def __init__(self,sampleNr,sampleDict):
		self.accessions = []
		self.fileNames = []
		self.ftpurl=""
		self.sampleDict = sampleDict
		self.sampleNr = sampleNr
		
		# create the filename used for the final downloaded file, this is the accession prefixed withthe line number padded to 6, as required by the scripts of the scheme creation pipeline. In case of multiple accessions, the first is taken and etc is added to the filename
		self.filename = ""

		
	# parse the accessions from the sample dict
	def ParseAccessions(self):

		#the accession can be found after / and before ;
		try:
			for part in self.sampleDict[columnAcc].split(";"):
			
				if "/" in part:
					acc = part.partition("/")[2]				
					self.accessions.append(acc)
			if len(self.accessions)==0:
				# if the parsing of the Replicons column failed, we can check the content of WGS
				acc = self.sampleDict[columnWGS]+ "000000"
				self.accessions.append(acc)
			if len(self.accessions)>0:
				self.filename= str(self.sampleNr).zfill(6) + "__" + self.accessions[0]
				if len(self.accessions)>1:
					self.filename = self.filename + "etc"
				self.filename = self.filename + ".gb"
		except:
				return None
	
#downloads all accessions linked to a sample and concatenates them into a single gb file, not implemented as current version of efetch does not support https			
#	def DownloadConcatenated(self):
#		try:
#			downloadfile= os.path.join(DownloadPath,"_".join(self.accessions)+"txt")
#			with open (downloadfile,'a') as fl:
#				for acc in self.accessions:
#					gb = downloadAcc(acc)
#					if gb:
#						fl.write(gb)
#				fl.close()
#				return downloadfile	
#		except:
#			return None
	
	def ParseFtpurl(self):
		# the ftip url in the file points towards a url containing several files, we are only interested in the genbank with the complete sequence
		# so from "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/558/475/GCF_001558475.2_ASM155847v2" we need:
		# ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/558/475/GCF_001558475.2_ASM155847v2/	GCF_001558475.2_ASM155847v2_genomic.gbff.gz
		try:
			self.ftpurl= self.sampleDict[RefSeqFtp] + "/" + self.sampleDict[RefSeqFtp].rpartition("/")[2] + "_genomic.gbff.gz"
			if not self.ftpurl.startswith("ftp"): #if no refSeq ftp is provided, check for a GenBank ftp
				self.ftpurl= self.sampleDict[GBFtp] + "/" + self.sampleDict[GBFtp].rpartition("/")[2] + "_genomic.gbff.gz"
				if not self.ftpurl.startswith("ftp"):
					self.ftpurl = None #no valid ftp found
			return self.ftpurl
		except:
			raise "Please check structure of file"
		
#main, loops over files starting with genome_proks in the specified folder, creates the GAdicts and retrieves file per sample per file

assemblyFiles = genomeProks(DownloadPath,GAFileStart)
assemblyFiles.SearchGenomeProks()

for GAFile,GADict in assemblyFiles.GAFiles.iteritems():
	
	if GADict:
		for key,value in GADict.iteritems():

			CurrentSample = sample(key,value)
			CurrentSample.ParseAccessions()
		
			if CurrentSample.ParseFtpurl():
				downloadftp(CurrentSample.ftpurl,CurrentSample.filename,GAFile)

		
