---
title: "LookUpAssembler"
date: 2018-08-22T16:06:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


Determines the assembler that was used for an assembly (in a certain experiment type) and writes it to an entry info field. 

Prior to running the script, the 'AssemblyField' variable should be changed to the field in which the assembler used will be written.


