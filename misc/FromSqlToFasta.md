---
title: "FromSqlToFasta"
date: 2019-06-17T16:09:00+01:00
draft: false
author: "Margo Dirickx"
scriptlanguage: "py"
---



Exports wgMLST allele sequences from an SQL database to fasta.

'sqlInPath' defines the path to the input SQL file.  = r'C:\Users\margo\Documents\dumps\Dump20180301\wgmlst_adb_AM_stenotrophomonas_maltophilia_v4_allele.sql'
'fastaOutPath' defines the path to which the output fasta file will be written. 

