---
title: "DownloadSRR"
date: 2018-12-05T08:00:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


this script will download the SRA files corresponding to the selected entries, based on the ID stored in a field, and create fastq-dumps of the files. 


