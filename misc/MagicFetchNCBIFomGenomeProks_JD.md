---
title: "MagicFetchNCBIFomGenomeProks_JD"
date: 2019-01-23T10:54:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


Download Genbank genome assembly (GA) files based on the "genomes_proks.txt" table file.

The "genomes_proks.txt" table file cn be obtained as an export of the 'Genome Assembly and Annotation report' from the NCBI Genome repository (https://www.ncbi.nlm.nih.gov/genome/).  

Based on "PythonCode/(MAIN)/BplRelease/Import/ImportSequences/fetchwizard.py"

