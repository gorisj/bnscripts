//$MENU window=main;popup=Import AB trace files;insertpopupafter=Scripts;name=Import...;

string filename,filelist,batchname,key,expername,cerror;
string refchannellist,refchannelsel,linktoname,importdate,descript;
string pool,sample,sel;
string dyenames[],channelfilename[];
string trc_keys[],trc_sample[],trc_comment[],trc_lanenrs[],trc_vals[][];
string st_resamp_start,st_resamp_stop,st_resamp_fact;
string parse_filename,parse_samplename,parse_samplecomment,complist;
string st,st1,st2,st3,st4;

float resamp_frac1,resamp_frac2,resamp_fact;

integer lanecount,dyecount,i,j,dye,lane;
integer rs,x0,y0,finished;
integer ctrl_preview,ctrl_ok,ctrl_cancel,ctrl_list,ctrl_fromfile;
integer refchannelnr,batchexists;
string experlist,expernames[];
integer expernr;

FLOATARRAY trc_curves[][];
XMLNODE docnode,doc,root,node,node1;
CRFPRINT crfpr;
DIALOG dlg;
INDEX existingfileindex;
TIME importtime;
TABLE fileinfo;

//*****************************************************************
// INITIALIZATION
//*****************************************************************

//-----------------------------------------------------------------
// close all analysis windows otherwise cannot create fpr
AnFprAttach;
if AnFprGetCurrent<>"" then {
   MessageBox("Error","Please close all fingerprint windows before running this script.","esclamation");
   stop;
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// timestamp
TmGetCurTime(importtime);
importdate=TmString(importtime);
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// create custom fingerprint file fields
try { FprFileFldAdd("IMPORTDATE",50); }
try { FprFileFldAdd("DESCRIPTION",50); }
//create custom fingerprint fields
try { FprFldAdd("TRACEID",150); }
try { FprFldAdd("TRCSAMPLE",180); }
try { FprFldAdd("TRCCOMMENT",180); }
try { FprFldAdd("LANENR",80); }
try { FprFldAdd("ENTRYID",180); }
try { FprFldAdd("PROJECTID",150); }
try { FprFldAdd("SETID",180); }
try { FprFldAdd("DYEID",150); }
try { FprFldAdd("IMPORTDATE",50); }
try { FprFldAdd("CURVEID",120); }
//-----------------------------------------------------------------

//*****************************************************************
// FILE INPUT
//*****************************************************************

//-----------------------------------------------------------------
// get file list to import
filelist="*.fsa";
if not(FilePromptName("Select AB files to import",filelist,1)) then stop;
TableCreate(fileinfo,"","File name:100	Sample:100	Comment:100	Lane:70	[EntryID]:100	[ProjectID]:100	[SetID]:100");
while filelist<>"" do {
   TableAddRow(fileinfo);
   TableSetField(fileinfo,TableGetRowCount(fileinfo),1,splitstring(filelist,"	"));
}
TableSort(fileinfo,1);
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// read file info into table & store curve values
dyecount=0;
lanecount = TableGetRowCount(fileinfo);
for lane=1 to lanecount do {
   setbusy("Reading lane "+str(lane));
   filename=TableGetField(fileinfo,lane,1);
   XMLNodeCreateDoc("Crv",docnode,node);
   CrFprReadCurve(filename,"ABI",node,cerror);
   if cerror<>"" then {
      setbusy("");
      MessageBox("Error","Error while reading "+filename+"~n"+cerror,"exclamation");
      stop;
   }
   // info fields
   st=filename; while find(st,"\",1)>0 do splitstring(st,"\");
   st=splitstring(st,".fsa");st=splitstring(st,".FSA");
   TableSetField(fileinfo,lane,1,st);
   TableSetField(fileinfo,lane,2,XMLNodeGetText(XMLNodeGetChild(node,"Sample")));
   TableSetField(fileinfo,lane,3,XMLNodeGetText(XMLNodeGetChild(node,"Comment")));
   TableSetField(fileinfo,lane,4,XMLNodeGetText(XMLNodeGetChild(node,"Lane")));
   // trace curve values
   dye=0;
   for i=1 to XMLNodeGetChildCount(node) do {
      node1=XMLNodeGetChildByNr(node,i);
      if XMLNodeGetName(node1)="Curve" then {
         dye=dye+1;
         trc_vals[lane][dye]=XMLNodeGetText(node1);
      }
   }
   if lane=1 then {
      dyecount=dye;
      for dye=1 to dyecount do {
         dyenames[dye]=XMLNodeGetText(XMLNodeGetChild(node,"DyeName"+str(dye)));
      }
   } else {
      if (dyecount<>dye) then {
         setbusy("");
         MessageBox("Error","Inconsistent number of dyes in file: "+filename,"exclamation");
         stop;
      }
   }
   XMLNodeCloseDocument(docnode);
}
setbusy("");
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// check input
if lanecount=0 then {
   MessageBox("Error","No lanes found!","exclamation");
   stop;
}
if dyecount=0 then {
   MessageBox("Error","No channel found!","exclamation");
   stop;
}
//-----------------------------------------------------------------

//*****************************************************************
// BATCH INFO DIALOG
//*****************************************************************

//-----------------------------------------------------------------
// load settings
refchannelnr=dyecount;
st_resamp_start="0";
st_resamp_stop="100";
st_resamp_fact="1";
st=DbLoadSettings("ImportAB_Settings",1);
if length(st)>0 then {
   docnode=XMLNodeReadString(st,cerror);
   root=XMLNodeGetChild(docnode,"ABImportSettings");
   node=XMLNodeGetChild(root,"Resampling");
   st_resamp_start=str(100*val(XMLNodeGetAttrib(node,"ResampFrac1")),0,2);
   st_resamp_stop=str(100*val(XMLNodeGetAttrib(node,"ResampFrac2")),0,2);
   st_resamp_fact=str(val(XMLNodeGetAttrib(node,"ResampFact")),0,3);
   node=XMLNodeGetChild(root,"FprExpers");
   if dyecount=XMLNodeGetChildCount(node) then {
      for dye=1 to dyecount do {
         node1=XMLNodeGetChildByNr(node,dye);
         expernames[dye] = XMLNodeGetText(node1);
         if intval(XMLNodeGetAttrib(node1,"refchannel"))=1 then {
            refchannelnr=dye;
         }
      }
   }
   XMLNodeCloseDocument(docnode);
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// get fingerprint experiments list
for expernr=1 to DbGetExperCount do {
   if DbGetExperClass(expernr) = "FPR" then {
      if Length(experlist)=0 then expername = DbGetExperName(expernr);
      experlist = experlist + "	" + DbGetExperName(expernr);
   }
}
splitstring(experlist,"	");
for dye=1 to dyecount do {
   if Length(expernames[dye])=0 then expernames[dye]=expername;
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// existing files
for i=1 to DbGetFileCount do {
   IdxSet(existingfileindex,DbGetFileName(i),1);
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// reference channel
refchannellist="<None>";
for dye=1 to dyecount do {
   refchannellist=refchannellist+"	Channel "+str(dye);
   if refchannelnr=dye then refchannelsel="Channel "+str(dye);
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// draw batch info dialog
DlgReset(dlg);
DlgAddFrame(dlg,"Batch",15,15,300,140);
DlgAddText(dlg,"Batch name:",25,35,80,15);
DlgAddEdit(dlg,batchname,127,32,180,20);
DlgAddText(dlg,"Import date:",25,65,80,15);
DlgAddEdit(dlg,importdate,127,62,180,20);
DlgAddText(dlg,"Description:",25,95,80,15);
DlgAddEdit(dlg,descript,127,92,180,20);
DlgAddText(dlg,"Reference channel:",25,125,100,15);
DlgAddList(dlg,refchannellist,refchannelsel,127,122,180,180,"DROP");

DlgAddFrame(dlg,"Channels",15,170,480,dyecount*40+5);
for dye=1 to dyecount do {
   DlgAddText(dlg,"Channel "+str(dye)+":",25,145+dye*40,70,15);
   DlgAddEdit(dlg,dyenames[dye],100,145+dye*40,180,20);
   DlgAddList(dlg,experlist,expernames[dye],300,142+dye*40,180,20,"drop");
}

DlgAddFrame(dlg,"Resampling",330,15,160,110);
DlgAddText(dlg,"Start:",340,35,77,15);
DlgAddEdit(dlg,st_resamp_start,420,32,40,20);  DlgAddText(dlg,"%",466,35,15,15);
DlgAddText(dlg,"Stop:",340,65,77,15);
DlgAddEdit(dlg,st_resamp_stop,420,62,40,20);  DlgAddText(dlg,"%",466,65,15,15);
DlgAddText(dlg,"Downsampling:",340,95,77,15);
DlgAddEdit(dlg,st_resamp_fact,420,92,40,20);  DlgAddText(dlg,"x",466,95,15,15);
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// show & check dialog
finished=0;
while not(finished) do {
   rs=DlgShow(dlg,"Import AB densitometric curves",510,290+dyecount*40);
   if rs=0 then stop;
   finished=1;
   if (finished) and (batchname="") then {
      finished=0;
      MessageBox("Error","You should enter a non-empty batch name","exclamation");
   }
   if finished then {
      if (find(batchname,"_",1)>0) or (find(batchname,",",1)>0) or (find(batchname,";",1)>0) or (find(batchname," ",1)>0)  or (find(batchname,"'",1)>0)  or (find(batchname,"~"",1)>0) then {
         finished=0;
         MessageBox("Error","Batch name contains illegal characters (only characters and numbers are valid)","exclamation");
      }
   }
   for dye=1 to dyecount do if finished and (dyenames[dye]="") then {
      finished=0;
      MessageBox("Error","All channels should have valid names","exclamation");
   }
   if finished then {
      batchexists=0;
      for dye=1 to dyecount do
         if IdxGet(existingfileindex,batchname+"_"+str(dye))>0 then batchexists=1;
      if batchexists then {
         finished=0;
         MessageBox("Error","This batch exists already in the database","exclamation");
      }
   }
   resamp_frac1=val(st_resamp_start)/100.0;
   resamp_frac2=val(st_resamp_stop)/100.0;
   resamp_fact=val(st_resamp_fact);
   if finished then {
      if (resamp_frac1<0) or (resamp_frac2>1) or (resamp_frac1>=resamp_frac2) or (resamp_fact<1) then {
         finished=0;
         MessageBox("Error","Invalid resampling settings","exclamation");
      }
   }
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
//save settings
XMLNodeCreateDoc("ABImportSettings",docnode,root);
node=XMLNodeAddChild(root,"Resampling");
XMLNodeAddAttrib(node,"ResampFrac1",str(resamp_frac1,0,4));
XMLNodeAddAttrib(node,"ResampFrac2",str(resamp_frac2,0,4));
XMLNodeAddAttrib(node,"ResampFact",str(resamp_fact,0,4));
node=XMLNodeAddChild(root,"FprExpers");
for dye=1 to dyecount do {
   node1=XMLNodeAddChild(node,"Name");
   XMLNodeAddText(node1,expernames[dye]);
   XMLNodeAddAttrib(node1,"refchannel",str(dye=refchannelnr));
}
st=XMLNodeToString(root);
DbSaveSettings("ImportAB_Settings",st,1);
//-----------------------------------------------------------------

//*****************************************************************
// ENTRY INFO DIALOG
//*****************************************************************

//-----------------------------------------------------------------
// load parse settings
parse_filename = "[PROJECTID]-[ENTRYID]-[SETID]";
st=DbLoadSettings("ImportAB_Parse",1);
if st<>"" then {
   parse_filename=splitstring(st,"	");
   parse_samplename=splitstring(st,"	");
   parse_samplecomment=splitstring(st,"	");
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
string poollist,fixedpoolid;
fixedpoolid="Parse from fields";
poollist=fixedpoolid;
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// parseable information
complist="[ENTRYID]	[PROJECTID]	[SETID]";
st1="Useable fields:";
st=complist;
while st<>"" do st1=st1+" "+splitstring(st,"	");
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// function parse data
function integer parsedata() {
   string stcomp1,stcomp2,stcomp3;
   if (find(parse_filename,"[ENTRYID]",1)=0) and (find(parse_samplename,"[ENTRYID]",1)=0) and (find(parse_samplecomment,"[ENTRYID]",1)=0) then {
      MessageBox("Error","The tag [ENTRYID] should be provided","exclamation+ok");
      return(0);
   }
   if (fixedpoolid="Parse from fields") and (find(parse_filename,"[PROJECTID]",1)=0) and (find(parse_samplename,"[PROJECTID]",1)=0) and (find(parse_samplecomment,"[PROJECTID]",1)=0) then {
      MessageBox("Error","The tag [PROJECTID] should be provided or the project id should be set as fixed","exclamation+ok");
      return(0);
   }
   for lane=1 to lanecount do {
      TableSetField(fileinfo,lane,5,"");
      TableSetField(fileinfo,lane,6,"");
      TableSetField(fileinfo,lane,7,"");
      if fixedpoolid<>"Parse from fields" then TableSetField(fileinfo,lane,6,fixedpoolid);
      stcomp1=ParseComp(TableGetField(fileinfo,lane,1),parse_filename,complist,cerror);
      stcomp2=ParseComp(TableGetField(fileinfo,lane,2),parse_samplename,complist,cerror);
      stcomp3=ParseComp(TableGetField(fileinfo,lane,3),parse_samplecomment,complist,cerror);
      st2=complist;
      while st2<>"" do {
         st3=splitstring(st2,"	");st3=substring(st3,2,length(st3)-1);
         st="";
         st4=splitstring(stcomp1,"	");if st="" then st=st4;
         st4=splitstring(stcomp2,"	");if st="" then st=st4;
         st4=splitstring(stcomp3,"	");if st="" then st=st4;
         if (fixedpoolid="Parse from fields") or (st3<>"PROJECTID") then {
            if st3="ENTRYID"   then TableSetField(fileinfo,lane,5,st);
            if st3="PROJECTID" then TableSetField(fileinfo,lane,6,st);
            if st3="SETID"     then TableSetField(fileinfo,lane,7,st);
         }
      }
   }
   return(1);
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// function callback
function integer callback(DIALOG *idlg, integer ctrl, string *action, string *comment) {
   if action="command" and ctrl=ctrl_preview then {
      parsedata;
      DlgReloadControl(idlg,ctrl_list);
   }
   if action="command" and ctrl=ctrl_fromfile then DlgReturn(idlg,111);
   if (action="command" and ctrl=ctrl_ok) or action="ok" then {
      if parsedata then DlgReturn(idlg,1);
   }
   if (action="command" and ctrl=ctrl_cancel) or action="cancel" then DlgReturn(idlg,0);
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// entry info dialog
x0=15;y0=300;
DlgReset(dlg);
ctrl_list=DlgAddMultiColumnList(dlg,fileinfo,sel,15,15,700,250,"grid");

DlgAddText(dlg,"Trace file name format string:",x0,y0,150,15);
DlgAddEdit(dlg,parse_filename,x0+155,y0,300,20);

//DlgAddText(dlg,"Sample name format string:",x0,y0+70,150,15);
//DlgAddEdit(dlg,parse_samplename,x0+20,y0+95,300,20);
//DlgAddText(dlg,"Sample comment format string:",x0,y0+140,150,15);
//DlgAddEdit(dlg,parse_samplecomment,x0+20,y0+165,300,20);

DlgAddText(dlg,st1,15,y0+30,250,15);

//DlgAddText(dlg,"Project name:",420,y0,150,15);
//DlgAddList(dlg,poollist,fixedpoolid,420,y0+25,180,200,"DROP");

ctrl_preview=DlgAddButton(dlg,"Preview",101,640,y0,75,25);
ctrl_ok=DlgAddButton(dlg,"Ok",102,640,y0+40,75,25);
ctrl_cancel=DlgAddButton(dlg,"Cancel",103,640,y0+80,75,25);
// ctrl_fromfile=DlgAddButton(dlg,"Use template file...",111,420,500,150,25);

DlgRegisterCallback(dlg,"callback");
rs=DlgShow(dlg,"Determine entry & project",740,y0+150);
if rs=0 then stop;
if rs=111 then {
//   runscriptplugin("nunhem_AB_importinfo_readtemplate",batchname);
   stop;
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// save settings
st = parse_filename   + "	" +
     parse_samplename + "	" +
     parse_samplecomment;
DbSaveSettings("ImportAB_Parse",st,1);
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// check presence of entry/exper pairs
for dye=1 to dyecount do {
   expername=expernames[dye];
   for lane=1 to lanecount do {
      key=TableGetField(fileinfo,lane,5);
      if DbIsKeyPresent(key) then {
         if DbGetEntryLink(key,expername)<>"" then {
            MessageBox("Error","There already exists an experiment "+expername+" with entry "+key+". Check the experiment and entry settings.","exclamation");
            stop;
         }
      }
   }
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
//perform the resampling
FLOATARRAY crv1,crv2,crv3;
integer ps1,ps2;
float vlf;
for dye=1 to dyecount do {
   for i=1 to lanecount do {
      setbusy("Resampling...  dye "+str(dye)+"  lane "+str(i));
      clear(crv1);clear(crv2);clear(crv3);
      st=trc_vals[i][dye];
      replace(st,";","	");
      FltArrFromString(st,crv1);
      ps1=resamp_frac1*length(crv1)+1;
      ps2=resamp_frac2*length(crv1);
      if (ps1>1) or (ps1<length(crv1)) then
         for j=ps1 to ps2 do add(crv2,get(crv1,j));
      else crv2=crv1;
      if resamp_fact>1.001 then Downsample(crv2,1.0/resamp_fact,crv3);
      else crv3=crv2;
      trc_curves[i][dye]=crv3;
   }
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// fingerprint file names
for dye=1 to dyecount do {
   channelfilename[dye]=batchname+"_"+dyenames[dye];
}
linktoname=channelfilename[refchannelnr];

//-----------------------------------------------------------------
// create fingerprints, add entries and save information
DbSelectNone;
for dye=1 to dyecount do {
   setbusy("Saving channel "+str(dye)+"...");
   // create fingerprint
   expername=expernames[dye];
   CrFprCreate(crfpr,channelfilename[dye],expername);
   for lane=1 to lanecount do {
      // add entry
      key=TableGetField(fileinfo,lane,5);
      if not(DbIsKeyPresent(key)) then DbAddEntry(key);
      DbSetSel(key,1);
      CrFprAddLane(crfpr,expername,key);
      CrFprLoadCurve(crfpr,lane,trc_curves[lane][dye]);
      if dye=refchannelnr then CrFprSetRef(crfpr,lane,1);
   }
   CrFprSave(crfpr);
   // save information
   FprFileFldSetContent(channelfilename[dye],"IMPORTDATE", importdate);
   FprFileFldSetContent(channelfilename[dye],"DESCRIPTION",descript);
   for lane=1 to lanecount do {
      FprFldSetContent(channelfilename[dye],lane,"TRACEID",   TableGetField(fileinfo,lane,1));
      FprFldSetContent(channelfilename[dye],lane,"TRCSAMPLE", TableGetField(fileinfo,lane,2));
      FprFldSetContent(channelfilename[dye],lane,"TRCCOMMENT",TableGetField(fileinfo,lane,3));
      FprFldSetContent(channelfilename[dye],lane,"LANENR",    TableGetField(fileinfo,lane,4));
      FprFldSetContent(channelfilename[dye],lane,"ENTRYID",   TableGetField(fileinfo,lane,5));
      FprFldSetContent(channelfilename[dye],lane,"PROJECTID", TableGetField(fileinfo,lane,6));
      FprFldSetContent(channelfilename[dye],lane,"SETID",     TableGetField(fileinfo,lane,7));
      FprFldSetContent(channelfilename[dye],lane,"DYEID",     dyenames[dye]);
      FprFldSetContent(channelfilename[dye],lane,"IMPORTDATE",importdate);
   }
   // link reference gel
   if channelfilename[dye]<>linktoname then
      CDBUpdateSingleField(ConnDbGetInfo(ConnDbGetDefault,"fprintfiles"),"INLINELINK",
         CDBLXEqual("FILENAME",channelfilename[dye]),linktoname);
}
setbusy("");
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// save batch name for future processing
st=DbLoadSettings("ImportAB_Batches",1);
if st<>"" then {
   docnode=XMLNodeReadString(st,cerror);
   root=XMLNodeGetChild(docnode,"Batches");
} else {
   XMLNodeCreateDoc("Batches",docnode,root);
}
XMLNodeCreateDoc("Batch",doc,node);
XMLNodeAddAttrib(node,"name",batchname);
XMLNodeAddAttrib(node,"refchannel",str(refchannelnr));
XMLNodeAddAttrib(node,"lanecount",str(lanecount));
for dye=1 to dyecount do {
   XMLNodeAddText(XMLNodeAddChild(node,"File"),channelfilename[dye]);
}
XMLNodeInsert(root,node);
st=XMLNodeToString(node);
DbSaveSettings("ImportAB_Batches",XMLNodeToString(root),1);
XMLNodeCloseDocument(docnode);
XMLNodeCloseDocument(doc);
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// proceed to processing wizard
runscriptplugin("nunhem_AB_processing",st);
//-----------------------------------------------------------------

