---
title: "MagicFetchNCBIFomGenomeInFolder"
date: 2019-01-23T10:54:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


This script is a slight modification to the MagicFetchNCBIFomGenomeProks_JD.py script that downloads Genbank genome assembly (GA) files based on the "genomes_proks.txt" table file.

The script will search a folder for files starting with 'genome_proks', create a subfolder per file, copy the file with prefix completed_ and download all assemblies to this folder

Script parameters can be modified in the section '#define parameters for script'.

