"""
Download Genbank genome assembly (GA) files based on the "genomes_prok.txt" table file.

Based on "PythonCode/(MAIN)/BplRelease/Import/ImportSequences/fetchwizard.py"
Authors: Katleen Vranckx, Jan Deneweth
"""

import os
import csv
import gzip
from Bio import Entrez
from time import sleep

# Get url download method
try:
    # Prioritise BioNumerics internal ethods
    import bns
    urlgetfile_raw = bns.Util.UrlTools.urlgetfile
except ImportError:
    # Fallback to general methods
    import urllib
    urlgetfile_raw = urllib.urlretrieve


# -- Globals --

# Define parameters for script
DOWNLOAD_PATH = r"D:\tmp\GenbankDownloads"
GENOME_ASSEMBLIES_FILE = os.path.join(DOWNLOAD_PATH, "genomes_proks.csv")

# headers from genome proks file used by the script
# (Change in case they changed)
COLUMN_ACCESSION = "Replicons"
COLUMN_WGS = "WGS"
COLUMN_REFSEQ_FTP = "RefSeq FTP"
COLUMN_GB_FTP = "GenBank FTP"


# -- Auxiliary Functions --

# NCBI only accepts 3 requests per second, so we need to add a delay
def delay_decorator(func, delay_default=None):
    """Decorate a function for added 'delay' functionality"""
    def delayed_func(*args, **kwargs):
        """
        Function with additional sleep delay.
    
        :param args: positional arguments for method
        :param kwargs: keyword arguments for method
        :param delay: time to delay after executing method (default set when decorating)
        """
        # Get delay parameter
        delay = kwargs.pop('delay', delay_default)
        # Execute function
        results = func(*args, **kwargs)
        # Delay if requested
        if delay is not None:
            sleep(delay)
        # Return result
        return results

    return delayed_func


urlgetfile = delay_decorator(urlgetfile_raw)


# -- Genome Assembly Parsing --

def parse_assemblies(ga_filepath):
    """
    Parse genome assemblies summary file (either as comma or tab delimited file)
    :param str ga_filepath: path to genome assembly (summary) file
    :return csv.DictReader: a DictReader instance allowing iteration over data as dictionaries

    Notes:
        - Seperator is assumed as comma, unless the file extension is tsv, in which case it is TAB
        - Example of item in reader: {"Organism/Name": "Vibrio mimicus", "Strain": "ATCC 33654", ...}
    """
    with open(ga_filepath, 'r') as fl:
        delim = '\t' if ga_filepath.endswith('.tsv') else ','  # Assume comma unless explicitly stated as TSV
        reader = csv.DictReader(fl, delimiter=delim)
        data = list(reader)
        return data


class Sample(object):
    """class to deal with single sample"""

    def __init__(self, sample_nr, sample_dict):
        # Process input
        self.sample_nr = sample_nr
        self._sample_dict = sample_dict
        try:
            # Get info from sample dict
            self.refseq_ftp = self._sample_dict[COLUMN_REFSEQ_FTP]
            self.gb_ftp = self._sample_dict[COLUMN_GB_FTP]
            self.accessions_raw = self._sample_dict[COLUMN_ACCESSION]
            self.wgs_raw = self._sample_dict[COLUMN_WGS]
        except (KeyError, IndexError) as e:
            raise AssertionError("Missing a column in data for sample number {0}, please check format. Error:\n{1}".format(self.sample_nr, str(e)))

        # Derive ftp_url, accessions and output filename
        self._parse_ftp_url()
        self._parse_accession()

    def _parse_accession(self):
        """parse the accessions from the sample dict"""
        self.accessions = []
        self.filename = None
        
        # The accessions can be found after '/' of each ';' delimited value
        for part in self.accessions_raw.split(";"):
            if "/" in part:
                acc = part.split("/", 1)[-1]
                self.accessions.append(acc)

        # if the parsing of the Replicons column failed, we can check the content of WGS
        if len(self.accessions) == 0 and self.wgs_raw:
            acc = self.wgs_raw + "000000"
            self.accessions.append(acc)

        # Set the filename based on accession
        # (Not a full path, only basic filename!)
        if len(self.accessions) > 0:
            # create the filename to be used for the downloaded file.
            # This is the FIRST accession prefixed with the line number zero-padded to six characters,
            # as required by the scripts of the 'wgMLST scheme creation pipeline'.
            self.filename = str(self.sample_nr).zfill(6) + "__" + self.accessions[0]
            # In case of multiple accessions, the first is taken and 'etc' is added to the filename.
            if len(self.accessions) > 1:
                self.filename += "etc"
            self.filename += ".gb"
        else:
            raise AssertionError("Could not find accessions in data for sample number {0}".format(self.sample_nr))

    def _parse_ftp_url(self):
        """
        Set the internal 'ftp_url' variable to the complete sequence genbank file.

        The ftp url in the file points towards a url containing several files,
        we are only interested in the genbank with the complete sequence.
        So for "GCF_001558475.2_ASM155847v2", we need:
            ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/558/475/GCF_001558475.2_ASM155847v2/GCF_001558475.2_ASM155847v2_genomic.gbff.gz
        """
        # Get a valid FTP base url, if any
        if self.refseq_ftp.startswith("ftp"):
            valid_ftp = self.refseq_ftp
        elif self.gb_ftp.startswith("ftp"):
            valid_ftp = self.gb_ftp
        else:
            raise AssertionError("No valid FTP url for sample number {0}".format(self.sample_nr))

        # Set FTP url
        accession = valid_ftp.split("/")[-1]
        self.ftp_url = valid_ftp + "/" + accession + "_genomic.gbff.gz"

    def download(self):
        """Downloads the Genbank file, and unzips in case of gz"""
        tmp_download_file_name = os.path.join(DOWNLOAD_PATH, 'tmp_{0}'.format(self.filename))
        download_file_name = os.path.join(DOWNLOAD_PATH, '{0}'.format(self.filename))
        # Only download if file does not yet exist
        if not os.path.exists(download_file_name):
            # Get raw file
            urlgetfile(self.ftp_url, tmp_download_file_name, delay=0.34)
            # Process file
            try:
                # Try to interpret as gzipped file
                with gzip.open(tmp_download_file_name, 'rb') as f_in:
                    content = f_in.read()
                    with open(download_file_name, 'wb') as f_out:
                        f_out.write(content)
                os.remove(tmp_download_file_name)
            except IOError as e:
                # If not gzipped, treat as normal file
                if "gzipped file" in str(e):
                    if os.path.exists(download_file_name):
                        os.remove(download_file_name)
                    os.rename(tmp_download_file_name, download_file_name)
                else:
                    raise e


# -- Script Execution --

def main():

    # Loop over assemblies in file, download samples
    successes, failures = 0, 0
    for sample_nr, sample_dict in enumerate(parse_assemblies(GENOME_ASSEMBLIES_FILE)):
        try:
            current_sample = Sample(sample_nr, sample_dict)
        except AssertionError as e:
            # AssertionError normally means an error in data format
            err_txt = "\t" + str(e).replace("\n", "\n\t")  # Indented error text
            print("Assertion broken for sample number {0}, error:\n{1}".format(sample_nr, err_txt))
            failures += 1
        else:
            # No error, download file
            current_sample.download()
            print("Succesfully downloaded sample number {0}".format(sample_nr))
            successes += 1

    # Print summary
    succes_ratio = float(successes)/(successes+failures)
    print("\n\nResult: {0} successes | {1} failures: {2:.1%} percent success".format(successes, failures, succes_ratio))


if __name__ == '__main__':
    main()

#
#
# END OF FILE
