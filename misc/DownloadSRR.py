#this script will download the SRA files corresponding to the selected entries, based on the ID stored in a field, and create fastq-dumps of the files. 

import bns
import os
import subprocess

#path of the fastqdump.exe from the sratoolkit (always keep the fastq-dump at the end)
DirSRAtools = r"D:\Jessy\fastq-dump"
#folder to store the final fastQ files (a subfolder will be created per run)
DirOutput = "D:\Jessy\New folder"
#field in your database that contains the SRR number
SRRField = "Run_s"
#number of simultaneous downloads
NbrDownloads = 4

#do not modify beyond this line (unless you know what you are doing)
Db = bns.Database.Db
	
def createRunBats(RunIds,DirOutput):
	batID = 1
	p={}
	for Id in RunIds:
		bat = os.path.join(DirOutput,Id +'.bat')
		
		batFile = open(bat, 'w')
		print '"{0}" {1} --outdir "{2}\{1}" --split-files --gzip"'.format(DirSRAtools,Id,DirOutput)
		batFile.write('"{0}" {1} --outdir "{2}\{1}" --split-files --gzip"'.format(DirSRAtools,Id,DirOutput))
		batFile.close()
		p[batID] = subprocess.Popen(bat)
		batID = batID +1
	exit_codes = [process.wait() for process in p.values()]
	
IDs =[]
for entry in Db.Selection:
	if len(IDs)< NbrDownloads:
		IDs.append(entry.Field(SRRField).Content)
	else:
		IDs.append(entry.Field(SRRField).Content)
		createRunBats(IDs,DirOutput)
		IDs = []
if len(IDs)>0:
	createRunBats(IDs,DirOutput)
