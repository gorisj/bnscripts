---
title: "getSraRunsFromAccIds"
date: 2018-01-15T10:05:00+01:00
draft: false
author: "Diederik Vanfleteren"
scriptlanguage: "py"
---


This script converts and links between different NCBI accessions, for instance from SRS ids to SSR ids.

The following variables should be altered before running the script: 
'pathIdentifiers': the path to the text file containing the original identifiers. Each ID on a seperate new line.
'outDir': the path to directory to which the output should be written


