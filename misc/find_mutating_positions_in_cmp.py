#//$MENU window=Comparison;popup=Sequence;subpopup=;insertsubpopupafter=;insertafter=Align selected sequences;name=Find mutating positions...;separator=1

import string

import collections
import re

import bns
dlgbuilder=bns.Windows.XmlDlgBuilder

def GetRefEntryKey(experType):
	#fetch the reference entry
	sett =  experType.Settings;
	refentries = re.findall(r'<Reference>\s*([^<]*?)\s*</Reference>', sett)
	if len(refentries)==0:
		return ''
	else:
		return refentries[0]
			

class DetermineRefSNPsDlg(dlgbuilder.Dialogs):
		def __init__(self, entries, currExper):			
			
			##define data members
			self.experId_=""
			##define base class
			dlgbuilder.Dialogs.__init__(self, "Determine mutating positions")
			##define items
			self.seqExperTypes_=[exper for exper in bns.Database.Db.ExperimentTypes if exper.Class=='SEQ']
			if len(self.seqExperTypes_)==0:
				raise RuntimeError("Please define a sequence experiment type first.")

			seqexperitems=[dlgbuilder.ListItem(exper.DispName, exper.Name) for exper in self.seqExperTypes_]
			snpexperitems=[dlgbuilder.ListItem(exper.DispName, exper.Name) for exper in self.seqExperTypes_]
			dfltExper = currExper
			if not(currExper.Name in [exper.Name for exper in self.seqExperTypes_]) and len(self.seqExperTypes_):
				dfltExper = self.seqExperTypes_[0]
				
			
			##define controls
			self.refseqentryIdTxt_= dlgbuilder.StaticText("Entry key:")
			self.refseqentryIdCtrl_ = dlgbuilder.Input("refseqentryId", 25, default=GetRefEntryKey(dfltExper))

			self.refseqexperIdTxt_= dlgbuilder.StaticText("Experiment type:")
			self.refseqexperIdCtrl_ = dlgbuilder.Drop("refseqexperId", seqexperitems, 5, 25, default=currExper.Name)

			self.seqeentryInfo1_ = dlgbuilder.StaticText("Number of samples to be analyzed: " + str(len(entries)))

			self.seqexperIdTxt_ = dlgbuilder.StaticText("Sample experiment type:")
			self.seqexperIdCtrl_=dlgbuilder.Drop("seqexperId", seqexperitems, 5, 25, default=dfltExper.Name)
			
			self.snpexperIdTxt_ = dlgbuilder.StaticText("SNP experiment type:")
			self.snpexperIdCtrl_=dlgbuilder.Drop("snpexperId", snpexperitems, 5, 25, default=self.seqExperTypes_[0].Name)
			
			self.useMissenseCtrl_=dlgbuilder.Check("usemissense", "Use missense mutations.", default=1)
			self.useSilentCtrl_=dlgbuilder.Check("usesilent", "Use silent mutations.", default=1)
			self.useIntergenicCtrl_=dlgbuilder.Check("useintergenic", "Use intergenic mutations.", default=1)
			self.openCmpCtrl_=dlgbuilder.Check("opencmp", "Open comparison window when finished.", default=1)

			grid1 = [ [self.refseqentryIdTxt_, self.refseqentryIdCtrl_], [self.refseqexperIdTxt_, self.refseqexperIdCtrl_] ]
			grid2 = [ dlgbuilder.Row([self.seqeentryInfo1_]), dlgbuilder.Row([dlgbuilder.Cell([[self.seqexperIdTxt_, self.seqexperIdCtrl_]])]) ]
			grid3 = [ 
				dlgbuilder.Row([dlgbuilder.Cell([[self.snpexperIdTxt_, self.snpexperIdCtrl_]])]), 
				dlgbuilder.Row([dlgbuilder.Cell([[self.useMissenseCtrl_]], paddingLeft=5)]),
				dlgbuilder.Row([dlgbuilder.Cell([[self.useSilentCtrl_]], paddingLeft=5)]),
				dlgbuilder.Row([dlgbuilder.Cell([[self.useIntergenicCtrl_]], paddingLeft=5)])
			]

			grid = [
				dlgbuilder.Row( [dlgbuilder.Cell(grid2, title = 'Sample sequences')] ),
				dlgbuilder.Row( [dlgbuilder.Cell(grid1, title='Reference sequence')] ),				
				dlgbuilder.Row( [dlgbuilder.Cell(grid3, title = 'Output')] )
			]
			
			tb = dlgbuilder.SimpleDialog(dlgbuilder.Grid(
				grid			
				), onStart=self.OnStart, onOk=self.OnOk)
			dlg = dlgbuilder.Dialog("Determine mutating positions", "Determine mutating positions", tb)
			self.AddDialog(dlg)
		def OnStart(self, args):
			return
		def OnOk(self, args):
			self.refseqentryId_=self.refseqentryIdCtrl_.GetValue()		
			self.refseqexperId_=self.refseqexperIdCtrl_.GetValue()		
			self.seqexperId_=self.seqexperIdCtrl_.GetValue()		
			self.snpexperId_=self.snpexperIdCtrl_.GetValue()		
			#self.openCmp_=self.openCmpCtrl_.Checked
			self.useMissense_=self.useMissenseCtrl_.Checked
			self.useSilent_=self.useSilentCtrl_.Checked
			self.useIntergenic_=self.useIntergenicCtrl_.Checked


class AminoAcidInfo:
	def __init__(self):
		self.isStartKodon=False
		self.isStopKodon=False
		self.aminoAcid='X'
	
class TranslationTable:
	def __init__(self):
		self.code_=[-1 for i in xrange(256)]
		self.code_[ord('A')]=0
		self.code_[ord('C')]=1
		self.code_[ord('G')]=2
		self.code_[ord('T')]=3
		
		self.table_=[ [ [AminoAcidInfo() for k in xrange(4)] for j in xrange(4)] for i in xrange(4)]

		AAs='FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG'
		starts = '---M---------------M---------------M----------------------------'
		base1 =  'TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG'
		base2 =  'TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG'
		base3 =  'TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG'
		
		for i, aa in enumerate(AAs):
			c1=self.code_[ord(base1[i])]
			c2=self.code_[ord(base2[i])]
			c3=self.code_[ord(base3[i])]
			self.table_[c1][c2][c3].aminoAcid = aa
			self.table_[c1][c2][c3].isStartKodon = starts[i]=='M'
			self.table_[c1][c2][c3].isStopKodon = aa=='*'	
	
	def translate(self, codon):
		c1=self.code_[ord(codon[0])]
		c2=self.code_[ord(codon[1])]
		c3=self.code_[ord(codon[2])]
		return self.table_[c1][c2][c3].aminoAcid


class SNP:
	def __init__(self):
		self.nucl=''
		self.aa=''
		self.tpe=0 #can be: 0=equal to ref, 1=missense, 2=silent, 3=intergenic, 4=no annotation 
		
	def hasType(self):
		return self.tpe!=0 and self.tpe!=4
	
	def getStrType(self):
		if self.tpe==1:
			return "missense"
		if self.tpe==2:
			return "silent"
		if self.tpe==3:
			return "intergenic"
		if self.tpe==4:
			return ""
		if self.tpe==5:
			return "?"
		return ""


class RefSNPComparator:
	def __init__(self):
		self.entries_=[]
		self.expertype_=None	
		self.snpExperType_=None
		self.refkey_=None
		self.refexpertype_=None
		self.context_=None
		self.hasannotation_=False
		self.usemissense_=True
		self.usemissenssilent_=True
		self.useintergenic_=True


		self.translationtable_=TranslationTable()
		self.revcompl_=[-1 for i in xrange(256)]
		self.revcompl_[ord('A')]='T'
		self.revcompl_[ord('C')]='G'
		self.revcompl_[ord('G')]='C'
		self.revcompl_[ord('T')]='A'

		self.snplists={}

		#for future reference
		self.comm_=None
		
	def setProgress(self, prog, mess):
		if mess:
			self.context_['SetBusy'](str(prog) + '% ... ' + mess)
		else:
			self.context_['SetBusy']('')
		#print(mess)	
		#bns.Windows.BnsWindow(1).SetProgressbar(prog, mess)
		#if self.comm_:
		#	self.comm_.SetProgress(prog, 100)
		#	if mess:
		#		self.comm_.SetMessage(mess)
			
	def mustContinue():
		return True
#		return self.comm_.MustContinue()
		
	def getSequence(self, entry, expertype):
		seqObj = bns.Sequences.Sequence()
		seqObj.Load(entry.Key, expertype.Name)
		args={'seq': ''}
		seqObj.Get(byref=args)
		return args['seq'].upper()
		
	def getTargetSeq(self, entry):
		return self.getSequence(entry, self.expertype_)
	
	def storeSeq(self, entrykey, experid, seq):
		myseq=bns.Sequences.Sequence()
		if self.hasExper(bns.Database.Entry(entrykey), bns.Database.ExperimentType(experid)):
			myseq.Load(entrykey, experid)
		else:
			myseq.Create(experid, '', entrykey)
		myseq.Set(seq)
		myseq.Save()
		
	def codonToProtein(self, codon):
		return self.translationtable_.translate(codon)
		
	def revComplCodon(self, codon):
		return string.join([self.revcompl_[ord(codon[2-i])] for i in xrange(3)])
		
	def isCodonCovered(self, codon):
		bases = 'ACGT'
		return codon[0] in bases and codon[1] in bases and codon[2] in bases  
		
	def hasExper(self, entry, expertype):
		return len(bns.Database.Experiment(entry.Key, expertype.Name).ExperID)>0
	
	def check_pre(self):
	
		#ids = [bns.Windows.BnsWindow.GetID(nr) for nr in xrange(bns.Windows.BnsWindow.GetCount())]
		#if 'Comparison' in [bns.Windows.BnsWindow(Id).GetClassname() for Id in ids]:
		#	raise RuntimeError("Please close all comparison windows first before calculating mutating positions.")
				
		missingSeq = [entry for entry in self.entries_ if not(self.hasExper(entry, self.expertype_))]
		if missingSeq:
			basemess = "Cannot calculate mutating positions, the following entries do not have a sequence:"
			if len(missingSeq)==1:
				basemess = "Cannot calculate mutating positions, the following entry does not have a sequence:"
				
			mess = string.join( 
				[	basemess, '\n',
					string.join(['\t' + entry.Key for entry in missingSeq], "\n") ] )
			raise RuntimeError(mess)

	def calc(self, args):
		entries=self.entries_
			
		self.check_pre()	
				
		#for future reference
		#self.comm_=args['communication']
			
		#--- load the sequences ------------------------------------------------------------------------------		
		self.setProgress(0, "Fetching sequences...")

		#get the reference sequence
		refentry=bns.Database.Entry(self.refkey_)		
		
		#load the reference sequence
		refseq =	self.getSequence(self.refentry_, self.refexpertype_)
		seqlen = len(refseq)
		
		#load the target sequences
		#targetseqs={}
		#for entry in entries:
		#	targetseqs[entry.Key] = self.getSequence(entry, self.expertype_)
		
		#--- determine mutating positions -------------------------------------------------------------------
		self.setProgress(0, "Determining mutating positions...")

		#determine mutating positions		
		bases = set(base for base in 'ACGT')
		baseList = [1]*256
		baseList [ord('A')] =0
		baseList [ord('C')] =0
		baseList [ord('G')] =0
		baseList [ord('T')] =0		
		
		prog=0
		step=100/(len(entries))
		
		args = { 
			'refentry' : self.refentry_.Key, 
			'samples': [entry.Key for entry in entries],
			'expertype': self.expertype_.Name,
			'present': [], 
			'mutating': [] 
		}
		
		bns.Sequences.SequenceTools.FindMutations(byref=args)
		
		pres = args['present']
		mut = args['mutating']
		positionList  = [ i for i in xrange(len(pres)) if pres[i]==len(entries) and mut[i] ]
		
		
		#make sure the reference has coverage
		#refCoverage = (i for i,j in enumerate(refseq) if baseList[ord(j)])
		#for i in refCoverage:
		#	coveredPositions[i]=False
		
		#figure out the mutating positions and the covered positions		
		#zp = zip(*targetseqs.values())
		#mutatingPositions = [any(seqbase!=refbase for seqbase in seqbases) for seqbases, refbase in zip(zp, refseq)]
		#coveredPositions = [all(seqbase in bases for seqbase in seqbases) for seqbases in zp]

		
		#for entry in entries:
		#	self.setProgress(prog, 'Processing entry \'' + entry.Key + '\'...')
		#	
		#	targetseq=self.getTargetSeq(entry) #targetseqs[entry.Key]
		#	myMutations = (i for i, j in enumerate(zip(targetseq, refseq)) if j[0]!=j[1])
		#	myCoverage = (i for i,j in enumerate(targetseq) if not(j in bases))
		#	
		#	#myCoverage = (i for i,j in enumerate(targetseq) if baseList[ord(j)])
		#	#myMutations=[i for i in xrange(seqlen) if targetseq[i]!=refseq[i]]
		#	#myCoverage=[i for i in xrange(seqlen) if not(targetseq[i]=='A' or targetseq[i]=='C' or targetseq[i]=='G' or targetseq[i]=='T')]
		#	self.setProgress(prog, 'Determining mutations for entry \'' + entry.Key + '\'...')
		#	for i in myMutations:
		#		mutatingPositions[i]=True
		#	self.setProgress(prog, 'Determining covered positions for entry \'' + entry.Key + '\'...')
		#	for i in myCoverage:
		#		coveredPositions[i]=False				
		#	prog=prog+step
						
		#find out the positions that are both mutating and covered
		#self.setProgress(prog, 'Determining mutating positions ...')		
		#positionList = [i for i in xrange(seqlen) if mutatingPositions[i] and coveredPositions[i]]
		#positionList.sort()
		
		#find out the positions that are mutating in the right way: 
		nothing 		= 		0	#identical to ref
		intergenic	= 		3 #intergenic
		silent 			= 		2 #silent
		missense 		= 		1 #missense
		noannot			=		4 #no annotation present
		codonnotcovered = 5
		
		self.setProgress(prog, 'Determining mutation types ...')		
		
		#load the annotation features
		annSeq=bns.Sequences.AnnSeq()
		annSeq.Create(self.refentry_.Key, self.refexpertype_.Name)
		features=[]
		for i in xrange(annSeq.GetFtsCnt()-1):			
			annFt=bns.Sequences.AnnFts()
			annSeq.GetFts(annFt, i+1)
			features.append(annFt)
										
		features=sorted(features, key=lambda x: x.GetStart())
		
		featureEnds = [ft.GetEnd() for ft in features]						
		featureStarts = [ft.GetStart() for ft in features]		
	
		hasAnnot=len(features)!=0
		self.hasannotation_=hasAnnot
		annotationList=['' for pos in positionList]
		
		#calculate the annotation indices
		annotIdx = [len(features)+1] * len(positionList)
		if hasAnnot:
			myidx=0
			previdx=0
			for idx, pos in enumerate(positionList):
				myidx=next(u for u in xrange(myidx, len(features)) if featureEnds[u]>=pos)
				if myidx:
					annotIdx[idx]=myidx
					previdx = myidx
				
		
		#write the final SNP data
		for entry in entries:
			snplist = [SNP() for i in positionList]
			targetseq=self.getTargetSeq(entry) #targetseqs[entry.Key]
			
			if not(hasAnnot):
				for idx, pos in enumerate(positionList):
					snplist[idx].nucl=targetseq[pos:pos+1]
					snplist[idx].aa=''
					if targetseq[pos]==refseq[pos]:
						snplist[idx].tpe=nothing
					else:
						snplist[idx].tpe=noannot
			else:
				i=0
				for idx, pos in enumerate(positionList):
					snplist[idx].nucl=targetseq[pos]
					snplist[idx].aa=''
					if targetseq[pos]==refseq[pos]:
						snplist[idx].tpe=nothing
					else:
#						while (i<len(features) and featuresEnd[i]-1<pos):
#							i=i+1
#						i=next((u for u in xrange(len(features)) if featureEnds[u]>=pos), len(features)+1)
						i=annotIdx[idx]
						if (i<len(features) and pos>=featureStarts[i]-1 and pos<featureEnds[i]-1):				
							#get the feature amino acid at the position				
							ftsStartPos=featureStarts[i]-1
							codonStartPos=ftsStartPos + (pos-ftsStartPos) - (pos-ftsStartPos)%3

							codon1=refseq[codonStartPos:codonStartPos+3]
							codon2=targetseq[codonStartPos:codonStartPos+3]
							
							if self.isCodonCovered(codon1) and self.isCodonCovered(codon2):
							
								if (features[i].GetOrientation()==1):
									codon1=self.revComplCodon(codon1)
								proteinFromRef = self.codonToProtein(codon1)
				
								if (features[i].GetOrientation()==1):
									codon2=self.revComplCodon(codon2)
								proteinFromTarget = self.codonToProtein(codon2)
													
								snplist[idx].aa=proteinFromTarget					
								if proteinFromTarget==proteinFromRef:
									snplist[idx].tpe=silent
								else:
									snplist[idx].tpe=missense 
							
							else:
								snplist[idx].tpe=codonnotcovered		
							
							
							if len(annotationList[idx])==0:
								annotationList[idx] = string.join([features[i].GetQlfKey(j) + features[i].GetQlfData(j) for j in xrange(features[i].GetQlfCnt())], ', ')
						
						else:
							snplist[idx].tpe=intergenic
							
			self.snplists[entry.Key]=snplist						
		
		self.setProgress(100, "")
		
		self.positions=positionList
		self.annotations=annotationList
		
		#--- done calculating mutating positions -------------------------------------------------------------

			
	def refresh(self, args):	
		#open report window
		#gat.ReportWindow.Open(self.context_['parentWinID'], self.expertype_, self.positions, self.annotations, self.snplists)
		
		snpExperType = self.snpExperType_.Name
		
		#can be: 0=equal to ref, 1=missense, 2=silent, 3=intergenic, 4=no annotation 
		accepted=[False]*6
		accepted[1]=not(self.hasannotation_) or self.useMissense_
		accepted[2]=not(self.hasannotation_) or self.useSilent_
		accepted[3]=not(self.hasannotation_) or self.useIntergenic_
		accepted[4]=not(self.hasannotation_)
		accepted[5]=not(self.hasannotation_)

		
		#figure out which positions to use 
		allposns=[False for i in xrange(len(self.positions))]
		for entry, snplist in self.snplists.items():
			for idx, snp in enumerate(snplist):
				if accepted[snp.tpe]:		
					allposns[idx]=True
					
		posns=[i for i in xrange(len(allposns)) if allposns[i]]
		
		#store the sequences
		for key, snps in self.snplists.items():
			seq=string.join((snps[pos].nucl for pos in posns), '')	
			self.storeSeq(key, snpExperType, seq)		
			
		#grab the comparison 	
		comp = bns.Comparison.Comparison(self.context_['parentWinID'])
		comp.ShowImage(snpExperType, 1)
		
		#load the alignment
		compseq=bns.Comparison.Sequence(self.context_['parentWinID'], snpExperType)
		for i in xrange(comp.GetEntryCount()):
			key=comp.GetEntryKey(i)
			snps = self.snplists[key]			
			seq = string.join((snps[pos].nucl for pos in posns), '')			
			compseq.SetAlign(i, seq)
		
		#calculate the clustering
		comp.CalcClust(snpExperType)
		comp.CalcDendro(snpExperType, "Neighbor joining")
		comp.Redraw()		
		
		#open comparison, calculate clustering
		#if self.openCmp_:
		#	compWinId = bns.Comparison.Comparison.Open('')
		#	comp = bns.Comparison.Comparison(compWinId)
		#	comp.ShowImage(self.snpexpertype_.Name, 1)
		#	comp.CalcClust(self.snpexpertype_.Name)

	def Compare(self, entries, expertype, refentry, refexpertype, snpExperType, useMissense, useSilent, useIntergenic, context):		
		self.entries_=entries
		self.expertype_=expertype
		self.refentry_=refentry
		self.refexpertype_=refexpertype
		self.snpExperType_=snpExperType
		self.context_=context
		self.useMissense_ = useMissense
		self.useSilent_=useSilent
		self.useIntergenic_ = useIntergenic
			
		#for future reference
		#bns.Windows.BnsWindow(context['parentWinID']).StartAsyncCalculation(self.calc, self.refresh, False) 		
		
		try:			
			self.calc({})
			self.refresh({})
		except RuntimeError as e:
			self.setProgress(100, "")
			bns.Util.Warnings.Add(str(e))
			bns.Util.Warnings.Dump()
			 

def RefSNPCompare(entries, expertype, refentry, refexpertype, snpExperType, useMissense, useSilent, useIntergenic,context):
	comparator = RefSNPComparator()
	comparator.Compare(entries, expertype, refentry, refexpertype, snpExperType, useMissense, useSilent, useIntergenic, context)
	
def StartRefSNPCompare(context):	
	#fetch the selected entries
	selentries=[entry for entry in bns.Database.Db.Entries if entry.Selection]
	if len(selentries)==0: #if not, use them all
		bns.Util.Program.MessageBox("Error", "No entries selected.", 'exclamation')
		return
		
	dialog=DetermineRefSNPsDlg(selentries)
	
	if not(dialog.Show()):
		return
	
	#make sure the reference is the first entry in the list
	entries=[bns.Database.Entry(dialog.refseqentryId_)] + [entry for entry in selentries if entry.Key!=dialog.refseqentryId_]
	
	#fetch settings from the dialog box	
	expertype=bns.Database.ExperimentType(dialog.seqexperId_)
	refentry=bns.Database.Entry(dialog.refseqentryId_)
	refexpertype=bns.Database.ExperimentType(dialog.refseqexperId_)
	#snpexpertype=bns.Database.ExperimentType(dialog.snpexperId_)
	#openCmp = dialog.openCmp_
	#useMissense = dialog.useMissense_
	#useSilent = dialog.useSilent_
	#useIntergenic = dialog.useIntergenic_
	
	if (entries):
		RefSNPCompare(entries, expertype, refentry, refexpertype, context)	

def StartRefSNPCompareFromCmp(cmpWinId, context):	

	comp=bns.Comparison.Comparison(cmpWinId)
	
	#fetch the entries
	selentries=[bns.Database.Entry(comp.GetEntryKey(i)) for i in xrange(comp.GetEntryCount())]
	if len(selentries)==0:
		bns.Util.Program.MessageBox("Error", "No entries present.", 'exclamation')
		return
		
	#fetch the experiment type
	experType=bns.Database.ExperimentType(comp.GetSelExper())
	
	dialog = DetermineRefSNPsDlg(selentries, experType)
	if not(dialog.Show()):
		return

	refEntry=bns.Database.Entry(dialog.refseqentryId_)
	refExperType=bns.Database.ExperimentType(dialog.refseqexperId_)
	experType=bns.Database.ExperimentType(dialog.seqexperId_)
	snpExperType= bns.Database.ExperimentType(dialog.snpexperId_)
	useMissense = dialog.useMissense_
	useSilent = dialog.useSilent_
	useIntergenic = dialog.useIntergenic_
		
	#make sure the reference is the first entry in the list
	entries=[refEntry] + [entry for entry in selentries if entry.Key!=refEntry.Key]
	
	#fetch settings from the dialog box		
	if (entries):
		RefSNPCompare(entries, experType, refEntry, refExperType, snpExperType, useMissense, useSilent, useIntergenic, context)	

	

context = { 'parentWinID' : __bnswinid__, 'SetBusy': __bnscontext__.SetBusy }
StartRefSNPCompareFromCmp(__bnswinid__, context)	


	













