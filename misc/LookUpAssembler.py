import bns
import datetime


#Change to field in which the assembler used will be written
AssemblyField = "Assembler"

def try_strptime(logContent):
	format = "%Y-%m-%d %H:%M:%S"
	try:
		s = logContent.split("\n",1)[1].split("\t",1)[0]
		date = datetime.datetime.strptime(s, format)
	except ValueError:
		date = None
	return date

for entry in bns.Database.Db.Selection:
	attachments = entry.GetAttachmentList()
	assemblerDict ={}
	assemblers = ""
	previousDate = datetime.datetime(1,1,1)
	for log in attachments:
		
		if "detailed log" in log["Description"]:
			logContent = log["Attach"].Content
			if "DeNovoAssembler" in logContent:
				date = try_strptime(logContent)
				if "spades" in logContent:
					assemblerDict[date] = "Spades"
				if "velvet" in logContent:
					assemblerDict[date] = "Velvet"
	for key in sorted(assemblerDict.keys()):
		assemblers = assemblerDict[key]+ "," + assemblers
	entry.Field(AssemblyField).Content = assemblers.rstrip(",")
bns.Database.Db.Fields.Save()
