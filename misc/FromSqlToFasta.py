import os
sqlInPath = r'C:\Users\margo\Documents\dumps\Dump20180301\wgmlst_adb_AM_stenotrophomonas_maltophilia_v4_allele.sql'
fastaOutPath = r'C:\Users\margo\Documents\dumps\Dump20180301\wgmlst_adb_AM_stenotrophomonas_maltophilia_v4_allele.fasta'
onlyRefs = True

def FromSqlToFasta(sqlInPath, fastaOutPath, onlyRefs = False):
    cnt = 0
    with open(fastaOutPath,"w") as fileOut:
        with open(sqlInPath,"r") as fileIn:
            fileInSize = os.path.getsize(sqlInPath)
            while fileIn.tell() < fileInSize:
                txt = fileIn.readline()
                toContinue = False
                for word in ["INSERT INTO", "`allele`"]:
                    if not word in txt: toContinue = True
                if toContinue:
                    continue
                records = txt.split("),(")
                for record in records:
                    record = record.replace("'","").replace("\n","")
                    parts = record.split(",")
                    status = parts[9]
                    if onlyRefs:
                        print (status)
                        if 'reference' not in status and 'confirmed' not in status:
                            continue
                    try:
                        fileOut.write(">" + parts[2] + "|" + str(len(parts[4])) + "\n")
                        fileOut.write( parts[4] + "\n")
                    except:
                        print ("exception for line: ", txt[0:100])
                    cnt += 1
                    print (cnt)
                txt = ""
                parts = []

if __name__=="__main__":
    FromSqlToFasta(sqlInPath, fastaOutPath, onlyRefs)