import bns
experName = 'ReSeqAligned'
isReferenceMapped = True

# check experType
experType = bns.Database.Db.ExperimentTypes[experName]
if experType is None:
	raise RuntimeError("Unknown experiment type '{0}'.".format(experType))
if experType.Class != 'SEQ':
	raise RuntimeError("Experiment type '{0}' is not a Sequence type experiment.".format(experType.DispName))

if isReferenceMapped:
	# check all entries loaded
	if not bns.Database.Db.Entries.ObjectType.IsLoadSetComplete():
		raise RuntimeError("Load all entries first.")
	
	# check refKey sequence presence and length
	refKey = bns.CallBnsFunction('SeqTypeGetReferenceKey', experName)
	if not refKey:
		raise RuntimeError("Set a reference entry for sequence type '{0}'.".format(experType.DispName))
	if not bns.Database.Experiment(refKey, experName).IsPresent():
		raise RuntimeError("Set a reference sequence for sequence type '{0}'.".format(experType.DispName))
	refLen = bns.Sequences.SequenceData(refKey, experName).GetLen()
	if not refLen:
		raise RuntimeError("Set a reference sequence for sequence type '{0}'.".format(experType.DispName))
	
	# check length of all other sequences
	for entry in bns.Database.Db.Entries:
		key = entry.Key
		if bns.Database.Experiment(key, experName).IsPresent():
			if refLen != bns.Sequences.SequenceData(key, experName).GetLen():
				raise RuntimeError("Sequence length for entry '{0}' differs from reference sequence length.".format(entry.DispName))

bns.CallBnsFunction('SeqTypeSetReferenceMapped', experName, isReferenceMapped)
