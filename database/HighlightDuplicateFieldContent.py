#//$menu window=main;popup=Scripts;name=Highlight duplicates in field...; 
"""HighlightDuplicateFieldContent.py
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided as-is by Applied Maths.                !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

"""
#Cfr. the MS Excel functionality: Conditional formatting > Highlight duplicate values rule
#Creates field states for all duplicate values (ignores empty fields) and assigns colors
#Highlights duplicates in 21 different colors, if more duplicates then just uses grey
#Reports about occurrence of duplicates

import bns
Entries = bns.Database.Db.Entries
Fields = bns.Database.Db.Fields
Selection = bns.Database.Db.Selection
Color = bns.Graphics.Color
Dlg = bns.Windows.XmlDlgBuilder
messageBox = bns.Util.Program.MessageBox

contents = {}
duplicates = {}

#get the field ID based on the field name
def getFieldID(fieldname):
	ID = ""
	for Field in Fields:
		if fieldname == Field.DispName:
			ID = Field.Name
	return ID


#produces contrasting colors
def assignColor(n):
	colorDict = {0: Color(230.0, 25.0, 75.0),
				1: Color(60.0, 180.0, 75.0),
				2: Color(255.0, 225.0, 25.0),
				3: Color(0.0, 130.0, 200.0),
				4: Color(245.0, 130.0, 48.0),
				5: Color(145.0, 30.0, 180.0),
				6: Color(70.0, 240.0, 240.0),
				7: Color(240.0, 50.0, 230.0),
				8: Color(210.0, 245.0, 60.0),
				9: Color(250.0, 190.0, 190.0),
				10: Color(0.0, 128.0, 128.0),
				11: Color(230.0, 190.0, 255.0),
				12: Color(170.0, 110.0, 40.0),
				13: Color(255.0, 250.0, 200.0),
				14: Color(128.0, 0.0, 0.0),
				15: Color(170.0, 255.0, 195.0),
				16: Color(128.0, 128.0, 0.0),
				17: Color(255.0, 215.0, 180.0),
				18: Color(0.0, 0.0, 128.0),
				19: Color(128.0, 128.0, 128.0),
				20: Color(0.0, 0.0, 0.0),
				"grey": Color(128.0, 128.0, 128.0)}
	if n < 21:
		return colorDict[n]
	else:
		return colorDict["grey"]


#Dialog box
class SimpleListDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'SimpleListDlg')
		# the values to show in the list control
		values = Fields
		# member variables to hold the results
		self.selectedSingle = values[0]
		# the list control
		self.singleList = Dlg.SimpleList('sl', values, 20, 30, multi=False)
		# now define the dialog layout
		grid = [["Select entry field:", self.singleList],
					[Dlg.StaticText("NOTE: Existing field states will be removed.")]]
		simple = Dlg.SimpleDialog(grid, onStart=self.onStart, onOk=self.onOk)
		self.AddSimpleDialog("Highlight duplicates in field", simple)
	def onStart(self, args):
		"""Set the selected items"""
		self.singleList.SetValue(self.selectedSingle or "")
	def onOk(self, args):
		"""Get the selected items"""
		self.selectedSingle = self.singleList.GetValue()


def highlightDuplicates():
	# check for entry selection
	if len(Selection) == 0:
		raise RuntimeError("No entries selected.")
	# dialog, get field
	dlg = SimpleListDlg()
	if not dlg.Show():
		return
	fieldID = getFieldID(dlg.selectedSingle)
	
	# iterate over the selected entries and get field contents
	for entry in Selection:
		content = entry.Field(fieldID).Content
		key = entry.Key
		if content in contents: #add key to list
			l = contents[content]
			l.append(key)
			contents[content] = l
		else: #add to content to dict and add key to list
			contents[content] = [key]
	
	#put duplicates in separate dict and report
	message = ""
	for text, keys in contents.iteritems():    # for text, keys in contents.items():  (for Python 3.x)
		if len(text) > 0 and len(keys) > 1:
			message = message + "Duplicate found: '{0}' (occurs {1}x)\n".format(text, len(keys))
			duplicates[text] = keys
	
	colorField = Fields[fieldID]
	# reset field states
	colorField.SetStates([])
	colorField.SaveSettings()
	
	# set field states and colors
	colorField.SetStates(duplicates.keys())
	n = 0
	for state, keys in duplicates.iteritems():
		n = n + 1
		color = assignColor(n)
		colorField.SetFieldStateColor(state, color)
	colorField.SaveSettings()
	if len(message) > 0:
		messageBox("Duplicates report", message, "information"+"ok")
	else:
		messageBox("Duplicates report", "No duplicate values were found in '{0}'".format(dlg.selectedSingle), "information"+"ok")


highlightDuplicates()
