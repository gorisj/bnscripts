---
title: "HighlightDuplicateFieldContent"
date: 2018-07-19T15:58:45+01:00
draft: false
author: "Johan Goris"
scriptlanguage: "py"
---


This script creates field states for duplicate values found in an entry information field for the selected entries. Empty values are ignored. The first twenty field states are assigned distinct colors, additional duplicates are highlighted in grey. This tool is inspired by the "Conditional formatting > Highlight duplicate values" rule in MS Excel.

Additionally, the script displays a simple report about the occurrence of duplicates.

