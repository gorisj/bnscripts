"""
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""
#//$MENU window=main;popup=Experiments;insertafter=Remove experiment type...;name=Delete the selected experiment values...

import bns

Dlg = bns.Windows.XmlDlgBuilder
MessageBox = bns.Util.Program.MessageBox


class DeleteExperimentDataDlg(Dlg.Dialogs):
	def __init__(self, setbusy, charactherExperiments, sequenceExperiments):
		Dlg.Dialogs.__init__(self, 'DeleteExperimentDataDlg', remembersettings='none')
		self.setbusy = setbusy
		self.charactherExperiments = charactherExperiments
		self.sequenceExperiments = sequenceExperiments
		self.selectedExperimentType = None
		self.selectedExperiment = None
		self.experimentTypes = ["character", "sequence"]
		self.singleExperiment = None
		radioDefault = None
		experimentDefault = None
		self.onlySequences = False
		self.onlyCharacters = False
		singleExperimentString = "Delete the selected experiment values from experiment type '{0}'?".format
		
		
		if (len(self.charactherExperiments) == 1 and len(self.sequenceExperiments) == 0):
			self.singleExperiment = [[Dlg.StaticText(singleExperimentString(self.charactherExperiments[0]), maxWidth=30)]]
		if (len(self.charactherExperiments) == 0 and len(self.sequenceExperiments) == 1):
			self.singleExperiment = [[Dlg.StaticText(singleExperimentString(self.sequenceExperiments[0]), maxWidth=30)]]
		self.introText = [[Dlg.StaticText("Please select the experiment type.", maxWidth=30)]]
		if len(self.sequenceExperiments) == 0:
			self.onlyCharacters = True
			radioDefault = self.experimentTypes[0]
			experimentDefault = self.charactherExperiments[0]
		if len(self.charactherExperiments) == 0:
			self.onlySequences = True
			radioDefault = self.experimentTypes[1]
			experimentDefault = self.sequenceExperiments[0]
		self.experimentTypesRadioList = Dlg.Radio('experimentTypesRadioList', self.experimentTypes , charWidth=30, vertical = True, 
							default = radioDefault if radioDefault else self.experimentTypes[0], OnChange = self.onChange)
		self.middleText = [[Dlg.StaticText("Please select the {0}experiment ".format("sequence " if self.onlySequences else "character " if self.onlyCharacters else '') + \
		                                   "from which you want to delete the selected experiment values.", maxWidth=30)]]
		self.experimentList = Dlg.SimpleList('experimentList', 	 self.sequenceExperiments if self.onlySequences else self.charactherExperiments, 
		                      itemsvisible = max(len(self.charactherExperiments), len(self.sequenceExperiments))+3, 
		                      charWidth = max(24, max(max(len(i) for i in self.sequenceExperiments) if self.sequenceExperiments else 0, 
		                                  max(len(i) for i in self.charactherExperiments) if self.charactherExperiments else 0))+2,
											 multi=False, default = experimentDefault  if experimentDefault  else self.charactherExperiments[0])
	
		grid = []
		if self.singleExperiment:
			grid = [[self.singleExperiment]]
		else:
			grid = [[self.middleText],
						 [self.experimentList]]
			if (len(self.charactherExperiments) != 0 and len(self.sequenceExperiments) != 0) :
				grid.insert(0, [self.introText])
				grid.insert(1, [self.experimentTypesRadioList])
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Delete the selected experiment values", simple)
		
	def onChange(self, args):
		self.selectedExperimentType = self.experimentTypesRadioList.GetValue()
		experiments = []
		if self.selectedExperimentType == 'character':
			experiments = [Dlg.ListItem(i) for i in self.charactherExperiments]
		else:
			experiments = [Dlg.ListItem(i) for i in self.sequenceExperiments]
		self.experimentList.SetItems(experiments)

	
	def onOk(self, args):
		"""Get the selected items"""
		if self.singleExperiment:
			if len(self.charactherExperiments) == 1:
				self.selectedExperimentType = self.experimentTypes[0]
			else:
				self.selectedExperimentType = self.experimentTypes[1]
		elif self.onlyCharacters:
			self.selectedExperimentType = self.experimentTypes[0]
		elif self.onlySequences:
			self.selectedExperimentType = self.experimentTypes[1]
		else:
			self.selectedExperimentType = self.experimentTypesRadioList.GetValue()
		if self.singleExperiment:
			if len(self.charactherExperiments) == 1:
				self.selectedExperiment = self.charactherExperiments[0]
			else:
				self.selectedExperiment = self.sequenceExperiments[0]
		else:
			self.selectedExperiment = self.experimentList.GetValue()
		print bns.Database.Db.Selection
		selectedEntries = bns.Database.Db.Selection
		if not selectedEntries:
			MessageBox("Warning",
               "There are no entries selected. No entries will be deleted.", 'exclamation')
			return
		r = MessageBox("Warning",
               "You are about to delete the selected experiment values for {0} type '{1}'! Are you really sure?".format(self.selectedExperimentType, self.selectedExperiment),
               'exclamation+yesno')
		deleted = []
		if r == 1:
			for entry in bns.Database.Db.Selection:
				if bns.Database.Experiment(entry.Key, self.selectedExperiment).ExperID:
					self.setbusy("Deleting experiment value '{0}' in {1}.".format(entry.Key, self.selectedExperiment))
					deleted.append(entry.Key)
					bns.Database.Experiment(entry.Key, self.selectedExperiment).Delete()
			self.setbusy('')
		if deleted:
			MessageBox("Information", "Deleted {0} experiment value{1} in experiment '{2}'.".format(len(deleted), '' if len(deleted) == 1 else 's', self.selectedExperiment), 'information')
		else:
			MessageBox("Information", "There were no experiment values found in experiment '{1}'!".format(len(deleted), self.selectedExperiment), 'exclamation')
	
def main(setbusy):
	charactherExperiments = [e.Name for e in bns.Database.Db.ExperimentTypes if e.Class == 'CHR']
	sequenceExperiments = [e.Name for e in bns.Database.Db.ExperimentTypes if e.Class == 'SEQ']
	if len(charactherExperiments) == 0 and len(sequenceExperiments) == 0:
		MessageBox("Error", "No characther or sequence experiment types found.", 'exclamation')
		return
	dlg = DeleteExperimentDataDlg(setbusy, charactherExperiments, sequenceExperiments)
	dlg.Show()
	
if __name__ == '__main__':
	main(__bnscontext__.SetBusy)
