---
title: "InvertSelection"
date: 2010-06-15T12:24:33+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "py"
---


This script inverts the current selection in the main window of the software.

