"""
Retrieve presence/absence of experiments
	- Limited to visible, selected entries
	- Limited to visible, selected experiment types
	- Use display names
	
Export as CSV and open file with associated program (generally Excel)
"""

import bns
db = bns.Database.Db


# Fetch visible, selected entries' and experiments' keys as strings (= selected content in current view)

entries = db.Entries.Views.Current.GetSelectedContent()
expertypes = db.ExperimentTypes.Views.Current.GetSelectedContent()

if not entries:
	raise RuntimeError("No entries are selected.")
if not expertypes:
	raise RuntimeError("No experiment types are selected.")


# Prepare matrix

header = ["Entry", ] + [bns.Database.ExperimentType(exptype_key).DispName for exptype_key in expertypes]
matrix = [header, ]  # will be modified in-place by function 'expand_matrix'


# Fetch and export data asynchronously

def expand_matrix(dct):
	calc_com_obj = dct['communication']
	calc_com_obj.SetMessage('Exporting...')
	# Expand matrix with entry data
	for entry_key in entries:
		entry_dispname = bns.Database.Entry(entry_key).DispName
		row = [entry_dispname, ]
		for exptype_key in expertypes:
			presence = bns.Database.Experiment(entry_key, exptype_key).IsPresent()
			presence_char = "X" if presence else ""
			row.append(presence_char)
		if not calc_com_obj.MustContinue():
			return
		matrix.append(row)


def export_matrix(unused_dct):
	# Concatenate to CSV format string
	matrix_str = "\n".join([
		# Join cells with tabs: will be converted to locale-specific seperator
		"\t".join(row) for row in matrix
	])
	# Export and open
	bns.Util.IO.ExportAndView(matrix_str)  # Keyword asCSV deprecated, overwritten by: "(main window) -> File -> Preferences... -> Export table files in CSV format"


main_window = bns.Windows.BnsWindow(__bnswinid__)  # If script is run from "Python Script Window", please select the main window in "Debug -> Environment parameters... -> Attached Window"
main_window.StartAsyncCalculation(expand_matrix, export_matrix, asynch=True)


#
#
# END OF FILE
