---
title: "DbExperEntryPresenceList"
date: 2018-09-17T14:55:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "py"
---



Retrieve presence/absence of experiments
	- Limited to visible, selected entries
	- Limited to visible, selected experiment types
	- Use display names
	
Export as CSV and open file with associated program (generally Excel)

