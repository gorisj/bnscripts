""" DB_DeleteExperLinks.py

Purpose:
	In order to delete an experiment type, all experiments linking
	to it should be deleted.
	This script will delete experiment links from a database.
	The experiment types are selected from a list in a dialog,
	the entries are taken from the selected entries or all entries.

Warning:
	This script can seriously corrupt the database!

History:
	Created: 2010-05-07

"""
#//$menu window=main;popup=Database;name=Delete experiment links...
import bns
dlgbuilder = bns.Windows.XmlDlgBuilder
Db =  bns.Database.Db

class DeleteExperLinkDlg(dlgbuilder.Dialogs):
	def __init__(self):
		dlgbuilder.Dialogs.__init__(self, 'DeleteExperLinkDlg')
		
		## text
		select_text = dlgbuilder.StaticText("Select the experiment type(s):")
		warn_text = dlgbuilder.StaticText("Warning: this action cannot be undone!")
		
		## experiment list
		items = [(exper.Name, exper.DispName) for exper in Db.ExperimentTypes]
		listitems = [dlgbuilder.ListItem(val, id) for id, val in items]
		listvalues = dlgbuilder.ListValues(listitems)
		self.experlist = dlgbuilder.SimpleList('experlist', listvalues, 10, 30, multi=True)
		grid = dlgbuilder.Grid([[select_text], [self.experlist], [warn_text]])
		valids = [dlgbuilder.Validation(dlgbuilder.ValueOf(self.experlist.Id).IsNotEmpty(), "No experiment type selected.")]
		simple = dlgbuilder.SimpleDialog(grid, validations=valids, onOk=self.__onOk)
		dlg = dlgbuilder.Dialog('dlg0', "Delete experiment links", simple)
		self.AddDialog(dlg)
	def __onOk(self, args):
		self.experlist = self.experlist.Multi

def DeleteExperLinks(experlist):
	entries = Db.Selection if len(Db.Selection) else Db.Entries
	for entry in entries:
		for exper in experlist:
			explink = bns.Database.Experiment(entry, exper)
			if explink.ExperID:
				if explink.ExperID.upper().startswith('EXT'):
					explink.Delete()
				else:
					explink.Key = ''

def main():
	if not len(Db.ExperimentTypes):
		bns.Util.Program.MessageBox("Error", "No experiment types present.", 'exclamation')
		return
	if not len(Db.Selection):
		r = bns.Util.Program.MessageBox("Warning", "No selection present.\nDo you want to run the tool on the whole database?", 'question+yesno')
		if r != 1:
			return
	
	dlg = DeleteExperLinkDlg()
	if not dlg.Show():
		return
	DeleteExperLinks(dlg.experlist)

main()
