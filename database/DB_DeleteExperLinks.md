---
title: "DB_DeleteExperLinks"
date: 2010-09-10T12:10:29+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "py"
---



Purpose: 
       In order to delete an experiment type, all experiments linking
       to it should be deleted.
       This script will delete experiment links from a database.
       The experiment types are selected from a list in a dialog,
       the entries are taken from the selected entries or all entries

This script deletes for a selection of database entries the sequence experiment links for a selected sequence type experiment (works for BioNumerics 6.1 or higher).

This script can seriously corrupt the database!

