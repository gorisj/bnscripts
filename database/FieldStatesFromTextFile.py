import bns

Fields = bns.Database.Db.Fields
Dlg = bns.Windows.XmlDlgBuilder

#Dialog box
class SimpleListDlg(Dlg.Dialogs):
	def __init__(self):
		# the file select
		Dlg.Dialogs.__init__(self, 'SimpleListDlg')
		self.txtFileCtrl = Dlg.File('txtFileCtrl',
													[Dlg.Extension('TXT', "Text file")],
													charWidth=40,
													dlgTitle="Select a text file",
													hasfocus=True,
													mustExist=True)
		# member variable to hold the result
		self.selectedFile = ""
		# the values to show in the list control
		values = Fields
		# member variable to hold the results
		self.selectedSingle = values[0]
		# the list control
		self.singleList = Dlg.SimpleList('sl', values, 20, 40, multi=False)
		# now define the dialog layout
		grid = [["Select text file:", self.txtFileCtrl], 
					["Select entry field:", self.singleList]]
		simple = Dlg.SimpleDialog(grid, onStart=self.onStart, onOk=self.onOk)
		self.AddSimpleDialog("Create field states from text file", simple)
	def onStart(self, args):
		"""Set the selected items"""
		self.singleList.SetValue(self.selectedSingle or "")
	def onOk(self, args):
		"""Get the selected items"""
		self.selectedFile = self.txtFileCtrl.GetValue()
		self.selectedSingle = self.singleList.GetValue()
		

#get the field ID based on the field name
def getFieldID(fieldname):
	ID = ""
	for Field in Fields:
		if fieldname == Field.DispName:
			ID = Field.Name
	return ID

#get field states from the text file
def GetFieldStates(TextFileName):
	FL = open(TextFileName, "r")
	lines = FL.read().splitlines()
	return lines

# main function
def FieldStatesFromFile():
	# dialog, get text file and database field
	dlg = SimpleListDlg()
	if not dlg.Show():
		return
	fieldID = getFieldID(dlg.selectedSingle)
	flName = dlg.selectedFile
	States = GetFieldStates(flName)
	selectedField = Fields[fieldID]
	# Set the field states for the chosen database field
	selectedField.SetStates(States)
	selectedField.SaveSettings()


FieldStatesFromFile()
