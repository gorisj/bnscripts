---
title: "Pad field with spaces"
date: 2000-05-14T14:47:00+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "bns"
---


Originally developped for Nestlé (v5.1).

Adds leading spaces to all fields until they have a constant length


