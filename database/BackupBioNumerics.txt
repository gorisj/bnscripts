@echo off
set installdir=C:\Program Files (x86)\Applied Maths\BioNumerics
set homedir=C:\Users\Public\Documents\BioNumerics\Data
set backupdir=C:\Users\Johan\Desktop\Backups

cd "%installdir%"

bn.exe -homedir="%homedir%" -dbmanagement="backup" -backupfile="%backupdir%" -silent=1 -database="DemoBase Connected"
bn.exe -homedir="%homedir%" -dbmanagement="backup" -backupfile="%backupdir%" -silent=1 -database="MyDB"
bn.exe -homedir="%homedir%" -dbmanagement="backup" -backupfile="%backupdir%" -silent=1 -database="YourDB"