---
title: "FieldStatesFromTextFile"
date: 2018-12-17T14:41:41+01:00
draft: false
author: "Johan Goris"
scriptlanguage: "py"
---


This tools creates field states for a selected entry field based on a simple text file. Each line in the text file should correspond to a field state. Any previous field states for the selected field will be overwritten.

