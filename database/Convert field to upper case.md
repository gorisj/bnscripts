---
title: "Convert field to upper case"
date: 2000-11-30T11:30:00+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "bns"
---


Originally developped for Nestlé (v5.1).

Converts field content to upper case.


