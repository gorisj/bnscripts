#//$menu window=main;popup=Scripts;subpopup=ECDC;name=Export quality to information fields; 

import bns
from string import *

#Convert experiment type in entry field
selectedEntries = bns.Database.Db.Selection
if len(selectedEntries)==0:
	raise RuntimeError("No entries selected.")

def GetCharacters(chrExpName):
	"""Return the characters in a character type experiment"""
	chrExp = bns.Characters.CharSetType(chrExpName)
	return [chrExp.GetChar(idx) for idx in xrange(chrExp.GetCount())]

def GetCharacterValues(key, chrExpName):
	"""Return the values of the characters in a character experiment in a dictionary"""
	chrDict = {}
	if bns.Database.Experiment(key, chrExpName).ExperID:
		chrSet = bns.Characters.CharSet()
		chrSet.Load(key, chrExpName)
		for idx in xrange(chrSet.GetCount()):
			if chrSet.GetPresent(idx):
				chrName = chrSet.GetName(idx)
				chrDict[chrName] = chrSet.GetVal(idx)
	return chrDict #all values are returning as float!

def GetSrsFields(srsExpName, entries):
	for entry in entries:
		key = entry.Key
		srsDict = {}
		if bns.Database.Experiment(key, srsExpName).ExperID:
			srs = bns.SequenceReadSets.SeqReadSet()
			srs.Load(key, srsExpName)
			info = {}
			srs.GetInfo(info)
			info['PercentGC'] = 0
			outList = sorted('Srs'+k for k in info.keys()) + sorted('SrsTrimmed'+k for k in info.keys()) 
			return outList 

def GetSrsValues(key, srsExpName, prefix='Srs'):
	srsDict = {}
	if bns.Database.Experiment(key, srsExpName).ExperID:
		srs = bns.SequenceReadSets.SeqReadSet()
		srs.Load(key, srsExpName)
		info = {}
		srs.GetInfo(info)
		gcBaseCount = info['CountG'] + info['CountC']
		allBaseCount = info['CountA'] + info['CountC'] + info['CountG'] + info['CountT'] + info['CountOther']
		info['PercentGC'] = (100.0 * gcBaseCount) / allBaseCount if allBaseCount else 0
		for k, v in info.items():
			srsDict[prefix+k] = v
	return srsDict #all values are returning as string!

chrName = 'Quality'
srsName = 'wgs'
fields = GetCharacters(chrName)
srsFields = GetSrsFields(srsName, selectedEntries)
for field in fields + srsFields:
	name = 'Q_'+field
	if (name) not in bns.Database.Db.Fields:
		thisField = bns.Database.Db.Fields.Add(name)
		thisField.Type = 'Number'

for entry in selectedEntries:
	dic_fields = GetCharacterValues(entry.Key, chrName)
	dic_fields.update(GetSrsValues(entry.Key, srsName))
	dic_fields.update(GetSrsValues(entry.Key, srsName+'_TrimmedStats', 'SrsTrimmed'))
	#set the value for new fields
	for field, value in dic_fields.items():
		entryField = entry.Field('Q_'+field)
		if isinstance(value, float):
			if (value - int(value)):
				# floating point that has non-zero decimal part
				entryField.Content = '{0:.2f}'.format(value)
			else:
				# floating point that is actually an integer
				entryField.Content = '{0}'.format(int(value))
		else:
			entryField.Content = '{0}'.format(value)
bns.Database.Db.Fields.Save()		
