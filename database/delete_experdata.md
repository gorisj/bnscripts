---
title: "delete_experdata"
date: 2001-01-02T11:41:13+01:00
draft: false
author: "Jeroen Van Goey"
scriptlanguage: "py"
---


This scripts removes character and sequence experiment values for the current selection. 

Close all BioNumerics databases.
Save the script to the database folder: Scripts \ Python
Open the BioNumerics database again.
!! This script works on the current selection of entries
An extra menu item appears under Experiments:
If only one CHR or SEQ experiment is present in the database, the script directly asks whether to remove all values for this experiment:

If there are only SEQ or only CHR experiments present in the database, a list with the existing experiments is displayed (FPR, matrix, trend data types etc. are ignored) :

If both SEQ and CHR experiments are present in the database, you should first select the experiment type. If you select the experiment type (character/sequence), the experiment lists will be updated.
Next, select the experiment that should be deleted.

After you selected the correct experiment, press OK to continue.
SUPPORT BIONUMERICS: SCRIPT: Delete experiment data
3
After confirmation, all experiment dots of that experiment are deleted. An additional confirmation is displayed on the number of experiment values that were deleted, or the software reports that no experiment values were found.


You can now select the experiment from the Experiments panel, and select Experiments > Remove experiment type from the main window. Close and restart the database to have the experiment deleted from the list.


