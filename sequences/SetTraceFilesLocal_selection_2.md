---
title: "SetTraceFilesLocal_selection_2"
date: 2018-03-07T15:47:49+01:00
draft: false
author: "Paul Vauterin"
scriptlanguage: "bns"
---



Trace files are removed from the connected database for a selection of entries and the trace files are created in an appointed folder. A unique ID is given to each trace file. Only works for ABI files.

This script will extract for all selected entries the sequence trace files from a connected database, and store them as files in a folder.

1.	Make a backup of your connected database before proceeding.
2.	Open your database with BioNumerics, and select some/all entries.
3.	Run the script with Scripts > Run script from file.
4.	Select the sequence type from the list. 
5.	The default path where the trace files will be stored is written in the lower part of the dialog box (default path: sourcefiles\contig folder of the database). The path can be changed by the user if desired.

  

The trace files are removed from the connected database, created and stored in the selected folder. 

In a next step, the access .mdb file needs to be repaired to reduce the size. 

1.	Close the BioNumerics database.
2.	Navigate with windows explorer to the path where your name_db.mdb file is stored. 
3.	Open the name_db.mdb file with access.
4.	Depending on the access version compact and repair the .mdb file as follows: 

i. In access 2007: select the left upper button > select Manage > Compact and repair database.
ii. In access 2000, 2002, 2003: Choose tools > Database utilities > Compact and repair database.

The access .mdb file is now reduced in size (compare the size of the mdb file before and after repair). 

IMPORTANT STEP: Open the BioNumerics database and select Database > Connected databases > Select the database from the list > select Edit > and uncheck Store trace files in database. When importing new trace files in the connected database, the trace files will not be stored in the connected database anymore.


