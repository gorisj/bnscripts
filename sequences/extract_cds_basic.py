"""
Extract a CDS from input sequence in a very naive manner.

Check sequence and reverse complement for start and stop codon (considering divisibility by 3
Select longest CDS and place in new experiment.

Input: source experiment called 'src' (if your experiment has another name, change it in def main()section)
Output: destination experiment called 'dest' (if your experiment has another name, change it in def main()section)

Requestor: Margo Diricks
Author: Jan Deneweth
Date: 2019-06-03
"""

from Bio.Seq import Seq
from Bio.Alphabet.IUPAC import IUPACAmbiguousDNA

import bns


# -- Globals --

#START_CODONS = ['ATG', 'CTG', 'GTG', 'TTG', ]
START_CODONS = ['ATG']
STOP_CODONS = ['TAG', 'TAA', 'TGA', ]


# -- Public Functions --

def _extract_cds_basic(sequence):
	"""
	Extract the first coding sequence (CDS) range within given sequence.
	:param str sequence: uppercase nucleotide sequence containing a CDS
	:return (int, int): coding sequence range as tuple of first and last nucleotides
	:raise ValueError: when no CDS can be found
	"""
	# OBSOLETE: better to find largest than first CDS
	
	# Find first start codon:
	candidates = [sequence.find(codon) for codon in START_CODONS]
	candidates = [candidate for candidate in candidates if candidate >= 0]
	first_raw = min(candidates) if candidates else -1
	if first_raw < 0:
		raise ValueError("Could not find CDS, no start codon")
	# Find subsequent stop codon
	for index in range(first_raw+3, len(sequence)-2, 3):
		codon = sequence[index:index+3]
		if codon in STOP_CODONS:
			last_raw = index
			break
	else:
		raise ValueError("Could not find CDS, no stop after first start codon")
	# Return range
	return first_raw, last_raw + 3


def _extract_cds_largest(sequence):
	"""
	Extract the largest coding sequence (CDS) range within given sequence.
	:param str sequence: uppercase nucleotide sequence containing a one or more CDS's
	:return (int, int): largest coding sequence range as tuple of first and last nucleotides
	:raise ValueError: when no CDS can be found
	"""
	seqlen = len(sequence)
	best_cds = None
	best_cds_len = 0
	for start_index in range(0, seqlen-5):
		# Find start codons
		if sequence[start_index:start_index+3] in START_CODONS:
			# Find first stop codon
			for stop_index in range(start_index+3, seqlen-5, 3):
				if sequence[stop_index:stop_index+3] in STOP_CODONS:
					# Check if better CDS
					cds_len = (stop_index+3) - start_index
					if cds_len > best_cds_len:
						best_cds = (start_index, stop_index+3)
						best_cds_len = cds_len
	if not best_cds:
		raise ValueError("Could not find any CDS in sequence")
	return best_cds


def extract_cds(sequence):
	"""
	Extract the largest CDS from the given sequence and its reverse complement
	:param str sequence: nucleotide sequence containing one or more CDS's
	:return str, None: longest first CDS, or None if none found
	"""
	sequence = sequence.upper()  # Ensure all uppercase
	revcomp = Seq(sequence, alphabet=IUPACAmbiguousDNA()).reverse_complement().tostring()
	# Try to find the best CDS
	best_cds = None
	for seq in [sequence, revcomp]:
		try:
			start, stop = _extract_cds_largest(sequence=seq)
		except ValueError as e:
			continue
		cds = seq[start:stop]
		if not best_cds or len(cds) > len(best_cds):
			best_cds = cds
	# Return result
	return best_cds

	
# -- Run as Script --

def test():
	"""Run test"""
	testseq = "nnnnATGnnnnnnnnnTAAnnnn"
	testrev = "nnnnTTAnnnnnnnnnCATnnnn"  # Reverse complement of previous sequence
	resultseq = extract_cds(sequence=testseq)
	resultrev = extract_cds(sequence=testrev)
	print(resultseq == resultrev)  # Should be True
	print(resultseq)
	print(resultrev)
	

def main():
	"""Extract CDS from selected entries and experiment to new"""
	selected = list(bns.Database.Db.Entries.IterViewSelection())
	
	#source_exper_name = 'src'
	source_exper_name = 'HA' #ExampleMargo
	#output_exper_name = 'dest'
	output_exper_name = 'HA_CDS' #ExampleMargo
	experdata = bns.Sequences.Sequence()
	
	for entry in selected:
		# Get sequence
		experdata.Load(entry.Key, source_exper_name)
		byref = {'seq': ''}
		experdata.Get(byref=byref)
		seq = byref['seq']
		# Get CDS
		cds = extract_cds(sequence=seq)
		# Write to new experiment
		if cds:
			if bns.Database.Experiment(entry.Key, output_exper_name).ExperID:
				experdata.Load(entry.Key, output_exper_name)
			else:
				experdata.Create(output_exper_name, "", entry.Key)
			experdata.Set(seq=cds)
			experdata.Save()


if __name__ == '__main__':
	main()


#
#
# END OF FILE
