---
title: "SetTraceFilesLocal_SCF_ABI"
date: 2011-07-18T16:28:13+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---



Links the sequence trace file of SCF and ABI chromatograms stored in the connected database, to the local files.

When running the script on an access database, perform the compact and repair action after running the script to reduce the size of the database.

