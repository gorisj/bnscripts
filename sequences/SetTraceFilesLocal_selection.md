---
title: "SetTraceFilesLocal_selection"
date: 2009-12-17T15:41:41+01:00
draft: false
author: "Paul Vauterin"
scriptlanguage: "bns"
---


For a selection of entries the trace files are created in an appointed folder. A unique ID is default given to each trace file; optionally the file names can be 1) composed of the key and experiment name and 2) removed from the connected database. This script works for ABI and SCF files.

This script will generate for all selected entries the sequence trace files that are stored inside connected database, and store them as files in a folder. Optionally the trace files can be removed from the database.

1. Make a backup of your connected database before proceeding.
2. Open your database with BioNumerics, and select some/all entries.
3. Run the script with Scripts > Run script from file.
4. Select the sequence type from the list.
5. The default path where the trace files will be stored is written in the lower part of the dialog box (default path: sourcefiles\contig folder of the database). The path can be changed by the user if desired.
6. If the option Write key and experiment in file names is checked, the file names will include the key and the sequence type name.
NOTE: Make sure no special signs (% / \ ? * : | "" < > .) are present in the key and sequence type names.
7. If the option Remove from the database is checked, the trace files will be removed from the database and the location of the trace files will be stored in the database instead.
8. Press <OK>.

If the option Remove from the database was checked, the trace files are removed from the database and the location of the trace files is stored in the database. In that event, the access .mdb file still needs to be repaired to reduce the size:

1. Close the BioNumerics database.
2. Navigate with windows explorer to the path where your "name_db".mdb file is stored.
3. Open the "name_db".mdb file with access.
4. Depending on the access version compact and repair the .mdb file as follows:
i. In access 2007: select the left upper button > select Manage > Compact and repair database.
ii. In access 2000, 2002, 2003: Choose tools > Database utilities > Compact and repair database.

The access .mdb file is now reduced in size (compare the size of the mdb file before and after repair).

IMPORTANT STEP: Open the BioNumerics database and select Database > Connected databases > Select the database from the list > select Edit > and uncheck Store trace files in database. When importing new trace files in the connected database, the trace files will not be stored in the connected database anymore.

