"""Relocate_connected_tracefiles.py

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""

import os
import bns
import re
import shutil

Db 				= bns.Database.Db
CDB				= bns.ConnectedDb
Bld				= bns.Windows.XmlDlgBuilder
Experiment 	= bns.Database.Experiment
MessageBox	= bns.Util.Program.MessageBox


class SeqExpTypeDlg(Bld.Dialogs):
	""" Dialog to pick the sequence experiment type"""
	def __init__(self, seqExpTypes):
		Bld.Dialogs.__init__(self, 'Sequences', remembersettings='session')
		self.DbSrcFileDirTag	= os.path.join('[DBSOURCEDIR]','contig')
		
		self.radioChoices		= 	["Move trace files to source files directory ", "Re-link trace files"]		
		
		self.radioCtrl		= Bld.Radio('radioCtrl',self.radioChoices , charWidth = 30, vertical = True, hasfocus = True, default = self.radioChoices[0], OnChange = self._OnRadioChanged )
		frame1					= Bld.Cell([[self.radioCtrl]], "Action")
		
		disable					= Bld.Dependency(Bld.ValueOf(self.radioCtrl.Id) != self.radioChoices[0]) 
		self.dirCtrl		= Bld.File('dirCtrl', [], charWidth = 35, dlgTitle = "", default = self.DbSrcFileDirTag, rememberDir = False, isdir = True, dependency = disable)
		frame2					= Bld.Cell([[self.dirCtrl]], "Destination folder")

		listItems				= [Bld.ListItem(tpe) for tpe in seqExpTypes]
		listVals 				= Bld.ListValues(listItems, translate=False)
		self.listCtrl		= Bld.SimpleList("SeqListCtrl", listVals, 6, 20, multi = True)
		frame3					= Bld.Cell([[self.listCtrl]], "Sequences")		
		
		grid = Bld.Grid([[frame1],
				[frame2],
				[frame3]])
		
		
		simple = Bld.SimpleDialog(grid, onOk = self._OnOk)
		self.AddSimpleDialog("Move trace files", simple)	
			
	
	def _OnRadioChanged(self, args):
		self.dirCtrl.Enabled = (self.radioCtrl.GetValue() != self.radioChoices[0])
		if not self.dirCtrl.Enabled:
			self.dirCtrl.SetValue(self.DbSrcFileDirTag)
	
	
	def _OnOk(self, args):
		""" check if a choice is made """
		errors = []
		if not self.listCtrl.GetValue():
			errors.append("Please select a sequence experiment type.")
		if (self.radioCtrl.GetValue() == self.radioChoices[1]) and not self.dirCtrl.GetValue():
			errors.append("Please select the directory containing the moved traces files.")
		if self.dirCtrl.GetValue():
			if (self.dirCtrl.GetValue() != self.DbSrcFileDirTag) and (not os.path.exists(self.dirCtrl.GetValue())):
				errors.append("Your input directory does not exist")
		
		if errors:	
			MessageBox("Error", '\n'.join(errors), "exclamation")
			args['result'] = '0'
	
		
	def GetExpChoices(self):
		return self.listCtrl.Multi
		
		
	def GetDir(self):
		if self.radioCtrl.GetValue() == self.radioChoices[0]:
			return self.DbSrcFileDirTag
		else:
			return self.dirCtrl.GetValue()


def GetSeqTypeList():
	""" Load the self.seqs list with all sequence exp types"""
	return	[e.Name for e in Db.ExperimentTypes if e.Class == 'SEQ']
					

def UpdateTraceFileLocation(seqExpTypes, newDir):
	""" Update the location of the trace files belonging to a seq exp type
		and finish with a warning
	"""
	contigCols 		= ["CONTIGFILE", "DATA", "INFO", "TRACEID"]
	cntReloc			= 0
	cntNReloc			= 0
	
	otSeqs		= CDB.ObjectType('SEQUENCES')
	otTraces	= CDB.ObjectType('SEQTRACEFILES')
	
	action		= CDB.ObjAction("Update trace file locations")
	seqFlds		= {'KEY': '', 'EXPERIMENT': ''}
			
	parse1		= re.compile('DataType=\"(.*?)\".*?>')
	parse2		= re.compile('\"Link\">(.*?)<')
	parse3		= re.compile('\[DBSOURCEDIR\]')
	
	for entry in Db.Selection:
		for seqExp in seqExpTypes:
			seqFlds['KEY'] = entry.Key
			seqFlds['EXPERIMENT'] = seqExp
			
			if Experiment(entry.Key, seqExp).ExperID:
				seq = bns.Sequences.Sequence()
				seq.Load(entry.Key, seqExp)
				ctgfname = seq.GetContig()
			
				objID		= CDB.ObjectID(otSeqs, seqFlds)
				
				records 	= CDB.DbRecordSet('SEQTRACEFILES', contigCols)
			
				traceIDs		= []
				dataStrs		= []
				infoStrs		= []
				
				for record in records.Select(bns.ConnectedDb.DbColumn('CONTIGFILE').Equal(ctgfname)):
					str1		= parse1.findall(record['DATA'])[0]				
					
					if str1 == 'Link':
						hit1					= parse3.search(newDir)
						oldPath				= parse2.findall(record['DATA'])[0]
						hit2					= parse3.search(oldPath)
						newPath				= os.path.join(newDir, os.path.basename(oldPath))	
						if hit2 != None:
							oldConcrPath	= oldPath.replace('[DBSOURCEDIR]', Db.Info.SourceFilesDir)
						else:
							oldConcrPath	= oldPath
					else:
						break
					
					if os.path.exists(oldConcrPath):
						if oldPath == newPath:
							break
	
						__bnscontext__.SetBusy("Relocating sequence trace files for entry {0}".format(entry.Key))				
						if hit1 != None:
							newConcrPath = newPath.replace('[DBSOURCEDIR]', Db.Info.SourceFilesDir)
						else:
							newConcrPath = newPath
						
						base		= os.path.splitext(os.path.basename(oldPath))
						
						while os.path.isfile(newConcrPath):
							autonumber		= bns.ConnectedDb.DbInterface('').GenerateAutoNumber('test')
							newName				= ''.join([base[0], str(autonumber), base[1]])
							newConcrPath	= 	os.path.join(os.path.dirname(newConcrPath), newName)
														
						shutil.copyfile(oldConcrPath, newConcrPath)
						newPath = os.path.join(os.path.dirname(newPath), os.path.basename(newConcrPath))
						
						dataStr		= record['DATA']
						dataStrs.append(dataStr.replace(oldPath, newPath))
						infoStr		= record['INFO']
						infoStrs.append(infoStr.replace(oldPath, newPath))
						traceIDs.append(record['TRACEID'])
						cntReloc	+= 1
						__bnscontext__.SetBusy("")
						
					else:
						bns.Util.Warnings.Add("trace file {0} not found!".format(oldPath))
						cntNReloc += 1
						break						
			
				for i in xrange(len(traceIDs)):
					where = CDB.DbColumn('TRACEID') == traceIDs[i]
					objaction = CDB.ObjectSpecifAction(action, objID)
					objaction.UpdateSingleField(otTraces, 'DATA', where.Expression, dataStrs[i])
					objaction.UpdateSingleField(otTraces, 'INFO', where.Expression, infoStrs[i])
					objaction.Close()
	action.Close()
	
	message		= "{0} trace files were relocated.\n".format(cntReloc)
	if cntNReloc > 0:
		r = MessageBox("Warning",
					message + "{0} trace file{1} {2} not found."
					.format(cntNReloc, '' if cntNReloc == 1 else 's', 'was' if cntNReloc == 1 else 'were')
					,  'exclamation')
		bns.Util.Warnings.Dump()
			
	else:
		if cntReloc > 0:
				MessageBox("Info", message, '')
		else:
			MessageBox("Info", "No trace files were relocated.", '')


if not Db.Selection:
	raise RuntimeError("No entries were selected!")

seqExpTypes = GetSeqTypeList()
if not seqExpTypes:
	raise RuntimeError("No sequence experiment types found!")

dlg = 	SeqExpTypeDlg(seqExpTypes)
if dlg.Show():
		UpdateTraceFileLocation(dlg.GetExpChoices(), dlg.GetDir())

	
	
	
					
	
		



