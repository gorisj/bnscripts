---
title: "Find_path_trace_files"
date: 2007-08-24T17:03:28+01:00
draft: false
author: "Jill Dombrecht"
scriptlanguage: "bns"
---



For the selected entries in the database, the paths of the trace files for the selected sequence type are listed in a new window. This script is only valid if the contigs consist of two sequences (eg. forward and reverse).

