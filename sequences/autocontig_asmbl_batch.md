---
title: "Autocontig_asmbl_batch"
date: 2007-09-13T17:01:31+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---



This script will auto-assemble trace files for an entry based on one or more files. Will continue when sequence is not a member of a contig.

