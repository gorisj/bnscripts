---
title: "extract_cds_basic"
date: 2019-06-03T12:14:00+01:00
draft: false
author: "Jan Deneweth"
scriptlanguage: "py"
---



Extracts a CDS from input sequence in a very naive manner.

Checks sequence and reverse complement for start and stop codon (considering divisibility by 3).
Selects longest CDS and places it in a new experiment.

Input: source experiment called 'src' (if your experiment has another name, change it in def main()section)
Output: destination experiment called 'dest' (if your experiment has another name, change it in def main()section)

