---
title: "Relocate_connected_tracefiles"
date: 2012-01-25T11:37:20+01:00
draft: false
author: "Brian West"
scriptlanguage: "bns"
---


Relocates the trace files after they have been moved, for a connected database only.

A python version (Relocate_connected_tracefiles.py) of this script exists as well. 


