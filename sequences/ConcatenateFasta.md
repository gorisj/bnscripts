---
title: "ConcatenateFasta"
date: 2014-03-17T09:41:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---



Workflow:
1. Save the script to your pc
2. In BioNumerics 7, select 'Scripts>Run script' from 'file' and browse to the script. 
3. You will get a dialog where you can browse to the original fasta file. 
4. The script will then make a file with the concatenated sequences (seperated by a pipe '|') in the same folder as the original file. This file can then be imported into BioNumerics using the normal import procedure of fasta files.


