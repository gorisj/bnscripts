import os 
import bns 
import itertools
import numpy as np

from collections import defaultdict
import xml.etree.cElementTree as ET

Entries = bns.Database.Db.Entries
Dlg = bns.Windows.XmlDlgBuilder
ExperimentTypes = bns.Database.Db.ExperimentTypes
MessageBox = bns.Util.Program.MessageBox
Entries = bns.Database.Db.Entries
MessageBox = bns.Util.Program.MessageBox
EntryField = bns.Database.EntryField
rc = bns.ConnectedDb.ObjectReadContext()


#==== settings ==========================================================================

# FIELD = 'BN_Cluster'
# refFIELD = 'Reference'

# denovoExperimentName = 'denovo'
viewID = '__AllLoci__'
# experName = 'wgMLST'	
# qualExperName = 'quality'
# clusterName = 'Cluster1'
# sharedGenomeViewId = '__All__'
# numberOfCachedRecords = 200


#==== do not modify beyond this line ====================================================

#grab the current quality experiment type
from wgMLST_Client.wgMLSTSchema import Schema
currSchema = Schema.GetCurrent()
qualExperName = bns.Database.ExperimentType(currSchema.QualityExperTypeID).Name

#grab the current wgMLST experiment type number
experName = bns.Database.ExperimentType(currSchema.WgMLSTExperTypeID).Name

#grab the current wgMLST experiment type number
denovoExperimentName = bns.Database.ExperimentType(currSchema.DeNovoExperTypeID).Name


class SelectNameDlg(Dlg.Dialogs):
	def __init__(self, names, nameType, maxValue):
		Dlg.Dialogs.__init__(self, 'SelectNameDlg')
		self.name = ''
		self.namesList = Dlg.Drop('namesList', names, 10, 30, canEdit=False, default=names[0])		
		grid = [ [Dlg.StaticText("The following {0} all occurred {1} times, please select one:".format(nameType, maxValue))],
						[[["Select {0}:".format(nameType), self.namesList]]]]
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Select cluster", simple)
	
	def onOk(self, args):
		self.name = self.namesList.GetValue()
	
def ShowMessage(title, message):
	bns.Util.Program.MessageBox(style = 'inform', title = title, content=message)
	
def GetN50(entry, qualExperName):
		if bns.Database.Experiment(entry, qualExperName).IsPresent():
			qualExper = bns.Characters.CharSetType(qualExperName)
			qualData = bns.Characters.CharSet()
			qualData.Load(entry.Key,qualExperName)
			nr = qualExper.FindChar("N50")
		return qualData.GetVal(nr)

def CalculateRef(charPresences, entries, exper, qualExper):

		
	#self.data.LoadExperData(self.experNr)
	nEntries = len(entries)
	#find out the relative importance of each character
	presences = charPresences.values()
	nrChrs = 	len(presences[0])		
	charWeights = np.zeros(nrChrs)
	for entry, presences in charPresences.iteritems():			
		charWeights+=presences		
	charWeights /= (float(nEntries))
	totCharWeights = sum(charWeights)
		
	#for each entry, calculate its weight
		
	entryWeights = { entry: np.sum(np.multiply(charWeights, p))/totCharWeights for entry, p in charPresences.iteritems() }
		
	#for each entry, grab its N50
	entryN50s = { entry : GetN50(entry, qualExper) for entry in charPresences }
	maxN50 = max(entryN50s.values()) 
	for entry, n50 in entryN50s.iteritems():
		entryN50s[entry] = n50 / maxN50
				
	#calculate weighted score for 
	scoreWeights = { 'entryWeight': 0.5, 'N50': 0.5 }				
	entryScores = { entry : entryWeights[entry] * scoreWeights['entryWeight'] + entryN50s[entry] * scoreWeights['N50'] for entry in entryWeights }
		
	#find entry with maximal score
	maxEntryScore = max(entryScores.values())
	centralEntry = next(entry for entry, entryScore in entryScores.iteritems() if entryScore == maxEntryScore)
	score = 100*entryWeights[centralEntry] 
	scoreRange = [min(entryWeights.values())*100, max(entryWeights.values())*100]
	return centralEntry
		
def DetermineCharPresences(entries, chrIDs, exper, viewID):
	charPresences = {}
	for entry in entries:
		presences = []
		if bns.Database.Experiment(entry, exper).IsPresent():
			charset = bns.Characters.CharSet()
			charset.Load(entry.Key,exper)
			for i in xrange(len(chrIDs)):
				presences.insert(i,charset.GetPresent(chrIDs[i]))
			charPresences[entry] = presences
	return charPresences
	
def getLinks():
	links= []
	experimentTypes = GetExperTypes()
	for exptype in experimentTypes:
		if exptype.Class == 'SRS':
			for entry in Entries:
				seqReadSet = bns.SequenceReadSets.SeqReadSet()
				try:
					seqReadSet.Load(entry.Key, exptype.ID)
					if seqReadSet.GetLinks():
						links.append([entry.Key, exptype.ID ,'\t'.join(seqReadSet.GetLinks())])
				except Exception as e:
					print str(e)
	return links
	
def createMappingExperiment(key, entrykeys, ref, compWinId = None):	
	mappingExperimentName = 'Map2_'+ key
	i = 1
	while mappingExperimentName in bns.Database.Db.ExperimentTypes:
		seqExperType = bns.Database.Db.ExperimentTypes[mappingExperimentName]
		xSettings = ET.XML(seqExperType.Settings)
		expRef = xSettings.find('./Reference').text
		print ref, expRef
		if ref == expRef:
			return mappingExperimentName
		else:
			mappingExperimentName = 'Map_' + key + '_' + str(i)
			i = i+1
	seqExperType = bns.Database.Db.ExperimentTypes[mappingExperimentName]
	if seqExperType is None:
		seqExperType = ExperimentTypes.Create(mappingExperimentName, 'SEQ')		
	xSettings = ET.XML(seqExperType.Settings)
	xUseRefForMapping = xSettings.find('./Settings/UseRefForMapping')

	if xUseRefForMapping.text == '0':
		bns.CallBnsFunction('SeqTypeSetReferenceMapped', mappingExperimentName, True)
	return mappingExperimentName
	
#	for entryKey in entrykeys:
#		d = {'seq': ""}
#		seq = bns.Sequences.Sequence()
#		seq.Load(entryKey,'MapCluster1')
#		seq.Get(byref=d)
##		linkExper(entryKey,name,d['seq'])

#def linkExper(key,exper,sequence):
#	seq = bns.Sequences.Sequence()
#	if bns.Database.Experiment(key,exper).ExperID:
#		seq.Load(key,exper)
#	else:
#		seq.Create(exper,"",key)
#		seq.Set(sequence)
#		seq.Save()


def createRefEntry(denovoExperimentName):
	
		
	selection =  bns.Database.Db.Selection
	exper = bns.Characters.CharSetType(experName)
	allViews = exper.ViewGetList()
	chrIDs = []
	if viewID in allViews:
		chrStringIDs = exper.ViewGet(viewID).GetCharacters()
		for i in xrange(len(chrStringIDs)):
			chrIDs.insert(i, exper.FindChar(chrStringIDs[i]))
		charPresences = DetermineCharPresences(selection, chrIDs, experName, viewID)
	else:
		raise RuntimeError("The wgMLST view All Loci is not present in the database.")
	ref = CalculateRef(charPresences, selection, experName, qualExperName)
	key = ref.Key
	referenceKey = key + '_ref'
	
	mappingExperimentName = createMappingExperiment(key, selection, referenceKey)
	covTypes = ('tot', 'fwd', 'rev')
	cov = {}
	if Entries.IsPresent(referenceKey):
		entry = Entries[referenceKey]
	else:
		entry = Entries.Add(referenceKey)
	if bns.Database.Experiment(key, denovoExperimentName).ExperID:
		annSeq = bns.Sequences.AnnSeq()
		annSeq.Create(key, denovoExperimentName) # key and denovoExperimentName exists, so this loads the annotated sequence in the annSeq object
		seqData = bns.Sequences.SequenceData(key, denovoExperimentName)
		for covType in covTypes:
			if seqData.HasCoverage(rc, covType):
				cA, cC, cG, cT, cTot = [], [], [], [], []
				seqData.LoadCoverage(cA, cC, cG, cT, cTot, rc, covType)
				cov[covType] = cA, cC, cG, cT, cTot
		curves = seqData.GetCurveList()
		
			
		annSeq.SetExp(mappingExperimentName)
		annSeq.SetKey(referenceKey)
		annSeq.Save()
		seqRefData = bns.Sequences.SequenceData(referenceKey, mappingExperimentName)
		sequence = seqRefData.LoadSequence()
		d = {}
		for covType in covTypes:
			if covType in cov:
				cA, cC, cG, cT, cTot = cov[covType]
				d = {
					'Sequence': sequence,
					'CoverageType': covType,
					'CoverageA': cA,
					'CoverageC': cC,
					'CoverageG': cG,
					'CoverageT': cT,
					'CoverageTotal': cTot,
				}
		seqRefData.SaveData2(d) 
		annSeq.Save()
		
		
		resultStr = "The de novo sequence of {0} has been copied to the experiment type {1}".format(key, mappingExperimentName)
		return resultStr
		
	else:
		raise RuntimeError("The defined reference does not contain a sequence for the de novo experiment")

		
def findMostOccurringClusterAndReference():
	entrykeys = [entry.Key for entry in Entries if entry.Selection]
	if entrykeys:
		BNClusterValues = defaultdict(int)
		BNRefValues = defaultdict(int)
		for entryKey in entrykeys:
			if EntryField(entryKey, FIELD).Content:
				BNClusterValues[EntryField(entryKey, FIELD).Content] +=1
			if EntryField(entryKey, refFIELD).Content:
				BNRefValues[EntryField(entryKey, refFIELD).Content] +=1
			
		if BNClusterValues == {}:
			raise RuntimeError("No clusters present in selection!")
		else:
			maxValue = max(BNClusterValues.values())
			
		if BNRefValues == {}:
			raise RuntimeError("No references defined in selection!")
		else:
			maxRefValue = max(BNRefValues.values())
			

		clusterNames = [key for key in BNClusterValues.keys() if BNClusterValues[key] == maxValue]
		if len(clusterNames) > 1:
			dlg = SelectNameDlg(clusterNames, "clusters", maxValue)
			if dlg.Show():
				clusterName = dlg.name
			else:
				raise RuntimeError("No cluster selected!")
		else:
			clusterName = clusterNames[0]
		mappingExperimentName = createMappingExperiment(clusterName, entrykeys, None)
			
		referenceKeys = [key for key in BNRefValues.keys() if BNRefValues[key] == maxRefValue]
		if len(referenceKeys) > 1:
			dlg = SelectNameDlg(referenceKeys, "references", maxRefValue)
			if dlg.Show():
				referenceKey = dlg.name
			else:
				raise RuntimeError("No reference selected!")
		else:
			referenceKey = referenceKeys[0]
		refEntry = createRefEntry('denovo', mappingExperimentName, referenceKey)
	else:
		raise RuntimeError("No entries selected!")

def MakeMappingExpMostOccurringCluster():
	entrykeys = [entry.Key for entry in Entries if entry.Selection]
	if entrykeys:
		BNClusterValues = defaultdict(int)
		for entryKey in entrykeys:
			if EntryField(entryKey, FIELD).Content:
				BNClusterValues[EntryField(entryKey, FIELD).Content] +=1
			
			
		if BNClusterValues == {}:
			raise RuntimeError("No clusters present in selection!")
		else:
			maxValue = max(BNClusterValues.values())
			
		
		clusterNames = [key for key in BNClusterValues.keys() if BNClusterValues[key] == maxValue]
		if len(clusterNames) > 1:
			dlg = SelectNameDlg(clusterNames, "clusters", maxValue)
			if dlg.Show():
				clusterName = dlg.name
			else:
				raise RuntimeError("No cluster selected!")
		else:
			clusterName = clusterNames[0]
		#mappingExperimentName = createMappingExperiment(clusterName, entrykeys, None)
		return clusterName
		
	else:
		raise RuntimeError("No entries selected!")
		
if '__name__' == '__main__' or '__debug__':	
	
#	ids = [bns.Windows.BnsWindow.GetID(nr) for nr in xrange(bns.Windows.BnsWindow.GetCount())]
#	compId = [Id for Id in ids if bns.Windows.BnsWindow(Id).GetClassname() == 'Comparison'][0]
#	comp = bns.Comparison.Comparison(compId)
#	referenceKey = comp.GetSelEntry()
#	comp.Close()

#	mappingExperimentName = MakeMappingExpMostOccurringCluster()
	resultStr = createRefEntry(denovoExperimentName)
	ShowMessage("Mapping experiment was succesfully prepared",resultStr)

