---
title: "create_short_reads_from_fasta"
date: 2017-10-06T14:54:00+01:00
draft: false
author: "Dieter De Coninck"
scriptlanguage: "py"
---


Creates random short reads from a fasta file. 

The variables 'fastaFlName' (fasta input file), and 'outputDir' (output dir for generated fastq files) should be modified to suit the user's needs prior to running the script. 

Some variable are defined that allow the user some control over the output: 
* 'nReads': the number of reads that should be generated
* 'readLength': the length of the reads
* 'insertSize': the insert size between forward and reverse read. 


