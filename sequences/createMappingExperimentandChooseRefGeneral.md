---
title: "CreateMappingExperimentandChooseRefGeneral"
date: 2016-06-29T09:30:38+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---



Script that chooses most appropriate reference for selection of entries in the main window based on the number of wgMLST loci in common with the rest of the selection (based on presence/absence) and N50 (50/50 weight). A mapping experiment is then created with Map2KeyReference as name and a new entry is made from the reference (key_ref) and set as reference in the new mapping experiment type.


