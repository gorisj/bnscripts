
fastaFlName = r'C:\Users\Public\Documents\AnalysisDay_Teerarat\R40_542033_S79_contigs.fasta'
outputDir = r'C:\Users\Public\Documents\AnalysisDay_Teerarat'


nReads = 800000
readLength = 300
insertSize = 400


from Bio import SeqIO, Seq
import random, os
import gzip

def RC(seqStr):
	return Seq.reverse_complement(seqStr)	

key = os.path.splitext(os.path.split(fastaFlName)[1])[0]

seqs = [str(record.seq) for record in SeqIO.parse(fastaFlName, 'fasta') if len(record.seq)>2000]


readFl1 = os.path.join(outputDir, key+ '_1.fastq.gz')
readFl2 = os.path.join(outputDir, key+ '_2.fastq.gz')

qc = ''.join(['c']*readLength)
with gzip.open(readFl1, 'w') as oFile1, gzip.open(readFl2, 'w') as oFile2:
	for i in xrange(nReads):
		seq = seqs[random.randint(0, len(seqs)-1)]
		iSize = max(0, int(random.normalvariate(insertSize, 0.25*insertSize / 2.0)))
		pos1 = random.randint(0, len(seq)-iSize-2*readLength) 
		if pos1+iSize+2*readLength<=len(seq):
			if random.random()>0.5:
				oFile1.write('@{}:{}\n{}\n+{}:{}\n{}\n'.format(key, i+1, seq[pos1:pos1+readLength], key, i+1, qc))
				oFile2.write('@{}:{}\n{}\n+{}:{}\n{}\n'.format(key, i+1, RC(seq[pos1+readLength+iSize:pos1+readLength+iSize+readLength]), key, i+1, qc))
			else:
				oFile2.write('@{}:{}\n{}\n+{}:{}\n{}\n'.format(key, i+1, seq[pos1:pos1+readLength], key, i+1, qc))
				oFile1.write('@{}:{}\n{}\n+{}:{}\n{}\n'.format(key, i+1, RC(seq[pos1+readLength+iSize:pos1+readLength+iSize+readLength]), key, i+1, qc))
			

