---
title: "Convert Kodon to BN"
date: 2018-03-12T16:09:25+01:00
draft: false
author: "Marc Vauterin"
scriptlanguage: "bns"
---


Import kodon sequences (from a local kodon database) into a connected BioNumerics 6.1 database: 

1. Create a new, empty connected BioNumerics database
2. Create a sequence type
3. Run the script
4. Navigate to the kodon database folder 
5. Select the sequence type you wish to link the sequences to
6. The sequences are imported in the database and the Kodon information fields are automatically created in the BioNumerics database.

