---
title: "Export_entries_as_fasta"
date: 2009-02-11T16:37:27+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "bns"
---



For every selected entry in the database, a separate fasta file is created, containing the sequences of the selected sequence type(s).

