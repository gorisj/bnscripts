---
title: "SetTraceFilesLocal"
date: 2009-01-26T16:35:13+01:00
draft: false
author: "Koen Janssens"
scriptlanguage: "bns"
---



Links the sequence trace file stored in the connected database, to the local files.

When running the script on an access database, perform the compact and repair action after running the script to reduce the size of the database.

