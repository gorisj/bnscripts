"""ConcatenateFasta.py

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Concatenate all the sequence records in a FASTA file together.

"""



import bns, os
from Bio import SeqIO

Dlg = bns.Windows.XmlDlgBuilder
MessageBox = bns.Util.Program.MessageBox

class FileSelectDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'FileSelectDlg')
		
		self.flName = ""
		
		# the file controls
		exts = [Dlg.Extension('fa', "FASTA file"), Dlg.Extension("*", "Any file")]
		self.selFile = Dlg.File('f1', exts, 30, "Select a FASTA file")
		
		# dialog layout
		grid = [["File:", self.selFile]]
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Select a FASTA file", simple)
	
	def onOk(self, args):
		"""Get the selected file"""
		if not self.selFile.GetValue():
			MessageBox("Error", "Please select a FASTA file to concatenate", 'exclamation')
			args['result'] = 0
			return
		self.flName = self.selFile.GetValue()

def concatenateFasta(filename):
	outputfile = '{0}_concatenated{1}'.format(*os.path.splitext(filename))
	with open(outputfile, 'w') as f:
		f.write("> {0}\n".format(os.path.splitext(os.path.basename(filename))[0]))
		sequences = (str(record.seq) for record in SeqIO.parse(open(filename), 'fasta'))
		f.write('|'.join(sequences))
	MessageBox("Information", "Concatenated FASTA file written to:\n{0}".format(outputfile), 'information')


def main():
	dlg = FileSelectDlg()
	if dlg.Show():
		concatenateFasta(dlg.flName)
	
	
if __name__ == '__main__':
	main()







