"""ImportDouglasScientific.py

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no liability for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Import of Douglas Scientific csv files using the SNPcalling plugin.
NOTE: The format parsing is based on only one example file.

Requested by:
	Ian Davies (idavies@Identigen.com)
	IdentiGen

"""
#//$autorun;event=CreateMainWin;

import bns
from SNPcalling.ConnDb import ConnDb
from SNPcalling.Import import importBatch as ImportBatch
from SNPcalling.Dialogs.all_dialogs import AskMarkersMapDlgs, AskMarkerDlgs, SnpImportDlg, LinkConflictDlg
SetWaitCursor = bns.Windows.BnsWindow.SetWaitCursor

class ImportDouglasCsv(ImportBatch.Importer):	
	used_channels = ('FAM', 'VIC', 'ROX')
	def ParseDateTime(self):
		rundate = 'Date,'
		start = self.data.find(rundate)
		if start > -1:
			end = self.data.find('\n', start)
			rundatetime = self.data[start: end].split(',')
			if (len(rundatetime) == 2) and (len(rundatetime[1]) == len('-YYYYMMDDhhmmss-')) and (rundatetime[1][0] == rundatetime[1][-1] == '-'):
				datetimestr = rundatetime[1][1:-1]
				try:
					year = int(datetimestr[0:4])
					month = int(datetimestr[4:6])
					day = int(datetimestr[6:8])
					hour = int(datetimestr[8:10])
					minute = int(datetimestr[10:12])
					second = int(datetimestr[10:12])
					rundate = ConnDb.datetime.datetime(year, month, day, hour, minute, second)
					return ConnDb.strtime(rundate)
				except Exception:
					pass
		return ""
	
	def ParseData(self):
		# create data table
		try:
			start = self.data.index('Dye,')
		except ValueError:
			raise RuntimeError("Error in SNP file format; could not find start of data.")
		
		channels = {}
		while start > 0:
			end = self.data.find('\n', start)
			channel = self.data[start:end].split(',')[-1]
			
			start = self.data.find('<>', end)
			end = self.data.find('Dye,', start)
			channelTable = []
			for d in self.data[start:end].split('\n'):
				_cells = d.split(',')
				cells = [c.strip() for c in _cells]
				if any(cells):
					channelTable.append(cells)
			# !!! from the example file, we noted that
			# !!! the values table contains an extra comma at the end of each line
			# !!! the header row does not have a trailing comma
			for line in channelTable[1:]:
				line.pop()
			
			channels[channel] = channelTable
			start = end
		
		if any(ch not in channels for ch in self.used_channels):
			raise RuntimeError("Some required channels are not present.")
		
		channel0 = channels[self.used_channels[0]]
		N_snps1 = len(channel0)
		if N_snps1 <= 1:
			raise RuntimeError("No SNP experiment rows found.")
		if any(len(channels[ch]) != N_snps1 for ch in self.used_channels):
			raise RuntimeError("Inconsistent SNP experiment row blocks.")
		
		N_snps2 = len(channel0[0])
		if N_snps2 <= 1:
			raise RuntimeError("No SNP experiment columns found.")
		if any(len(channels[ch][j]) != N_snps2 for ch in self.used_channels for j in xrange(N_snps1)):
			raise RuntimeError("Inconsistent SNP experiment column blocks.")
		N_snps = (N_snps1 - 1) * (N_snps2 - 1)
		
		self.snp_batchids = tuple(self.batchid for x in xrange(N_snps))
		self.snp_wellids = tuple('{0}-{1}'.format(r[0], c) for r in channel0[1:] for c in channel0[0][1:])
		if self.linksettings['usetemplate']:
			self.LinkTemplateFile()
		else:
			if not self.rememberedsettings:
				existingmarkers = ConnDb.CharSetTypeGetChars(ImportBatch.exptype)
				SetWaitCursor(False)
				dlg = AskMarkerDlgs(self.batchid, existingmarkers)
				if not dlg.IsOk():
					return False
				SetWaitCursor(True)
				marker = dlg.GetMarker()
				setnames = dlg.UseBatchWellAsName()
				if dlg.UseForAllBatches():
					self.rememberedsettings = True
					self.remembered_marker = marker
					self.remembered_setnames = setnames
			else:
				marker = self.remembered_marker
				setnames = self.remembered_setnames
			if setnames:
				snp_name = lambda x: '{0}_{1}'.format(self.batchid, self.snp_wellids[x])
			else:
				snp_name = lambda x: self.snp_wellids[x]
			self.snp_markers = tuple(marker for x in xrange(N_snps))
			self.snp_names = tuple(snp_name(x) for x in xrange(N_snps)) 
			self.snp_types = tuple('' for x in xrange(N_snps))
		self.snp_markers = self.GetSnpMarkers(self.snp_markers)
		
		channel_values = {}
		for ch in self.used_channels:
			channel_values[ch] = []
			for r in channels[ch][1:]:
				for c in r[1:]:
					channel_values[ch].append(float(c.replace(',', '.')))
		if 0.0 in channel_values['ROX']:
			raise RuntimeError("Some ROX signals are equal to 0, resulting in division by zero.")
		
		self.snp_Xvals = tuple(str(fam/rox) for fam, rox in zip(channel_values['FAM'], channel_values['ROX']))
		self.snp_Yvals = tuple(str(vic/rox) for vic, rox in zip(channel_values['VIC'], channel_values['ROX']))
		self.snp_ROXvals = tuple(str(rox) for rox in channel_values['ROX'])
		self.snp_calls = tuple('0.0' for x in xrange(N_snps))
		self.snp_qualities = tuple('0.0' for x in xrange(N_snps))
		
		self.markers = sorted(set(self.snp_markers))
		self.SanityCheck()
		self.new_markers = tuple(m for m in self.markers if ImportBatch.exptype.FindChar(m) < 0)
		self.ask_markers = self.new_markers
		self.marker_map = dict(((m, c), c) for m in self.ask_markers for c in ConnDb.GetAlleleCalls())
		return True
ImportBatch.Importers['DouglasScientificCsv'] = ImportDouglasCsv()
ImportBatch.ImporterNames['DouglasScientificCsv'] = "Douglas Scientific (.csv)"
