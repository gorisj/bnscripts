#//$MENU window=comparison;popup=Clustering;subpopup=Calculate;name=Choose reference for SNP analysis...;
import bns
import itertools
import numpy as np

__CHARNAME_N50__ = 'N50'

def ShowMessage(title, message):
	bns.Util.Program.MessageBox(style = 'inform', title = title, content=message)

class CalculateCentrotype(object):
	def __init__(self, winid, experNr, aspectNr, qualExper):
		self.winId = winid
		self.qualExper = qualExper
		self.n50CharNr = bns.Characters.CharSetType(self.qualExper).FindChar(__CHARNAME_N50__)
		self.data = bns.Comparison.ExperimentData(self.winId)
		self.experNr = experNr
		self.experName = self.data.GetExperName(self.experNr)
		self.aspectNr = aspectNr
		self.comp = bns.Comparison.Comparison(self.winId)
		
		self.centralEntryNr = None
		self.maxEntryWeight = 0
		
	def _GetExper(self, entryNr, experName):
		return bns.Database.Experiment(self.data.GetEnKey(entryNr), experName)
	
	def _IsExperPresent(self, entryNr, experName):
		return self._GetExper(entryNr, experName).IsPresent()
		
	def IsExperPresent(self, entryNr):
		return self._IsExperPresent(entryNr, self.experName)
	
	def GetN50(self, entryNr):
		if self._IsExperPresent(entryNr, self.qualExper):
			return self._GetExper(entryNr, self.qualExper).LoadOrCreate().GetVal(self.n50CharNr)
	
	def GetPresences(self, entryNr):
		presences = []
		self.data.GetExperCharPresent(self.experNr, entryNr, presences, self.aspectNr)
		return np.array(presences)
	
	def Run(self, debug=False):
		if debug:
			self.DoCalc({})
			self.DoRefresh({})
		else:
			bns.Windows.BnsWindow(self.winId).StartAsyncCalculation(self.DoCalc, self.DoRefresh, False)
		
	def DoCalc(self, args):
		
		comm = args.get('communication', bns.Windows.CalcCommunication())
				
		#grab the number of entries
		nEntries = self.data.GetEntryCount()
		
		#fetch the data matrix
		comm.SetMessage("Loading character presences...")
		self.data.LoadExperData(self.experNr)
		charPresences = {entryNr : self.GetPresences(entryNr) for entryNr in xrange(nEntries) if self.IsExperPresent(entryNr) }

		#find out the relative importance of each character		
		comm.SetMessage("Determining character presence weights...")
		charWeights = np.zeros(self.data.GetExperCharCount(self.experNr, self.aspectNr))
		for entryNr, presences in charPresences.iteritems():			
			charWeights+=presences		
		charWeights /= (float(nEntries))
		totCharWeights = sum(charWeights)

		#for each entry, calculate its weight
		comm.SetMessage("Determining entry weights...")
		entryWeights = { entryNr: np.sum(np.multiply(charWeights, p))/totCharWeights for entryNr, p in charPresences.iteritems() }
		
		#for each entry, grab its N50
		entryN50s = { entryNr : self.GetN50(entryNr) for entryNr in charPresences }
		maxN50 = max(entryN50s.values()) 
		for entryNr, n50 in entryN50s.iteritems():
			entryN50s[entryNr] = n50 / maxN50
				
		#calculate weighted score for 
		scoreWeights = { 'entryWeight': 0.5, 'N50': 0.5 }				
		entryScores = { entryNr : entryWeights[entryNr] * scoreWeights['entryWeight'] + entryN50s[entryNr] * scoreWeights['N50'] for entryNr in entryWeights }
		
		#find entry with maximal score
		maxEntryScore = max(entryScores.values())
		self.centralEntryNr = next(entryNr for entryNr, entryScore in entryScores.iteritems() if entryScore == maxEntryScore)
		self.score = 100*entryWeights[self.centralEntryNr] 
		self.scoreRange = [min(entryWeights.values())*100, max(entryWeights.values())*100]

	def DoRefresh(self, args):
		entry = bns.Database.Entry(self.data.GetEnKey(self.centralEntryNr))
		self.comp.SelectEntry(entry.Key)
		
		resultStr = '\n'.join([
				"Most central entry: {}".format(entry.DispName),
				"This entry has been highlighted in the comparison.\n",
				"Coverage score: {0:.2f}% from range [{1:.2f}%, {2:.2f}%]". format(self.score, self.scoreRange[0], self.scoreRange[1])
			])
		ShowMessage("Central entry result", resultStr)
		



data = bns.Comparison.ExperimentData(__bnswinid__)

#grab the current quality experiment type
from wgMLST_Client.wgMLSTSchema import Schema
currSchema = Schema.GetCurrent()
qualExperName = bns.Database.ExperimentType(currSchema.QualityExperTypeID).Name

#grab the current wgMLST experiment type number
wgMLSTExperName = bns.Database.ExperimentType(currSchema.WgMLSTExperTypeID).Name
experNr = next((i for i in xrange(data.GetExperCount()) if data.GetExperName(i)==wgMLSTExperName), None)
if experNr is None:
	raise RuntimeError("Could not find experiment type '{}' in the comparison.".format(wgMLSTExperName))

#grab the currently selected experiment type
#experNr = data.GetSelExperNr()

#get the current aspect of whatever the experiment is
aspectNr = data.GetSelAspectNr(experNr)

#this functionality only makes sense when using a wgMLST experiment type or a subset thereof
#do we need to push a dialog box in between this so that the user can change the experiment type?

a = CalculateCentrotype(__bnswinid__, experNr, aspectNr, qualExperName) 
a.Run(True)








