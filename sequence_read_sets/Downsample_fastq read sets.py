#Author: Klaas mensaert
#Goal: To downsample sequence read sets to a specified amount of bases
#To change in script: coverages and n = expected genome length of your organism
#input: Local fastQ files vb. ERR550665_1.fastq and ERR550665_2.fastq (you can download SRA fastQ files using the SRA toolkit: extract map in a specific folder e.g. C:\Users\margo\Documents\Margo > open command prompt > Type C: > type cd C:\Users\Margo\Documents\sratoolkit.2.9.2-win64\bin > type fastq-dump --split-files SRR5569208 to have seperated files in case of paired-end sequence sets
#output: downsampled fastQ files



import os, glob
import subprocess
import random

forced_redo = False



def read_fastq(fastq_file_path):
	reads = list()
	total_length=0

	with open(fastq_file_path,'r') as flin:
		read = []
		i=0
		for line in flin:
			line.strip()
			read.append(line)
			if i % 4 == 1: total_length += len(line)
			if i%4==3: reads.append(read); read = []
			i += 1

	mean_length = total_length / len(reads)
	return reads, mean_length

def write_reads(filePath, reads, indices):
	with open(filePath,"w") as wrfl:
		for i in indices:
			for read in reads[i]:
				wrfl.write(read)

if __name__=="__main__":
	# input_folders = [r"/mnt/e/poster_myco/input/fastq"]
	# output_folder = r"/mnt/e/poster_myco/output/fastq_perm"
	input_folders = [r"e:\poster_myco\input\fastq"]
	output_folder = r"e:\poster_myco\output\fastq_perm"

	random.seed(42)

	# check samples
	fastq_paths = dict()
	for folder in input_folders:
		for file in glob.glob(os.path.join( folder,"*_1.fastq")):
			fastq_name = os.path.split(file)[1].replace("_1.fastq", "")
			fastq_paths[fastq_name] = os.path.join(folder,file)

	for fastq_name, fastq_path in fastq_paths.iteritems():

		print(fastq_name)
		# make output folder
		results_folder = os.path.join(output_folder, fastq_name)
		# print(assembly_name)
		if not os.path.exists(results_folder): os.mkdir(results_folder)

		coverages = [1,2,3,4,5,6,7,8,9,10,15] #coverages that you want
		n = 5000000 #expected genome length of your species

		reads_fastq_fw, mean_length_fw = read_fastq(fastq_path)
		reads_fastq_rv, mean_length_rv = read_fastq(fastq_path.replace("_1.fastq", "_2.fastq"))

		mean_length = (mean_length_fw + mean_length_rv) / 2

		indices = [i for i, _ in enumerate(reads_fastq_fw)]



		for cov in coverages:
			outputpath = os.path.join(results_folder,"permreads_{}_{}Mbp_mL{}_1.fastq".format(fastq_name,str(cov*n/1000000),mean_length))

			read_n = cov * n / mean_length / 2

			random.shuffle(indices)

			indices_cov = indices[:read_n]
			write_reads(outputpath, reads_fastq_fw, indices_cov)
			write_reads(outputpath.replace("_1.fastq", "_2.fastq"), reads_fastq_rv, indices_cov)

