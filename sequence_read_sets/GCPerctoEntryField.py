#Author: Margo Diricks
#Function: This script stores the GC% of your srs and srs_trimmed datasets into information fields. By default the srs experiment is 'wgs' but this can be adjusted below.

import bns

#Define the name of the wgs experiment that you want to analyze for GC%
srsName = 'wgs'

#no changes required below this point

#Create new information fields to store the GC%
srsFields = ('GC_Perc '+srsName, 'GC_Perc '+srsName+'_trimmed')
for field in srsFields:
	name = field
	if (name) not in bns.Database.Db.Fields:
		thisField = bns.Database.Db.Fields.Add(name)
		thisField.Type = 'Number'
		
#Make sure that entries were selected
selectedEntries = bns.Database.Db.Selection
if len(selectedEntries)==0:
	raise RuntimeError("No entries selected.")
	
#Calculate and store GC% for all entries with a wgs experiment type available
	
for entry in selectedEntries:
	key = entry.Key
	if bns.Database.Experiment(key, srsName).ExperID:
		srs = bns.SequenceReadSets.SeqReadSet()
		srs.Load(key, srsName)
		info = {}
		srs.GetInfo(info)
		gcBaseCount = info['CountG'] + info['CountC']
		allBaseCount = info['CountA'] + info['CountC'] + info['CountG'] + info['CountT'] + info['CountOther']
		PercentGC_wgs = (100.0 * gcBaseCount) / allBaseCount if allBaseCount else 0
		GC_perc_wgs = entry.Field(srsFields[0])
		GC_perc_wgs.Content = '{0:.2f}'.format(PercentGC_wgs) #only display 2 digits after decimal
	
	
		#Calculate GC% of wgs_trimmed read set
	if bns.Database.Experiment(key, srsName+'_TrimmedStats').ExperID:
		srs.Load(key, srsName+'_TrimmedStats')
		info_trimmed = {}
		srs.GetInfo(info_trimmed)
		print info_trimmed
		gcBaseCount = info_trimmed['CountG'] + info_trimmed['CountC']
		allBaseCount = info_trimmed['CountA'] + info_trimmed['CountC'] + info_trimmed['CountG'] + info_trimmed['CountT'] + info_trimmed['CountOther']
		PercentGC_wgs_trimmed = (100.0 * gcBaseCount) / allBaseCount if allBaseCount else 0
		GC_perc_wgs_trimmed = entry.Field(srsFields[1])
		GC_perc_wgs_trimmed.Content = '{0:.2f}'.format(PercentGC_wgs_trimmed)
bns.Database.Db.Fields.Save()		
