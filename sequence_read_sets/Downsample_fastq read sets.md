---
title: "Downsample_fastq read sets"
date: 2019-06-05T15:32:00+01:00
draft: false
author: "Klaas Mensaert"
scriptlanguage: "py"
---




**Goal:** To downsample sequence read sets to a specified amount of bases

**To change in script:** coverages and n = expected genome length of your organism

**input:** Local fastQ files vb. ERR550665_1.fastq and ERR550665_2.fastq (you can download SRA fastQ files using the SRA toolkit: extract map in a specific folder e.g. C:\Users\margo\Documents\Margo > open command prompt > Type C: > type cd C:\Users\Margo\Documents\sratoolkit.2.9.2-win64\bin > type fastq-dump --split-files SRR5569208 to have seperated files in case of paired-end sequence sets

**output:** downsampled fastQ files

