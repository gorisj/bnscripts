---
title: "check_sanity_fastq"
date: 2017-10-06T15:20:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---



Checks whether a fastq file is correctly defined:

* Do the lengths of the lines for each read match?
* Does the read has a name?
* Does each sequence record start with '@'?
* Does each quality record start with '+'?
* Are the quality values defined within the correct range?

Be advised that the script only checks one fastq file at a time, and that it thus will not be able to check whether reads paired files have the same name, the same length etc...

The script variable 'fastqFlName1' should be altered to contain the path and filename of the fastq file to check. 

