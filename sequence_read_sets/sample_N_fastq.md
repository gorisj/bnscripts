---
title: "sample_N_fastq"
date: 2017-06-05T08:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "py"
---



Randomly downsamples paired fastq files.

Usage: python sample_N_fastq.py forward.fastq reverse.fastq 20000
The last argument specifies the number of read records you wish to retrieve. 

WARNING: this implementation allows for picking a single record multiple times.


