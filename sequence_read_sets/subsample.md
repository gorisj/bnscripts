---
title: "subsample"
date: 2017-06-05T08:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "py"
---



Subsamples from (paired) fastq files.

Using HTSeq, coded by Simon Anders from the Babraham Institute.

Run as: python subsample.py <fraction> <input file 1> <input file 2> <output file 1> <output file 2>


