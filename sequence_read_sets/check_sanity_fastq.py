
fastqFlName1 = r'C:\Users\Public\Documents\AnalysisDay_Ludivine\SRR5207335.fastq.gz'

import gzip
from itertools import izip_longest 

nReads = 0

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

print "Checking file {}".format(fastqFlName1)
with gzip.open(fastqFlName1) as oFile:
	for i, lines in enumerate(grouper(oFile, 4, ['','','',''])):
		rLen = len(lines[1])-1
		qLen = len(lines[3])-1
		
		if rLen!=qLen:	
			print 4*i+1, lines[0][:-1], " lengths don't match: {} != {}".format(rLen, qLen)
			
		nameLen = len(lines[0])-2
		if nameLen==0:
			print 4*i+1, lines[0][:-1], "no read name!"
		
		if lines[0] and lines[0][0]!='@':
			print 4*i+1, lines[0][:-1], "missing the '@' at the beginning of the sequence record !"
		if lines[2] and lines[2][0]!='+':
			print 4*i+1, lines[3][:-1], "missing the '+' at the beginning of the quality record !"
		
		minQual = min(lines[3][:-1])
		maxQual = max(lines[3][:-1])
		if minQual<'!' or maxQual>'~':
			print 4*i+1, lines[0][:-1], "inappropriate quality values, between '{}' and '{}'!".format(minQual, maxQual)
		
		
		
print "done"	
			
