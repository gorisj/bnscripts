import bns
import numpy as np
from string import maketrans

#=== tools ========================================================================================

basecomplement = maketrans('ACGTacgt', 'TGCATGCA')
def RevCompl(seq):
    return seq.translate(basecomplement)[::-1]

startCodons = set(['ATG', 'GTG', 'TTG', 'CTG'])
stopCodons = set(['TAG', 'TAA', 'TGA'])

revComplStartCodons = set([RevCompl(c) for c in startCodons])
revComplStopCodons = set([RevCompl(c) for c in stopCodons])


def FixSequence(seq):
	finalSequence = seq
	if (seq[:3] in revComplStopCodons) and (seq[-3:] in revComplStartCodons):
		finalSequence = RevCompl(seq)
#	if (finalSequence[:3] not in startCodons):
#		print "Error"
#	if (finalSequence[-3:] not in stopCodons):
#		print "Error"
			
	return finalSequence
	
def IsAcceptable(seq):	
	if (seq[:3] not in startCodons):
		return False
	if (seq[-3:] not in stopCodons):
		return False
#	for i in xrange(0, len(seq)-3, 3):
#		if seq[i:i+3] in stopCodons:
#				return False
	return True
	

def GetSubSeq(annSeq, start, stop):
	return annSeq.GetSeq(start, stop-1).upper()
	
def GetFtStart(ft):
	return ft.GetStart()-1

def GetFtEnd(ft):
	return ft.GetEnd()
	
def GetFtSeq(annSeq, ft):
	return GetSubSeq(annSeq, GetFtStart(ft), GetFtEnd(ft))

def HasAnnotationTag(ft, tag):
	return ft.GetQlfPresent(tag)!=-1
	
def GetAnnotationTag(ft, tag):
	idx = ft.GetQlfPresent(tag)
	if idx==-1:
		return ''
	else:
		return ft.GetQlfData(idx)
		
def GetAnnotationTags(ft):
	return {ft.GetQlfKey(i): ft.GetQlfData(i) for i in xrange(ft.GetQlfCnt())}
	
def GetFtsList(annSeq, key='CDS'):
	ftsList = []
	for ftsNr in xrange(annSeq.GetFtsCnt()):
		annFts = bns.Sequences.AnnFts()
		annSeq.GetFts(annFts, ftsNr)
		if annFts.GetKey() == key:
			seq = GetFtSeq(annSeq, annFts)
			seq = FixSequence(seq)
			if (len(seq)<50000) and (True or IsAcceptable(seq)):
				ftsList.append(annFts)
	return ftsList


#=== count number of bases covered in wgMLST calls ================================================

class BaseCounts(object):
	def __init__(self, locusCount, cdsCount, coveredCds, coveredLoci, multiCoveredCds, multiCoveredLoci, genomeLen):
		self.locusCount = locusCount
		self.cdsCount = cdsCount
		self.coveredCds = coveredCds
		self.coveredLoci = coveredLoci
		self.multiCoveredCds = multiCoveredCds
		self.multiCoveredLoci = multiCoveredLoci
		self.genomeLen = genomeLen
		
	@property
	def FractionCds(self):
		return float(self.coveredCds) / float(self.genomeLen) if self.genomeLen>0 else 0

	@property
	def FractionLoci(self):
		return float(self.coveredLoci) / float(self.genomeLen) if self.genomeLen>0 else 0
		
	@property
	def FractionMultiCds(self):
		return float(self.multiCoveredCds) / float(self.genomeLen) if self.genomeLen>0 else 0
		
	@property
	def FractionMultiLoci(self):
		return float(self.multiCoveredLoci) / float(self.genomeLen) if self.genomeLen>0 else 0
		
	@property
	def GenomeLen(self):
		return float(self.genomeLen)

	def Add(self, other):
		self.locusCount += other.locusCount
		self.cdsCount += other.cdsCount
		self.coveredCds += other.coveredCds
		self.coveredLoci += other.coveredLoci
		self.multiCoveredCds += other.multiCoveredCds
		self.multiCoveredLoci += other.multiCoveredLoci
		self.genomeLen += other.genomeLen
		
def CountBases(annSeq, locusIds):
	coveredCds = np.zeros(annSeq.GetLen())
	coveredLoci = np.zeros(annSeq.GetLen())
	ftsList = GetFtsList(annSeq)
	locusCount = 0
	cdsCount = 0
	for ft in ftsList:
		#seq = GetFtSeq(annSeq, ft)
		#if not IsAcceptable(seq): continue
		if HasAnnotationTag(ft, '/allele='):
			locusId = GetAnnotationTag(ft, '/locus_tag=')
			evidence = float(GetAnnotationTag(ft, '/evidence='))
			if locusId in locusIds and evidence==100.0:
				locusCount +=1
				start = GetFtStart(ft)
				end = GetFtEnd(ft)
				if start>=annSeq.GetLen() or end>=annSeq.GetLen(): continue
				coveredLoci[GetFtStart(ft):GetFtEnd(ft)] += np.full(GetFtEnd(ft)-GetFtStart(ft), 1)
		else:
			coveredCds[GetFtStart(ft):GetFtEnd(ft)] += np.full(GetFtEnd(ft)-GetFtStart(ft), 1)
			cdsCount+=1
#	print '{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(locusCount, cdsCount, np.sum(coveredCds>0), np.sum(coveredLoci>0), np.sum(coveredCds>1), np.sum(coveredLoci>1), annSeq.GetLen())	
	return BaseCounts(locusCount, cdsCount, np.sum(coveredCds>0), np.sum(coveredLoci>0), np.sum(coveredCds>1), np.sum(coveredLoci>1), annSeq.GetLen())
	


from wgMLST_Client.wgMLSTSchema import Schema
currSchema = Schema.GetCurrent()
wgmlstExperType = bns.Database.ExperimentType(currSchema.WgMLSTExperTypeID)
seqExperType = bns.Database.ExperimentType(currSchema.DeNovoExperTypeID)
qualExperType = bns.Characters.CharSetType(currSchema.QualityExperTypeID)
phageloci = set()

#wgmlstExperType = bns.Database.ExperimentType("wgmlst_enterobase_clean_2")
#seqExperType = bns.Database.ExperimentType('denovo_enterobase_clean_2')
#phageloci.update(set(['ECOLI_'+str(k) for k in range(22324, 22353)]))
#phageloci.update(set([line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemas\enterobase_ecoli_clean_v1\phageLoci_enterobase_v1.txt')]))

#wgmlstExperType = bns.Database.ExperimentType("wgmlst_am_v3_clean")
#seqExperType = bns.Database.ExperimentType("denovo_am_v3_clean")
#phageloci.update(set(['EC_'+str(k) for k in range(15304, 15333)]))


#wgmlstExperType = bns.Database.ExperimentType("wgmlst_am_v3_ext")
#seqExperType = bns.Database.ExperimentType("denovo_am_v3_ext")

#phageloci = set([line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\salmonella_cdc\phages_to_remove.txt')])

#phageloci = set(['SALM_'+line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\salmonella_cdc\am_ext_to_remove_salm.txt')])
#phageloci.update(set([''+line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\salmonella_cdc\to_ignore_bad_frame_salm.txt')]))
#phageloci.update(set(['SALM_'+line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\salmonella_cdc\phages_to_remove.txt')]))

#phageloci.update(set(['EC_'+str(k) for k in range(15304, 15333)]))


#the final scheme
#remove all loci 
#- from EB that we dont care about
#phageloci.update(set(['EC_'+str(k) for k in range(17350+1, 17400)]))

#- that are in overlap, but not in the core
#phageloci.update(set(['ECOLI_'+line.strip().split()[0] for line in open(r'D:\Users\Hannes\schemacreation\ecoli_cdc\cleanup2\am_eb_combined_to_remove.txt')]))

#- that are labeled phage, but are not in the core
#phageloci.update(set(['ECOLI_'+str(k) for k in [2583, 3825, 4002, 4362, 4367, 5105, 6098, 6525, 7060, 7497, 7648, 8144, 8752, 9675, 10351, 11318, 11386, 11946, 12888, 13140, 13279, 13483]]))


#the final scheme
#remove all loci 
#- from EB that we dont care about
#phageloci.update(set(['EC_'+str(k) for k in range(17350+1, 17400)]))

#- that are in overlap, but not in the core
#phageloci.update(set(['SALM_'+line.strip().split()[0] for line in open(r'C:\Users\koenro\Documents\Leg_stats\am_eb_combined_to_remove.txt')]))

#- that are labeled phage, but are not in the core
#phageloci.update(set([line.strip().split()[0] for line in open(r'C:\Users\koenro\Documents\Leg_stats\am_eb_combined_phages.txt')]))



#phageloci.update(set(['ECOLI_'+str(k) for k in range(2513+1, 30000)]))
cst = bns.Characters.CharSetType(wgmlstExperType.Name)
print cst.GetCount()
locusIds = set(cst.GetChar(i) for i in xrange(cst.GetCount()) if cst.GetChar(i) not in phageloci)
print len(locusIds)
#locusIds = set(cst.ViewGet('non_phage_loci').GetCharacters())
#locusIds = set(l for l in locusIds if int(l.split('_')[1])<9495)


entryGroups = {}
entryInfo = {}
for entry in bns.Database.Db.Selection:
	#info = entry.Field('SampleId').Content
	info = entry.Key
	entryGroups[info] = entryGroups.get(info, []) + [entry]
	entryInfo[info] = '\t'.join(entry.Field(f).Content for f in bns.Database.Db.Fields)
	
print wgmlstExperType.Name
print seqExperType.Name

sortedGroups = entryGroups.keys()
sortedGroups.sort()
qualData = bns.Characters.CharSet()
Parameters = ("PercentageCDS","PercentageLoci", "BasesInOverlapCDS", "BasesInOverlapLoci", "PercentAnnGenCovered","LocusCount","cdsCount")
ParametersID = [0,0,0,0,0,0,0]
for i in range(len(Parameters)):
	CharID = qualExperType.FindChar(Parameters[i])
	if CharID<0:
		CharID = qualExperType.AddChar(Parameters[i],100)
	ParametersID[i]=CharID

for info in sortedGroups:
	allBaseCount = BaseCounts(0,0, 0, 0, 0, 0, 0)
	parameterVal=()
	
	try:
		for entry in entryGroups[info]:
			print entry.Key
			entry.Selection=0
			annSeq = bns.Sequences.AnnSeq()
			annSeq.Create(entry.Key, seqExperType.Name)
			baseCount = CountBases(annSeq, locusIds)
			allBaseCount.Add(baseCount)
		baseCount = allBaseCount
		parameterVal= (100*baseCount.FractionCds,  100*baseCount.FractionLoci , 100*baseCount.FractionMultiCds, 100*baseCount.FractionMultiLoci, 100*baseCount.FractionLoci / baseCount.FractionCds if baseCount.FractionCds else 100,baseCount.locusCount,baseCount.cdsCount)
		allInfo = entryInfo[info]
		qualData.Load(entry.Key,currSchema.QualityExperTypeID)
		for i in range(len(ParametersID)):
			qualData.SetVal(ParametersID[i],parameterVal[i])
		
		qualData.Save()
		#print '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(info, baseCount.locusCount, baseCount.GenomeLen, baseCount.FractionCds*baseCount.GenomeLen, baseCount.FractionLoci*baseCount.GenomeLen, baseCount.FractionCds,  baseCount.FractionLoci , baseCount.FractionMultiCds, baseCount.FractionMultiLoci, baseCount.FractionLoci / baseCount.FractionCds if baseCount.FractionCds else 100, allInfo)
	except:
		print info
	
	
	
	
