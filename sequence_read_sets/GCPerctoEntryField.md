---
title: "GCPerctoEntryField"
date: 2018-05-29T15:32:00+01:00
draft: false
author: "Margo Dirickx"
scriptlanguage: "py"
---



This script stores the GC% of your srs and srs_trimmed datasets into information fields. By default the srs experiment is 'wgs' but this can be adjusted below.


