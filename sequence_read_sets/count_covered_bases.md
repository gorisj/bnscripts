---
title: "count_covered_bases"
date: 2018-08-07T09:48:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---



Counts the number of bases covered in wgMLST calls.

