---
title: "extract_duration_CE_latest_log"
date: 2019-05-24T14:00:00+01:00
draft: false
author: "Jan Deneweth"
scriptlanguage: "py"
---


Extracts the duration (based on job end time) for a CE job from the latest log for that job and outputs it to an entry info filed in the format HH:MM:SS (HH can be larger than 24). 
