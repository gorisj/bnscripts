import bns
import datetime


Dlg = bns.Windows.XmlDlgBuilder

class dlgCheckFailedJobs(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'dlgCheckFailedJobs')
		
		# a radio list control
		values = ["De novo assembly", "Assembly-based calls", "Assembly-free calls"]
		self.job = values[0]
		self.jobList = Dlg.Radio('job type', values, 30, vertical=True,
		                            default=self.job)
		self.IncludeNew = False
		self.IncludeNewChkbox = Dlg.Check('check', "Include entries for which this job has never been submitted.")
		jobs = [["Type of job:",self.jobList]]
		
		self.inputHours = Dlg.Input('Hours', 8, default = 4, tp='integer')
		hours = [["Exclude jobs submitted in the last", self.inputHours , " hours, to avoid selecting for jobs that are still running:"]]
		
		title = Dlg.StaticText("This script will check all the entries in the current selection for the presence of submitted, but not finished, jobs of the selected type. Only such entries will be selected once the script is finished.",expandHor=False)
		
		# now define the dialog layout
		grid = [[title],
					[jobs],
					[hours],
					[self.IncludeNewChkbox]
				]
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Radio list", simple)
	
	def onOk(self, args):
		"""Get the selected items"""
		self.job = self.jobList.GetValue()
		self.Hours = self.inputHours.GetValue()
		self.IncludeNew = self.IncludeNewChkbox.Checked

dlg = dlgCheckFailedJobs()
if not dlg.Show():
	bns.Stop()




def try_strptime(line):
	format = "%Y-%m-%d %H:%M:%S"
	try:
		s = line.split("\t",1)[0]
		date = datetime.datetime.strptime(s, format)
	except ValueError:
		date = None
	return date

if dlg.job == "De novo assembly":
	jobtype = "Doing a denovo assembly"
if dlg.job == "Assembly-based calls":
	jobtype = "Performing BLAST"
if dlg.job == "Assembly-free calls":
	jobtype = "Finding alleles"
currentTime = datetime.datetime.utcnow()
submitTime = currentTime - datetime.timedelta(hours=int(dlg.Hours))


for entry in bns.Database.Db.Selection:
	attachments = entry.GetAttachmentList()
	
	
	previousDate = datetime.datetime(1,1,1)
	for log in attachments:
		
		if "Log history" in log["Description"]:
			logContent = log["Attach"].Content
			recentJob = ""
			for line in logContent.split("\n"):
				if jobtype in line:
					date = try_strptime(line)
					if date > previousDate:
						previousDate=date
						recentJob = line
			if recentJob == "" and not dlg.IncludeNew:
				entry.Selection=0
			else:
				if "Finished" in recentJob:
					entry.Selection=0
				if previousDate > submitTime:
					entry.Selection=0

	
				

