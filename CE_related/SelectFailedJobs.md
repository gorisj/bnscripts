---
title: "SelectFailedJobs"
date: 2018-09-28T09:46:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


This script will check all the entries in the current selection for the presence of submitted, but not finished, jobs of the selected type. Only such entries will be selected once the script is finished.

