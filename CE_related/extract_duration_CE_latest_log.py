"""
This script extracts the latest analysis duration from Calculation Engine logs.

Requestor: Margo Diricks
Author: Jan Deneweth
Date: 2019-05-20
"""

import bns
from datetime import datetime


# Global variables
INFOFIELD_NAME = "ce_duration"


# Process all selected entries
for entry in bns.Database.Db.Selection:
	
	# Iterate all CE log attachments
	attachments = entry.GetAttachmentList()
	latest = None
	duration_str = None
	for log in attachments:
		if 'detailed log for job with ID' in log['Description']:
			content = log['Attach'].Content
			
			# Get start and end times
			starttime = None
			endtime = None
			for line in content.splitlines()[1:]:
				# The log should start with a header line indicated by the pound symbol ('#')
				# Every other line in the log should consist of three parts, split by tabs.
				# Tabs may occur in the message, so only the first 2 tabs count for splitting.
				try:
					timestamp, msg_type, msg = line.split('\t', 2) # If this gives error, too few tabs => malformed log
				except ValueError:
					continue
				if starttime is None:
					starttime = timestamp
				endtime = timestamp
			if starttime is None:
				# No lines in the log => no duration
				continue
			# Check if it beats the latest entry (based on endtime)
			if latest:
				if not (endtime > latest):
					continue  # Before latest; skip
			latest = endtime
			
			# Calculate and save duration
			starttime = datetime.strptime(starttime, "%Y-%m-%d %H:%M:%S")
			endtime = datetime.strptime(endtime, "%Y-%m-%d %H:%M:%S")
			duration = int((endtime - starttime).total_seconds())
			hours = duration // 3600
			minutes = (duration - (hours * 3600)) // 60
			seconds = duration - (hours * 3600) - (minutes * 60)
			duration_str = "{H:0>2}:{M:0>2}:{S:0>2}".format(H=hours, M=minutes, S=seconds)
			
	# Update the entry infofield
	entry.Field(INFOFIELD_NAME).Content = duration_str or ''


# Save all updated fields
bns.Database.Db.Fields.Save()


#
#
# END OF FILE
