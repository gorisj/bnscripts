---
title: "LoadSamples"
date: 2000-09-18T13:57:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



This script will parse through a file created through DBQ and import all the sample lanes. For each experiment type (VCA, VCB, ...), the first lane in will define all the fields for the isolate. 
The Sample Label serves as the pseudo key. The script actually lets BioNumerics create the key. However, for purposes of linking additional experiments to a database entry the script keys on Sample Label.
The field RiboPrint keeps track of what experiments are attached to a database entry.


