---
title: "InitializeDB"
date: 2003-04-22T13:57:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



This script will do several things...

1. Load all the fields from fieldname[] into the target database. It will only load fields that are not already in the target database.

2. Copy files from A:\Fprint to the BioNumerics target database. These files contain the the experiments, the references, and the calibration curves.

3. Create a database entry for the marker.

In order to add a new field, just add an additional field to the array fieldname[].  
