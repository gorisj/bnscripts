---
title: "LoadCIDPats"
date: 2000-09-18T13:57:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



This script will parse through a file created through DBQ and import all the CIDPats.
The script lets BioNumerics create the key.
The field RiboPrint keeps track of what experiments are attached to a database entry.

