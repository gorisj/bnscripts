// script LoadCIDPats.bns
//
// This script will parse through a file created through DBQ and import all the	CIDPats.
// The script lets BioNumerics create the key.
// The field RiboPrint keeps track of what experiments are attached to a database entry.
//

string basefilename,filename,experfilename,st,st2,expername,key;
FILE fl;
CRFPRINT fpr[];
integer lanenr,lanecount[],pixcount,hitcount[],patsize;
integer riboprint[];
string keytext;
string stseqnr,stgenus,stspecies,stlaneprot,accessionnr;
string stlibname,ststraincomment;
string sampwritten,patternread,sampactive;
DIALOG dlg;
integer entrynr;
string entry;
string thiskey,thisstrainnr,thisRiboPrint,newRiboPrint;
string experiment[]; integer numexperiments,expercounter,expernum;

// load up an array to track different experimental files for each type
// the data file may be a single file with many experiment types, so we
// need to do this

numexperiments = DbGetExperCount;

//prompt the user for an import  file name

filename="";
if not(FilePromptName("Enter DBQ Created text file name",filename,0)) then stop;
if not(FileOpenRead(fl,filename)) then 
   {
   message("Could not read file");
   stop;
   }

// prompt the user for a file name inside the BN database

DlgReset(dlg);
while find(filename,"\",1)>0 do
  filename=substring(filename,find(filename,"\",1)+1,5000);
DlgAddText(dlg,"Enter the file name in the BN database",15,15,200,15);
basefilename = filename;
DlgAddEdit(dlg,basefilename,15,40,150,20);
if not(DlgShow(dlg,"Import Riboprint data",300,200)) then stop;

// scans the file and imports curves & database fields

setbusy("Importing RiboPrints");

for expercounter = 1 to numexperiments do
   {
   experiment[expercounter] = DbGetExperName(expercounter);
   lanecount[expercounter] = 0;
   hitcount[expercounter] = 0;
   }
   
key="";lanenr=0;

patsize = 999;
patternread = "no";sampactive = "no";

while not(FileIsEnd(fl)) do 
   {
   st=FileRead(fl,20000);

   if st="RPDataRec = DIDPAT" then
      {
      pixcount = 0; sampactive = "yes"; patternread = "no";
      }
          
// parse the fields

    while (sampactive = "yes") and (patternread = "no") do
      {
      st = FileRead(fl,20000);
      if substring(st,1,16) = "DIDNumIDLName = "  then
         stlibname = substring(st,17,1000);         
      if substring(st,1,17) = "DIDNumIDSeqNum = " then
         stseqnr = substring(st,18,1000);
      if substring(st,1,14) = "DIDLblGenus = "   then
         stgenus = substring(st,15,1000);
      if substring(st,1,16) = "DIDLblSpecies = "   then
         stspecies = substring(st,17,1000);
      if substring(st,1,13) = "DIDComment = " then
         ststraincomment = substring(st,14,1000);
      if substring(st,1,11) = "DIDProto = " then
         stlaneprot = substring(st,12,1000);
      if substring(st,1,10)="PatSize = " then
         {
         patsize = val(substring(st,11,1000));
         }
      if substring(st,1,7)="Pattern" then 
         {
         st=substring(st,13,5000);
         while length(st)>0 do 
            {
            pixcount = pixcount + 1;         
            st2 = splitstring(st," ");
            riboprint[pixcount] = val(st2);
            }
         }
      if (pixcount = patsize) then
         patternread = "yes";
      }

// at the end of the sample the pattern will have been read.  Then we need to write
// the lane to the experiment file for the correct protocol

   if (sampactive = "yes") and (patternread = "yes") then
      {
      
//    find the experiment index

      expername = stlaneprot;
      expernum = 1;
      while (expername <> experiment[expernum]) do 
         {
         expernum = expernum + 1;
         if expernum > numexperiments then
            {
            message("Unrecognized Fingerprint Type " + expername);
            stop;
            }
         }
      patternread = "no";
      sampactive = "no";
      lanecount[expernum] = lanecount[expernum] + 1;

      key = DbAddEntry("");
         
// open a new experiment file for the current protocol
                  
      if (lanecount[expernum] = 1) then
         {
         experfilename = basefilename + expername + str(hitcount[expernum],0,0);
         CrFprCreate(fpr[expernum],experfilename,expername);
         }
         
      lanenr = CrFprAddLane(fpr[expernum],expername,key);
      accessionnr = stlibname + "-" + stseqnr;
      DbSaveFields;

//  save the fields from the first lane defining an experiment

      DbSetField(key,"Accession Number",accessionnr);
      DbSetField(key,"Strain Number",accessionnr);
      DbSetField(key,"Comment",ststraincomment);
      DbSetField(key,"Genus",stgenus);
      DbSetField(key,"Species",stspecies);
      DbSaveFields;

      for pixcount = 1 to patsize do
         CrFprAddCurveVal(fpr[expernum],lanenr,riboprint[pixcount]);     

// when the current experiment file fills up with 100 entries, close it and save it.
// BioNumerics can only have 200 entries per experiment file

      if (lanecount[expernum] = 100) then
         {
         CrFprSave(fpr[expernum]);
         lanecount[expernum] = 0;
         hitcount[expernum] = hitcount[expernum] + 1;
         }
      }
   }

for expercounter = 1 to numexperiments do
   if (lanecount[expercounter] > 0) then    
      CrFprSave(fpr[expercounter]);

//close everything

FileClose(fl);
setbusy("");

for expercounter = 1 to numexperiments do
   if hitcount[expercounter]*100 + lanecount[expercounter] > 0 then
      message("Import complete.~n~n" + str(hitcount[expercounter]*100 + lanecount[expercounter],0,0) +
              " fingerprints of type " + experiment[expercounter] + " have been inported.");
