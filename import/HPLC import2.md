---
title: "HPLC import2"
date: 2008-03-03T12:55:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



Import of HPLC Chromatogram into BioNumerics.

This version of the scripts splits the HPLC export file based on dots ".".

Further information or workflow is unknown. 

