---
title: "read_bruker_text_files"
date: 2011-03-29T14:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
attachment: "D19_0_D19_2.txt"
attachment: "workflow_bruker.doc"
---


Imports Bruker text file(s) with MALDI data.

Attachments:
* "D19_0_D19_2.txt": example data
* "workflow_bruker.doc": document describing the workflow using the script to import the MALDI data.


