//*************************************************************
// Sample script provided "as is" by Applied Maths.
// You are free to use and modify this script for your
// own purposes.
// Please notice that improper use of script commands may
// corrupt your database. Running this script is entirely at
// your own responsibility. Applied Maths accepts no
// liability for any damage that results from using
// this script.
//*************************************************************

//$MENU window=main;popup=Agilent;insertpopupafter=Scripts;name=Import Agilent BioAnalyzer xml file(s)

String filename,fname, error, namestr,lanestring,st_mw,st_h,dummy,duplicateKey;
string gelname,expername,experlist,str_resol,str_width,st,refsysdef,position;
integer i,j,lanenr,ok,resol,retval,nr,OD,useSName,CreateDup,Dup;
float mw,h,width,value,maxVal,minTime;
XMLnode doc,chipset,chips,chip,files,filen,samples,sample[],name,node1,node2,node3,node4,weight,height,runtime,peak[];
DIALOG dlg1,dlg2;
CRFPRINT fpr;
FPRINT fp;
Collection ap;

filename="*.xml";
if not(FilePromptName("Select a BioAnalyzer XML file you want to import",filename,0)) then stop;
useSName=1;

for i=1 to DbGetExperCount do if DbGetExperClass(i)="FPR" then
   experlist=experlist+DbGetExperName(i)+"	";
experList = experlist+"<Create new experiment>";
expername = experlist;
expername = splitString(expername,"	");
str_resol="1000";
str_width="5";
gelname=filename;
while find(gelname,"\",1)>0 do splitstring(gelname,"\");
if find(gelname,"_",1) then gelname=splitstring(gelname,"_");

// get parameters
DlgAddText(dlg2,"Select the experiment",20,20,180,15);
DlgAddList(dlg2,experlist,expername,20,40,150,200,"LIST");
DlgAddText(dlg2,"Gel file name in BioNumerics",200,20,150,15);
DlgAddEdit(dlg2,gelname,200,40,150,22);
DlgAddText(dlg2,"Curve resolution",200,70,150,15);
DlgAddEdit(dlg2,str_resol,200,90,70,22);
DlgAddText(dlg2,"Band width",200,120,150,15);
DlgAddEdit(dlg2,str_width,200,140,70,22);
//DlgAddRadioButton(dlg2,"Use sample name as KEY",useSName,20,250,90,22);
DlgAddCheck(dlg2,"Use BioAnalyzer samplename* as KEY",useSName,20,250,250,22);
DlgAddText(dlg2,"* Unique sample names are needed!",20,275,250,22);
DlgAddCheck(dlg2,"Allow creation of duplicate entries",CreateDup,20,300,250,22);

ok=0;
while not(ok) do {
   if not(DlgShow(dlg2,"Import BioAnalyzer XML file",370,390)) then stop;
   ok=1;
   if expername="" then {
      ok=0;
      message("You should select an experiment!");
   }
}
resol=val(str_resol);
width=val(str_width);

// open XML file
doc=XMLNodeReadFile(filename,error);
if error<>"" then {
   message("Unable to read "+filename+": "+error);
   stop; 
}

// get sample nodes
chipset=XMLNodeGetChild(doc,"Chipset");
chips=XMLNodeGetChild(chipset,"Chips");
chip=XMLNodeGetChild(chips,"Chip");
files=XMLNodeGetChild(chip,"Files");
filen=XMLNodeGetChild(files,"File");
samples=XMLNodeGetChild(filen,"Samples");

// create new experiment type and reference system if necessary
if expername = "<Create new experiment>" then {
   expername="";
   DlgAddText(dlg1,"Enter an experiment name",20,20,150,15);
   DlgAddEdit(dlg1,expername,20,40,200,18);
   if not(DlgShow(dlg1,"Create Fingerprint Type",250,200)) then stop;
   DbCreateExperType(expername,"FPR");
   sample[1]=XMLNodeGetChildByNr(samples,1);
   node1=XMLNodeGetChild(sample[1],"DAResultStructures");
   node2=XMLNodeGetChild(node1,"DARStandardCurve");
   node3=XMLNodeGetChild(node2,"Channel");
   node4=XMLNodeGetChild(node3,"LadderPeaks");
   refsysdef = "";
   for i=1 to XMLNodeGetChildCount(node4) do {
      peak[i]=XMLNodeGetChildByNr(node4,i);
      weight=XMLNodeGetChild(peak[i],"FragmentSize");
      runtime=XMLNodeGetChild(peak[i],"MigrationTime");
      if i = 1 then minTime = val(XMLNodeGetText(runtime));
      st_mw = XMLNodeGetText(weight);
      position = str(100 * minTime / val(XMLNodeGetText(runtime)),2,2);
      refsysdef = st_mw + ":" + position + "	" + refsysdef;
   }
   FprAddRefsys(expername,"R01",refsysdef);
   FprSetCalibPoints(expername,"R01",refsysdef);
}

// and read in all samples and peaks 
setbusy("Reading XML file...");
CrFprCreate(fpr,gelname,expername);           
for i=1 to XMLNodeGetChildCount(samples) do {
  sample[i]=XMLNodeGetChildByNr(samples,i);
  name=XMLNodeGetChild(sample[i],"Name");
  node1=XMLNodeGetChild(sample[i],"DAResultStructures");
  node2=XMLNodeGetChild(node1,"DARIntegrator");
  node3=XMLNodeGetChild(node2,"Channel");
  node4=XMLNodeGetChild(node3,"PeaksMolecular");
  namestr=XMLNodeGetText(name);
  lanestring=namestr;
  if (lanestring<>"") then {
      if (lanestring<>"Ladder") then {
         if useSName=1 then {
            if not(DbIsKeyPresent(lanestring)) then {
              DbAddEntry(lanestring);
              Set(ap, lanestring, 1);
            }
            else if (FindName(ap, lanestring)<>0 or FprLoadNorm(fp,lanestring,expername)) then {
               if CreateDup=1 then {
                  Dup=1;
                  duplicateKey=lanestring;
                  while DbIsKeyPresent(duplicateKey) do {
                     duplicateKey = lanestring + "\#" + str(Dup);
                     Dup = Dup+1;
                  }
                  lanestring = duplicateKey;
                  DbAddEntry(lanestring);
                  Set(ap, lanestring, 1);
               } 
               if CreateDup=0 then lanestring="";
            }
         }
         if useSName=0 then { 
            dummy="";
            lanestring = DbAddEntry(dummy);
         }
         lanenr=CrFprAddLane(fpr,expername,lanestring);
      }
      else lanenr=CrFprAddLane(fpr,expername,"");
  }
  for j=1 to XMLNodeGetChildCount(node4) do {
     peak[j]=XMLNodeGetChildByNr(node4,j);
     weight=XMLNodeGetChild(peak[j],"FragmentSize");
     height=XMLNodeGetChild(peak[j],"Height");
     st_mw=XMLNodeGetText(weight);
     mw=val(st_mw);
     st_h=XMLNodeGetText(height);
     h=val(st_h);
     CrFprAddBand(fpr,lanenr,mw,h,width);
  }
}
setbusy("");
CrFprBandsToCurve(fpr,resol);
CrFprSave(fpr);   


