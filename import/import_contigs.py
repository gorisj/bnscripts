import bns
import string
import os

path=r'C:\Users\Katleen\Documents\CHRO\Campylobacter\contigs'
exper = r'WGS'
fieldid = r'BIGSDB_ID'
infofield = r'CONTIG_INFO'
addEntries=False

def ReadFromFasta(path):
	fl = open(path, 'r')
	seq = string.join((line.strip() for line in fl.readlines() if len(line)>0 and line[0]!='>'), '|')
	fl.close()
	return seq
	
bigsDbIds = []
id_to_entry = {}
for entry in bns.Database.Db.Entries:
	content = bns.Database.EntryField(entry, fieldid).Content
	bigsDbIds.append(content)
	id_to_entry[content]=entry.Key
	
for fl in os.listdir(path):
	key = fl.split('.')[0]
	
	try:
		entry=''
		if addEntries and not key in id_to_entry:
			entry = bns.Database.Db.Entries.Add().Key
			bns.Database.EntryField(entry, fieldid).Content=key
			id_to_entry[key]=entry
				
		if key in id_to_entry:
			entry=id_to_entry[key]
			seqstr=ReadFromFasta(os.path.join(path, fl)	)
			seq=bns.Sequences.Sequence()
			if len(bns.Database.Experiment(entry, exper).ExperID):
				seq.Load(entry, exper)
			else:
				seq.Create(exper, "", entry)
			
			seq.Set(seqstr)
			seq.Save()
			
	except RuntimeError as e:
		if __bnsdebug__:
			print e
		bns.Database.EntryField(entry, infofield).Content="Not imported" 

bns.Database.Db.Fields.Save()		
