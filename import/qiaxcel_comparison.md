---
title: "qiaxcel_comparison"
date: 2011-03-08T14:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
attachment: "qiaxcel_import.BNS"
attachment: "qiaxcel_normalization.BNS"
attachment: "QIAxcel import in BioNumerics.docx"
---


Shows imported diversilab entries in a comparison. 
Calculates an UPGMA dendrogram.

Attachments: 
* "qiaxcel_import.BNS"
* "qiaxcel_normalization.BNS"
* "QIAxcel import in BioNumerics.docx": describes the import, normalization and comparison workflow


