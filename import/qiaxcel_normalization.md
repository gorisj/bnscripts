---
title: "qiaxcel_normalization"
date: 2011-03-08T14:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
attachment: "qiaxcel_import.BNS"
attachment: "qiaxcel_comparison.BNS"
attachment: "QIAxcel import in BioNumerics.docx"
---


Normalizes QIAxcel pseudo-gel files
- finds first and last (high) peak
- performs linear normalization
- looks for bands

Attachments: 
* "qiaxcel_import.BNS"
* "qiaxcel_comparison.BNS"
* "QIAxcel import in BioNumerics.docx": describes the import, normalization and comparison workflow


