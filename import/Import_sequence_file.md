---
title: "Import_sequence_file"
date: 2008-02-15T13:53:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



Imports a sequence file to a sequence experiment in BioNumerics.

The sequence file can be either in EMBL, Genbank or fasta format.

The sequence experiment type should be created in the database prior to running the script. 

