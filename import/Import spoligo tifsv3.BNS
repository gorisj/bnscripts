//$MENU window=main;popup=Spoligo typing;insertpopupafter=Scripts;name=Import Spoligo TIFF files...;insertattop;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

DIALOG dlg,chdlg,dlg1;
CHARSET cs;
integer i,j,numrow,numcol,maxval,minval,bin,manual,x,y,ok;
string bnima,data[],charsetname,charsetlist,cfname,key[];
string vals[][],cutoff;
string stnumcol,stnumrow,st,path,spolfile,spolfield;
float value,co;
file fl;

// import file info
string flname,separator,sepchar;
string flcols[],flcontent[][],flcolkey,flcolexp;
integer flrowcnt,flcolcnt,flcolkeynr,flcolexpnr;
INDEX flcolidx;

spolfield="Spoligo file";
function integer readimportfile() {
   integer flcolnr,flrownr;
   string flline,flcol;
   FILE fl;
// read import file header
   if not(FileOpenRead(fl,flname)) then {
      MessageBox("Error","Unable to open file '"+flname+"'","exclamation");
      return(0);
   }
   if FileIsEnd(fl) then {
      MessageBox("Error","File '"+flname+"' is empty","exclamation");
      return(0);
   }
   flline=FileRead(fl,99999);
   if flline="" then {
      MessageBox("Error","First line of import file '"+flname+"' should contain header information","exclamation");
      return(0);
   }
   flcolcnt=0;
   IdxReset(flcolidx);
   while flline<>"" do {
      flcolcnt=flcolcnt+1;
      flcol=splitstring(flline,sepchar);
      if substring(flcol,1,1)="~"" and substring(flcol,length(flcol),length(flcol))="~"" then flcol=substring(flcol,2,length(flcol)-1);
      flcols[flcolcnt]=flcol;
      if IdxGet(flcolidx,flcol)>0 then {
         MessageBox("Error","Multiple occurence of column name '"+flcol+"' in import file","exclamation");
         return(0);
      } else {
         IdxSet(flcolidx,flcol,flcolcnt);
      }
   }
// read the rest of the import file
   flrowcnt=0;
   while not(FileIsEnd(fl)) do {
      flline=FileRead(fl,99999);
      flrowcnt=flrowcnt+1;
      if flline<>"" then {
         flcolnr=0;
         while flline<>"" do {
            flcolnr=flcolnr+1;
            flcontent[flrowcnt][flcolnr]=splitstring(flline,sepchar);
         }
      }
   }
   FileClose(fl);
   flrowcnt=flrowcnt-1;
// check with FPR file
   if flrowcnt<>numrow then {
      MessageBox("Error","The number of entries in the spoligo grid (# rows = "+str(numrow,1,0)+") is not equal to the number of entries in the link file ("+str(flrowcnt,1,0)+")","exclamation");
      return(0);
   }
   return(1);
}


function integer getimportfile(string *spolfile) {
   integer xp,yp,xs,ys,ctrl_fl,ctrl_bt,ctrl_ok,ctrl_cc;
   string separatorlist;
   DIALOG dlg;
   
   function integer callbackimportfile(DIALOG *dlg, integer id, string *action, string *comment) {
      string flcheck;
      if action="command" and id=ctrl_bt then {
         FilePromptName("Select spoligo entry link file:",flname,0);
         DlgReloadControl(dlg,ctrl_fl);
      }
      if action="cancel" or (action="command" and id=ctrl_cc) then DlgReturn(dlg,0);
      if action="ok"     or (action="command" and id=ctrl_ok) then {
         FileGetList(flname,flcheck);
         if flcheck="" then {
            MessageBox("Error","File '"+flname+"' does not exist. Please enter a valid file name","exclamation");
         } else {
            DlgReturn(dlg,1);
         }
      }
   }
   
// prepare dialog controls
   separatorlist="TAB	Comma	Semicolon";
   if separator="" then separator="TAB";
   
   xp=20;
   yp=20;
   xs=250;
   
// create dialog
   DlgReset(dlg);
   
   DlgAddText(dlg,"File name:",xp,yp,xs,18);
   yp=yp+20;
   ctrl_fl=DlgAddEdit(dlg,flname,xp,yp,xs,18);
   yp=yp+25;
   DlgAddText(dlg,"Separator:",xp,yp+3,60,18);
   DlgAddList(dlg,separatorlist,separator,xp+70,yp,60,20,"drop");
   ctrl_bt=DlgAddButton(dlg,"Browse...",20,xp+xs-80,yp-1,80,25);
   yp=yp+50;
   DlgAddText(dlg,"Enter Spoligo filename",xp,yp,150,18);
   ctrl_ok=DlgAddButton(dlg,"OK",10,xp+xs-80,yp,80,25);
   yp=yp+25;
   DlgAddEdit(dlg,spolfile,xp,yp,150,18);
   yp=yp+15;
   ctrl_cc=DlgAddButton(dlg,"Cancel",99,xp+xs-80,yp,80,25);
   
   DlgRegisterCallback(dlg,"callbackimportfile");
   if not(DlgShow(dlg,"Import spoligo entry link information",xp+xs+20,yp+65)) then return(0);
   
// finalize
   if separator="TAB"       then sepchar="	";
   if separator="Comma"     then sepchar=",";
   if separator="Semicolon" then sepchar=";";
   
   return(1);
}



DlgAddText(dlg,"This script will import spoligo bitmaps using BNIMA. Do you want to continue?",15,15,180,50);
if not(DlgShow(dlg,"Spoligo Bitmaps (import tool)",300,120)) then stop;

//Analysis in BNIMA
manual=0;
DlgReset(dlg);
DlgAddText(dlg,"STEP1: 'File>Load Image' and 'Edit>Settings' check 'Color Scale'",15,15,400,15);
DlgAddText(dlg,"and 'Grid>Add New', Place the resized grid correctly over the image... ",52,30,400,15);
DlgAddText(dlg,"STEP2: Select all (draw rectangle around grid)",15,55,350,15);
DlgAddText(dlg,"'Cells>Add disk to mask' and choose an appropriate disk size...",52,70,350,15);
DlgAddText(dlg,"STEP3: 'Quantification>Add cells to character set'",15,95,350,15);
DlgAddText(dlg,"'Quantification>Export to clipboard'",52,110,350,15);
DlgAddText(dlg,"!!! Only close BNIMA after 'Export to clipboard' !!!",65,140,300,15);
DlgAddCheck(dlg,"Open short BNIMA manual in Notepad (printable)?",manual,15,180,300,15);
if not(DlgShow(dlg,"How to use BNIMA (opens after clicking <OK>)",500,250)) then stop;

/// BNIMA SHORT MANUAL => notepad
path=DbGetStartupDir;
if manual=1 then {
FileOpenWrite(fl,path+"\bnima.txt");
FileWrite(fl,"Quick guide to BioNumerics Image Analysis (BNIMA)");
FileWriteLine(fl);
FileWriteLine(fl);
FileWriteLine(fl);
FileWrite(fl,"STEP1: 'File>Load Image' and 'Edit>Settings' =>Check 'Color Scale' then proceed with 'Grid>Add New'. Specify the number of rows and columns (default: 45 and 43 respectively). Place the resized grid correctly over the image so that the green stars are in the middle of each spot! Move to the second step. "); 
FileWriteLine(fl);FileWriteLine(fl);
FileWrite(fl,"STEP2: Select all (draw(=drag) a rectangle around grid) then proceed with Cells>Add disk to mask' and choose an appropriate radius, each disk should fit perfectly on the spot. (If the radius is set too high, select 'File>Remove disk' and change the size of the disk). Move to the third step.");
FileWriteLine(fl);FileWriteLine(fl);
FileWrite(fl,"STEP3: 'Quantification>Add cells to character set' then end with 'Quantification>Export to clipboard' ");
FileWriteLine(fl);FileWriteLine(fl);
FileWriteLine(fl);
FileWrite(fl,"Copyright Applied Maths NV 2009");
FileClose(fl);
execute("notepad "+path+"\bnima.txt");
}

//proceed with BNIMA

execute(DbGetStartupDir+"\filters\bnima.exe");
DlgReset(dlg);
DlgAddText(dlg,"Click <OK> to continue import...",15,15,180,50);
if not(DlgShow(dlg,"Import and link to entries in BioNumerics",300,120)) then stop;

//Paste in BN

PasteClipBoard(bnima);

ok=getimportfile(spolfile);  if not(ok) then stop;


//request paramaters 
charsetlist="";charsetname="";
stnumrow="45";stnumcol="43";bin=1;cutoff="20";
for i=1 to DbGetExperCount do
   if DbGetExperClass(i)="CHR" then {
      charsetlist=charsetlist+DbGetExperName(i)+"	";
      }
      charsetlist = charsetlist+"<Create new experiment>";
      charsetname = charsetlist;
      charsetname = splitString(charsetname,"	");
      if length(charsetname)<=0 then charsetname=DbGetExperName(i);
DlgAddText(chdlg,"Select Experiment",15,15,100,20);   
DlgAddList(chdlg,charsetlist,charsetname,15,40,150,200,"LIST");
//DlgAddText(chdlg,"Enter the spoligo filename",200,15,150,20);
//DlgAddEdit(chdlg,cfname,200,40,150,20);
DlgAddText(chdlg,"Number of rows (Isolates):",200,15,150,15);
DlgAddEdit(chdlg,stnumrow,200,40,150,20);
DlgAddText(chdlg,"Number of colums:",200,70,150,15);
DlgAddEdit(chdlg,stnumcol,200,90,150,20);
DlgAddCheck(chdlg,"Convert to binary?",bin,200,115,150,20);
DlgAddText(chdlg,"Present above (%):",210,145,100,15);
DlgAddEdit(chdlg,cutoff,315,145,50,20);

if not(DlgShow(chdlg,"Specify Parameters",400,300)) then stop;
if length(charsetname)<=0 then stop;

co=val(cutoff);
numrow=val(stnumrow);
numcol=val(stnumcol);
maxval=1; minval=100;



ok=readimportfile; if not(ok) then stop;

setbusy("Importing data... Please wait...");
x=1; y=1;
for x=1 to numrow do {
for y=1 to numcol do {
vals[x][y]=splitstring(bnima,"~n");
if val(vals[x][y]) > maxval then maxval = val(vals[x][y]);
if val(vals[x][y]) < minval then minval = val(vals[x][y]);
y=y+1;
}
x=x+1;
}

//create new character type if needed
if charsetname = "<Create new experiment>" then {
   charsetname="";
   DlgAddText(dlg1,"Enter a character set name",20,20,150,15);
   DlgAddEdit(dlg1,charsetname,20,40,200,18);
   if not(DlgShow(dlg1,"Create Character Type",250,200)) then stop;
   DbCreateExperType(charsetname,"CHR");
   for i=1 to numcol do {
      if bin=0 then ChrSetAddChar(charsetname,"Sp"+str(i,1,0),100);
      if bin=1 then ChrSetAddChar(charsetname,"Sp"+str(i,1,0),1);
   //ChrSetRange(charsetname,i,minval,maxval);
   }
} 

//adding data and entries
//rescale to range 0 and 100
float newvals[][];
for x=1 to numrow do {
   for y=1 to numcol do {
      newvals[x][y]=(val(vals[x][y])-minval)/(maxval-minval)*100;
   y=y+1;
   }
x=x+1;
}


for x=1 to numrow do {
key[x]= flcontent[x][2];
//key[x]="Isolate_"+str(x,1,0);
if key[x]<>"" then {
if not(DbIsKeyPresent(key[x])) then DbAddEntry(key[x]);
if spolfile<>"" then DbSetField(key[x],spolfield,spolfile);
if DbGetEntryLink(key[x],charsetname)="" then {
   ChrCreate(cs,charsetname,cfname,key[x]);
   }
   else {
   ChrLoad(cs,key[x],charsetname);
   }
  for y=1 to numcol do {
   if bin=0 then ChrSetVal(cs,y,newvals[x][y]);
   if bin=1 then {
      value=1;
      if newvals[x][y] <= co then value=0;
      ChrSetVal(cs,y,value);
   }
  
  y=y+1;  }
ChrSave(cs);
}
x=x+1;
}
DbSaveFields;

setbusy("");






