---
title: "Import_matrix_data"
date: 2010-07-30T10:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



Imports matrix data files to a matrix data experiment in BioNumerics.

The matrix data experiment type should be created in the database prior to running the script. 

