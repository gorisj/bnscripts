---
title: "qiaxcel_import"
date: 2011-03-08T14:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
attachment: "qiaxcel_comparison.BNS"
attachment: "qiaxcel_normalization.BNS"
attachment: "QIAxcel import in BioNumerics.docx"
---


Imports txt file from QIAxcel output from QIAGEN

Attachments: 
* "qiaxcel_comparison.BNS"
* "qiaxcel_normalization.BNS"
* "QIAxcel import in BioNumerics.docx": describes the import, normalization and comparison workflow


