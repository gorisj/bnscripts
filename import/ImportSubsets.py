"""ImportSubsets.py

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""


import bns
import os
flName = os.path.join(bns.Database.Db.Info.HomeDir, 'subsets.txt')
fl = open(flName)
for l in fl:
	line = l.rstrip()
	if line:
		parts = line.split('\t')
		subsetName = parts.pop(0)
		subset = bns.Database.Db.Subsets[subsetName]
		if subset is not None:
			bns.Util.Warnings.Add("Subset '{0}' already exists.".format(subset))
			continue
		
		subset = bns.Database.Db.Subsets.Create(subsetName)
		entries = [k for k in parts if bns.Database.Db.Entries.IsPresent(k)]
		if len(entries) < len(parts):
			bns.Util.Warnings.Add("Some entries for subset '{0}' have been removed. Adding remaining entries.".format(subset))
		subset.Add(entries)
fl.close()

