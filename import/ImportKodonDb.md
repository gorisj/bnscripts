---
title: "ImportKodonDb"
date: 2015-09-10T14:12:00+01:00
draft: false
author: "Dolf Michielsen"
scriptlanguage: "py"
---



Imports a Kodon database into a BioNumerics database.

The csript variable 'kodonDbPath' contains the path to the folder containing the Kodon database and should be modified accordingly before running the script. 

