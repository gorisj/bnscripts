---
title: "import_kodon_contigs"
date: 2014-03-17T09:42:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



Imports Kodon contigs files to sequence experiment in BioNumerics.

The sequence experiment type should be created in the database prior to running the script. 

