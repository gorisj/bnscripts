---
title: "read_applied_text_files"
date: 2011-03-29T14:13:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
attachment: "123921 exported spectrum.txt"
attachment: "workflowAB.doc"
---


Imports Applied Biosystems text file(s) with MALDI data.

Attachments:
* "123921 exported spectrum.txt": example data
* "workflowAB.doc": decsribed the workflow using the scripts to import the AB MALDI data. 


