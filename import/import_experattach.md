---
title: "import_experattach"
date: 2010-08-06T12:33:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



Imports the ExperAttach database table from one connected database to another via an ODBC connection

The ExperAttach table from the source database is read, then the Experiment attachment is entered in the target database using the regular script function DbExAttachSet(key, exper, name, content)
This requires the key/exper combinations to be present in the target database!
