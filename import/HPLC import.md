---
title: "HPLC import"
date: 2008-03-03T11:05:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



Import of HPLC Chromatogram into BioNumerics.

This version of the scripts splits the HPLC export file based on tabs.

Further information or workflow is unknown. 

