---
title: "import_band_table"
date: 2009-04-06T14:04:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---



Imports band table data from tab delimited file.
A fingerprint type for the data should be created before using the script.

