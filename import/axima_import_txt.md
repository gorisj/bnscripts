---
title: "axima_import_txt"
date: 2010-01-14T09:02:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
attachment: "Anto_ASCII.txt"
attachment: "Axima.docx"
---



This script imports data from Shimadzu Axima MALDI hardware txt files. One lane per file.

Attachments:
* "Anto_ASCII.txt": example data
* "Axima.docx": document decsribing the workflow of how to use the script for import of the MALDI data. 


