---
title: "agilent_import_csv"
date: 2011-01-25T09:02:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---


This script imports data from Agilent BioAnalyzer 2100 software CSV files. One lane per file.


