"""
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""

import os
import bns
import xml.etree.cElementTree as ET

Dlg = bns.Windows.XmlDlgBuilder

class B64D(object):
	"""Custom base64 encoding
	"""
	charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789%*'
	
	def Encode(self, bs):
		binsize = len(bs)
		b64d = ['B64D{0}'.format(str(binsize).zfill(10))]
		for ps in xrange(0, binsize, 3):
			iv0 = ord(bs[ps])
			iv1 = ord(bs[ps+1]) if (ps + 1) < binsize else 0
			iv2 = ord(bs[ps+2]) if (ps + 2) < binsize else 0
			
			ov3 = iv0 + 256*(iv1 + 256*iv2)
			ov3, ov0 = divmod(ov3, 64)
			ov3, ov1 = divmod(ov3, 64)
			ov3, ov2 = divmod(ov3, 64)
			b64d.append(self.charset[ov0] + self.charset[ov1] + self.charset[ov2] + self.charset[ov3])
		return ''.join(b64d)

	def Decode(self, s):
		if (s[:4] != 'B64D'):
			raise RuntimeError("Invalid B64D string.")
		try:
			binsize = int(s[4:14])
		except ValueError:
			raise RuntimeError("Invalid B64D string.")
		
		chartransform = dict((c, i) for i, c in enumerate(self.charset))
		s = s[14:]
		textsize = len(s)
		if textsize%4:
			raise RuntimeError("Invalid padding for B64D string.")
		
		bs = []
		ps2 = 0
		for ps in xrange(0, textsize, 4):
			iv0 = chartransform[s[ps]]
			iv1 = chartransform[s[ps+1]]
			iv2 = chartransform[s[ps+2]]
			iv3 = chartransform[s[ps+3]]
			
			ov2 = iv0 + 64*(iv1 + 64*(iv2 + 64*iv3))
			ov2, ov0 = divmod(ov2, 256)
			ov2, ov1 = divmod(ov2, 256)
			
			bs.append(chr(ov0)); ps2 += 1;
			if (ps2 == binsize):
				break
			bs.append(chr(ov1)); ps2 += 1;
			if (ps2 == binsize):
				break
			bs.append(chr(ov2)); ps2 += 1
		return ''.join(bs)
	
	def EncodeFile(self, bfn):
		with open(bfn, 'rb') as bfp:
			return self.Encode(bfp.read())


class ImportKodonDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'ImportKodonDlg', remembersettings='global')
		
		self.seqExper = None
		self.directory = None
		self.database = None
		self.importAssembly = None
		
		seqExperList = []
		for e in bns.Database.Db.ExperimentTypes:
			if e.Class == 'SEQ':
				if self.seqExper is None:
					self.seqExper = e.ID
				baseType = "nucleotides" if '<Nucl>1</Nucl>' in e.Settings else "amino acids"
				seqExperList.append(('{0} ({1})'.format(e.DispName, baseType), e.ID))
		if not seqExperList:
			raise RuntimeError("Create a sequence type experiment first.")
		items = [Dlg.ListItem(v, i) for v, i in seqExperList]
		values = Dlg.ListValues(items)
		self.seqExperCtrl = Dlg.Drop('seqExper', values, 10, 40, canEdit=False)
		
		# the file controls
		exts = [Dlg.Extension("dbs", "DBS file")]
		self.directoryCtrl = Dlg.File('directory', [], 40, "Home directory:", isdir=True, OnChange=self.changeDir)
		self.databaseCtrl = Dlg.SimpleList('database', [], 10, 40, multi=False)
		self.validate = [Dlg.Validation((Dlg.ValueOf(self.databaseCtrl)!=''),"Please select a database.")]
		
		self.importAssemblyCtrl = Dlg.Check('importAssembly', "Import sequence assemblies", default='1')
		
		# now define the dialog layout
		grid = [["Home directory:", self.directoryCtrl],
		       	 ["Database:", self.databaseCtrl],
					 ["Sequence type:", self.seqExperCtrl],
					 ["", self.importAssemblyCtrl],
					]
		simple = Dlg.SimpleDialog(grid, onStart=self.onStart, onOk=self.onOk, validations=self.validate)
		self.AddSimpleDialog("Import Kodon database", simple)
		
	def onStart(self, args):
		if not self.seqExperCtrl.GetValue():
			self.seqExperCtrl.SetValue(self.seqExper)
		self.changeDir(args)
			
	def changeDir(self, args):
		self.database = self.databaseCtrl.GetValue()
		databases = self.getDatabases(self.directoryCtrl.GetValue())
		self.databaseCtrl.SetValue('')
		self.databaseCtrl.SetItems([Dlg.ListItem(db) for db in databases])
		if self.database in databases:
			self.databaseCtrl.SetValue(self.database)
	
	def getDatabases(self, directory):
		databases = []
		if directory:
			nameexts = (os.path.splitext(f) for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f)))
			databases.extend(n for n, e in nameexts if e.lower() == '.dbs' and os.path.isdir(os.path.join(directory, n)))
		return databases
	
	def onOk(self, args):
		self.database = self.databaseCtrl.GetValue()
		self.directory = self.directoryCtrl.GetValue()
		self.seqExper = self.seqExperCtrl.GetValue()
		self.importAssembly = self.importAssemblyCtrl.Checked

seqImportExper = 'seq'
kodonDbPath = r'T:\Dolf\van Stijn\Kodon HomeDir\KDassemblies_GenBank'
#kodonDbPath = r'T:\Dolf\van Stijn\Kodon HomeDir\KDassemblies_default'

dlg = ImportKodonDlg()
if not dlg.Show():
	bns.Stop()

seqImportExper = dlg.seqExper
kodonDbPath = os.path.join(dlg.directory, dlg.database)
importAssembly = dlg.importAssembly
# create information fields
fldDict = {}
seqSettingsPath = os.path.join(kodonDbPath, 'settings.dbs')
try:
	with open(seqSettingsPath) as fl:
		for line in fl:
			# skip everything before [DBFIELDS]
			if line.rstrip().upper() == '[DBFIELDS]':
				break
		for line in fl:
			# stop when reaching [ENDFIELDS]
			if line.rstrip().upper() == '[ENDFIELDS]':
				break
			fldDef = line.split('\t')
			if len(fldDef) == 3 and fldDef[0]:
				field = fldDef[0]
				fldDict[fldDef[2]] = field
				if bns.Database.Db.Fields[field] is None:
					bns.Database.Db.Fields.Add(field)		
except IOError:
	raise RuntimeError("Could not read the KODON database settings file.")

# make sure that default fields exist
headerFields = ('Accession', 'Identification', 'Organism', 'Description', 'Keyword')
for field in headerFields:
	if bns.Database.Db.Fields[field] is None:
		bns.Database.Db.Fields.Add(field)			

seqImportFiles = []
for fn in os.listdir(kodonDbPath):
	name, ext = os.path.splitext(fn)
	if ext.lower() == '.seq':
		seqImportFiles.append(name)
if seqImportFiles:
	bns.Util.Program.MessageBox("Information", "There are {0} sequences to import.".format(len(seqImportFiles)), 'information')
else:
	raise RuntimeError("There are no sequence files to import.")

# read sequence files
bns.Database.Db.Selection.Clear()
b64d = B64D()
for seq in seqImportFiles:
	bns.SetBusy("Importing sequence '{0}'".format(seq))
	
	with open(os.path.join(kodonDbPath, seq + '.seq')) as fl:
		header = fl.readline().rstrip()
		if not header.startswith('#'):
			raise RuntimeError("Error in sequence file format.")
		fieldContentDict = dict(zip(headerFields, header[1:].split('\t')))
		
		fields = fl.readline().rstrip()
		if not fields.startswith('#'):
			raise RuntimeError("Error in sequence file format.")
		fields = fields[1:].split('\t')
		for s in fields:
			if ':' in s:
				fIdx, content = s.split(':', 1)
				if fIdx in fldDict:
					fieldContentDict[fldDict[fIdx]] = content
		
		# skip everyting until first line that does not start with '#'
		for line in fl:
			if not line.startswith('#'):
				break
		# the rest is the annotated sequence
		seqData = line + ''.join(fl)
		if not seqData.strip():
			raise RuntimeError("Error in sequence file format.")
		
		annSeq = bns.Sequences.AnnSeq()
		key = annSeq.Paste(seqData)
		if not key:
			bns.Util.Warnings.Add("Invalid sequence file.")
		exper = bns.Database.Experiment(key, seqImportExper)
		if exper.ExperID:
			exper.Delete()
		
		annSeq.SetExp(seqImportExper)
		annSeq.Save()
		
		entry = bns.Database.Entry(key)
		entry.Selection = True
		for field, content in fieldContentDict.items():
			entry.Field(field).Content = content
	
	if importAssembly:
		ctgFn = os.path.join(kodonDbPath, 'contigs', seq + '.ctg')
		if not os.path.isfile(ctgFn):
			continue
		with open(ctgFn) as fl:
			xml = ET.XML(fl.read())
		for xSeq in xml.findall('Sequence'):
			for xTag in xSeq:
				if xTag.tag == 'FileName':
					traceFn = xTag.text
					if not os.path.isfile(traceFn):
						raise RuntimeError("Could not find trace file '{0}'.".format(traceFn))
					traceFc = b64d.EncodeFile(traceFn)
					ET.SubElement(xSeq, 'FileContent').text = traceFc
					xSeq.remove(xTag)
					break
			else:
				raise RuntimeError("Invalid trace file.")
		bns.DST.Xml.ImpContig(key, seqImportExper, ET.tostring(xml))
	
bns.Database.Db.Fields.Save()
