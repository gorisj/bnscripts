---
title: "import_contigs"
date: 2014-03-17T09:42:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---



Imports concatenated contigs from a fasta files into a sequence experiment type in BioNumerics. Contiges are seperated by a pipe '|'. The filename is linked to a specified entry field. 
The script can handle large amounts of fasta files at once, as long as they are located in the same folder. 

Sompe variables in the script should be adjusted before running the script: 
* The folder location should be modified in the script variable 'path'.
* The experiment type name should be modified in the variable 'exper' 
* The entry info field to which the filename will be linked, must be modified in the variable 'fieldid' inside the script.

When you run the script, you will not get a dialog, but the import will start immediately, without progress bar.

If you are importing a lot of whole genome sequences, it is best to leave BN alone (ignore any BN is not responding messages, to distinguish between a true crash and BN just not communicating with Windows, you can open the resource monitor (resmon.exe) and look at the memory usage of bn.exe, if you see this changing regularly, the import is still running).


