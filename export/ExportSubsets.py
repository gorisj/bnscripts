"""ExportSubsets.py

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ! This script is provided "as is" by Applied Maths.              !
// ! You are free to use and modify this script for your own needs. !
// ! Redistribution or reproduction of the script is prohibited.    !
// ! DISCLAIMER:                                                    !
// ! Improper use of scripts may corrupt your database.             !
// ! Running this script is entirely at your own responsibility.    !
// ! Applied Maths accepts no lialibility for any consequences      !
// ! resulting from its use.                                        !
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""


import bns
import os
flName = os.path.join(bns.Database.Db.Info.HomeDir, 'subsets.txt')
fl = open(flName, 'w')
for subset in bns.Database.Db.Subsets:
	fl.write('{0}\t{1}\n'.format(subset, '\t'.join(e.Key for e in subset)))
fl.close()

