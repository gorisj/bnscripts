"""Export SRS statistics to CSV file
"""
#//$autorun;event=PostCreateMainWin
import bns
import Import.importactions as IA
import Import.ImportSequenceReadSets.export
from collections import OrderedDict

Dlg = bns.Windows.XmlDlgBuilder

stats = OrderedDict([
		('Count', '# sequences'), 
		('CountPaired', '# paired sequences'),
		('AvgLen', 'Avg seq length'),
		('StDevLen', 'Std dev seq length'),
		('MinLen', 'Min seq length'),
		('MaxLen', 'Max seq length'),
		
		('AvgQual', 'Avg base quality'),
		('StDevQual', 'Std dev base quality'),
		('MinQual', 'Min base quality'),
		('MaxQual', 'Max base quality'),
		('Q20', 'Q20'),
		('Q25', 'Q25'),
		('Q30', 'Q30'),
		
		('CountBases', '# bases'),
		('CountBasesLeft', '# bases 1st end'),
		('CountBasesRight', '# bases 2nd end'),
		('CountA', '# bases A'),
		('CountC', '# bases C'),
		('CountG', '# bases G'),
		('CountT', '# bases T'),
		('CountOther', '# other bases'),
	
	])
header = ['Entry']
header.extend(stats.values())
header.append('%GC')

def _ExportSrsStats(keys, exper):
	export = [header]
	
	for key in keys:
		experData = bns.Database.Experiment(key, exper)
		if experData.IsPresent():
			srs = experData.LoadOrCreate()
			info = {}
			srs.GetInfo(info)
			
			entry = bns.Database.Entry(key)
			row = [entry.DispName]
			row.extend('{0}'.format(info[s]) for s in stats)
			row.append('{0}'.format(100.0 * (info['CountG'] + info['CountC']) / info['CountBases']))
			export.append(row)
	
	if len(export) == 1:
		raise RuntimeError("No entries with SRS data selected.")
	
	content = '\n'.join('\t'.join(row) for row in export)
	bns.Util.IO.ExportAndView(content)
	
def ExportSrsStats(args):
	
	viewID = bns.Database.Db.ExperimentTypes.ObjectType.GetCurrentViewID()
	expers = bns.Database.Db.ExperimentTypes.ObjectType.GetViewContent(viewID)
	srsExpers = [e for e in expers if bns.Database.ExperimentType(e).Class == 'SRS']
	if not srsExpers:
		raise RuntimeError("No SRS experiment types in current view.")
	elif len(srsExpers) == 1:
		exper = srsExpers[0]
	else:
		dlg = SelectSrsExperDlg(srsExpers)
		if not dlg.Show():
			return
		exper = dlg.exper
	
	viewID = bns.Database.Db.Entries.ObjectType.GetCurrentViewID()
	keys = bns.Database.Db.Entries.ObjectType.GetViewSelectedContent(viewID)
	
	_ExportSrsStats(keys, exper)
	
class SelectSrsExperDlg(Dlg.Dialogs):
	def __init__(self, expers):
		Dlg.Dialogs.__init__(self, 'ExportSrsStatsDlg', remembersettings='none')
		items = [Dlg.ListItem(e.DispName, e.ID) for e in (bns.Database.ExperimentType(_) for _ in expers)]
		values = Dlg.ListValues(items, translate=False)
		self.exper = expers[0]
		self.experCtrl = Dlg.Drop('exper', values, 10, 30, canEdit=False, default=self.exper, hasfocus=True)
		grid = [["SRS type:", self.experCtrl]]
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Export SRS statistics", simple)

	def onOk(self, args):
		self.exper = self.experCtrl.GetValue()
	
class SrsStatsAction(IA.ExportAction):
	id = 'SrsStats'
	name = "Export SRS statistics to CSV file"
	handler_name = 'Import.ImportSequenceReadSets.export.ExportSrsStats'
	cats = ['SRS']
	mods = ['Sequence types']
	descr = "Export SRS statistics to CSV file."

Import.ImportSequenceReadSets.export.ExportSrsStats = ExportSrsStats
actions = IA.ExportActions()
actions.Add(SrsStatsAction())
actions.Store()

if __name__ == '__main__' and __bnsdebug__:
	ExportSrsStats({})
