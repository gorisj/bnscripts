---
title: "export_contigs_to_file"
date: 2017-12-28T15:09:00+01:00
draft: false
author: "Katleen Vranckx"
scriptlanguage: "py"
---


Exports different contigs of a sequence (contigs in BioNumerics are separated by a pipe '|') to a fasta file. Each contig will be assigned a new fasta header.

