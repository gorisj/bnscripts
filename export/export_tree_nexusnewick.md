---
title: "export_tree_nexusnewick"
date: 2012-09-17T08:19:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "bns"
---


Exports a tree from the comparison window to Nexus or Newick format. 
The format to which the script should export can be specified by altering the 'format' variable. Accepted values are 'Nexus' and 'Newick'


