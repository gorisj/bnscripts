#//$menu window=main;popup=Scripts;subpopup=ECDC;name=Export contigs ...; 
import bns
import os

dialog = bns.Windows.XmlDlgBuilder
MessageBox 	= bns.Util.Program.MessageBox

class FileSelectDlg(dialog.Dialogs):
	def __init__(self):
		dialog.Dialogs.__init__(self, 'DialogExportSeq')
		self.folderName = ""
		self.exptype = ""
		#TODO: use view, because sometimes there are more elements selected and you dont see them
		ot = bns.Database.Db.Entries.ObjectType
		viewSelectedEntries = ot.GetViewSelectedContent(ot.GetCurrentViewID())
		self.selectedEntries = [x for x in bns.Database.Db.Selection if x.Key in viewSelectedEntries]
		if len(self.selectedEntries) == 0:
			raise RuntimeError("Warning", "No entry selected.")
		e = bns.Database.Db.ExperimentTypes
		self.experiments = [x for x in e if x.Class=='SEQ']
		self.experiments_names = [exp.DispName for exp in self.experiments]
		n = min(4,len(self.experiments))
		self.fieldExp = dialog.Drop('exps',self.experiments_names,n,30)
		self.fieldOutputFolder = dialog.File('f2', [], 30, "Select a folder",
		                          isdir=True)
		grid = [["Output folder:", self.fieldOutputFolder],
						["Experiment:", self.fieldExp]]
		simple = dialog.SimpleDialog(grid,onOk=self.onOk)
		self.AddSimpleDialog("Export contig sequences of the selected entries", simple)

	def onOk(self, args):
		self.folderName = self.fieldOutputFolder.GetValue()
		self.exptype = self.fieldExp.GetValue()
		

	def export(self):
		for entry in self.selectedEntries:
			seqData = bns.Sequences.SequenceData(entry.Key,self.exptype)
			thisSeq = seqData.LoadSequence().split('|')
			fname = os.path.join(self.folderName,entry.Key+'_'+self.exptype+'.fasta')
			with open(fname,'w') as f:
				i = 1
				for s in thisSeq:
					f.write(">%s_%s_%s\n" % (i,entry.Key,self.exptype))
					f.write("%s\n" % (s))
					i+=1
	
dlg = FileSelectDlg()
if dlg.Show():
	dlg.export()





