---
title: "ExportAllelesToFastaGeneral"
date: 2018-10-03T16:09:00+01:00
draft: false
author: "Unknown"
scriptlanguage: "py"
---


Export alleles for aspect of wgMLST to a fasta file. Exported fasta files can be found in the subfolder 'Export' of the database folder.

The script requires muscle.exe to be present on the computer (a dialog window will show allowing the user to browse to the exact location of the exe).

