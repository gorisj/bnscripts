#//$menu window=main;popup=WGS tools;insertafter=Synchronize;name=Export alleles for aspect of wgMLST...
"""
Script that exports alleles for allele db to fasta file
"""
import os
import time
import bns
import xml.etree.cElementTree as ET
import sys
import subprocess
from collections import OrderedDict
from wgMLST_Client.CommonTools.Settings import StoredSettings
from wgMLST_Client.CuratorDbItf import CuratorDbItf

Dlg = bns.Windows.XmlDlgBuilder
MessageBox = bns.Util.Program.MessageBox
settings = StoredSettings('WGMLST_CLIENT_SCHEMA_SETTINGS',
												'wgMLSTExperTypeName', 'wgMLSTExperTypeID',
												'qualityExperTypeName', 'qualityExperTypeID')		


wgMLSTId = settings.wgMLSTExperTypeID
PROCESSEDDIRECTORY_NAME = bns.Database.Db.Info.SourceFilesDir

#MusclePath = "C:\Program Files\Muscle\muscle.exe"

def GetCharacterValues(key,exp,locusList):
	alleles = []
	chrSet = bns.Characters.CharSet()
	chrSet.Load(key, exp)
	for chrName in locusList:
		idx = chrSet.FindName(chrName)
		if chrSet.GetPresent(idx):
			alleles.append(chrSet.GetVal(idx))
		else:
			alleles.append('-1')
	return alleles	

class SelectMuscleDir(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'MuscleSelectDirDlg')
		
		
		self.flName = "C:\Program Files\Muscle\muscle.exe"

		
		# the file controls
		exts = [Dlg.Extension("exe", "exe file")]
		self.selFile = Dlg.File('muscle', exts, 60, "Select the muscle execution file.", default = self.flName)

		
		# now define the dialog layout
		grid = [["Muscle.exe:", self.selFile]]		       
		simple = Dlg.SimpleDialog(grid, onOk=self.onOk)
		self.AddSimpleDialog("Select the muscle execution file", simple)
	
	def onOk(self, args):
		"""Get the selected items"""
		self.flName = self.selFile.GetValue()




class SelectViewDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'WgsExportAllelesSelectViewDlg')

		settings = StoredSettings('WGMLST_CLIENT_SCHEMA_SETTINGS',
												'wgMLSTExperTypeName', 'wgMLSTExperTypeID',
												'qualityExperTypeName', 'qualityExperTypeID')		

		
		self.wgMLSTId = settings.wgMLSTExperTypeID		
		
		# Get the character views
		aspects = self.GetAspects(self.wgMLSTId)
		
		# member variables to hold the results
		self.selectedAspect = aspects[0]
		updateOptions = ['all loci','none','loci with new allele in selection']
		exportValues = ['aligned','unaligned']
		self.selectedOption = exportValues[0]

		self.updateOption = updateOptions[2]
		self.NrAlgn = 8
		# the list controls
		self.aspectList = Dlg.Drop('dropList', aspects, 10, 30, canEdit=False, default=self.selectedAspect,remembersettings=True)
		
		self.updateList= Dlg.Radio('update', updateOptions, 30, vertical=True,
		                            default=self.updateOption)
		self.inputNrAlgn = Dlg.Input('numberAlign', 5, tp="integer", default=self.NrAlgn) # integer actually means positive, natural number

		self.exportList = Dlg.Radio('alignment', exportValues, 30, vertical=False,default=self.selectedOption)

		# now define the dialog layout
		grid = [["Select aspect:", self.aspectList],
		        ["What do you want to export?:", self.exportList],
						["How many alignments should"],
					[ "run simultaneously?:", self.inputNrAlgn],
					["How do you want to update"], 
					["previously analysed loci?:", self.updateList],]
		simple = Dlg.SimpleDialog(grid, onStart=self.onStart, onOk=self.onOk)
		self.AddSimpleDialog("Export alleles to fasta:", simple)
	

			
	def onStart(self, args):
		"""Set the selected items"""
#		self.aspectList.SetValue(self.selectedAspect)
	
	def onOk(self, args):
		"""Get the selected items"""
		if not bns.Database.Db.Selection:
			MessageBox("Error", "No entries selected.", "exclamation")
			args['result'] = '0'
			return
			
		chrSetType = bns.Characters.CharSetType(self.wgMLSTId)
		chrView = chrSetType.ViewGet(self.aspectList.GetValue())
		locusList = chrView.GetCharacters()
		processedDir = os.path.join(bns.Database.Db.Info.Path,"Export")
		lociDir = os.path.join(bns.Database.Db.Info.SourceFilesDir,"Loci")


		if self.updateList.GetValue()=='all loci':
			
			for name in os.listdir(lociDir):
				for loc in locusList:
					if name.find(loc+"_")>-1 or name.find(loc+".")>-1:
						os.remove(os.path.join(lociDir,name))
						break
			
		if self.updateList.GetValue()=='loci with new allele in selection':
			
			for entry in bns.Database.Db.Selection:
				
				if not bns.Database.Experiment(entry.Key, 'wgMLST').IsPresent():
					continue
					
				alleleIds = GetCharacterValues(entry.Key,'wgMLST', locusList)
				for idx,alleleId in enumerate(alleleIds):
			
					if str(int(alleleId)) == '-1':
						continue
		
					flName = os.path.join(lociDir,'{0}_unaligned.fasta'.format(locusList[idx]))
					if not os.path.exists(flName):
						continue

					with open(flName, 'r') as oFile:
						for line in oFile:
							if line!='':
								if line.startswith('>'):
									alleleIdFasta = line.strip('>\n\r')
									if alleleIdFasta == str(int(alleleId)):
										break
						else:
							oFile.close()
							try:
								os.remove(flName)
								os.remove(os.path.join(lociDir,'{0}.bat'.format(locusList[idx])))
								os.remove(os.path.join(lociDir,'{0}_aligned.fasta'.format(locusList[idx])))
								
							except:
								pass
					
					oFile.close()
		FetchSequencesServerSide(locusList,self.exportList.GetValue(),self.inputNrAlgn.GetValue())
		
		
	def GetAspects(self,exper):
		chrSetType = bns.Characters.CharSetType(exper)
		return chrSetType.ViewGetList()
	

		
def FetchSequencesServerSide(locusList,exportOption, NrAlgn):
	exportDict = {'locusList':locusList, 'exportOption':exportOption}

	def CheckRunningBats(p,bats, NrAlgn, errorList):
		
		finishedList=[]
		for k,v in p.iteritems():
			if v.poll()==0:
				finishedList.append(k)
			else:
				if v.poll() != None:
					errorList.append(k)
		for process in finishedList:
			try: del p[process] 
			except:pass
		for process in errorList:
			try: del p[process] 
			except:pass
		for bat in bats:
			if len(p)<int(NrAlgn):
			
				p[bat] = subprocess.Popen(bat,shell=True)
				bats.remove(bat)
				
		return p,bats,errorList

	def DownloadCreateFasta(curatorDbItf,loc,exportOption,requestXml, commObj,lociDir,bats):

		flName = os.path.join(lociDir,'{0}_unaligned.fasta'.format(loc))
		AlflName = os.path.join(lociDir,'{0}_aligned.fasta'.format(loc))
		
		if not os.path.exists(flName):
			responseXml = curatorDbItf.GetAlleleSequences(requestXml, commObj)
			responsLog = os.path.join(lociDir,'{0}_log.txt'.format(loc))
			with open(responsLog,'w') as logFile:
				logFile.write(responseXml)
				logFile.close()
#		# parse response
			xAlleleSequences = ET.fromstring(responseXml)
			if xAlleleSequences.tag == 'Error':
				error = xAlleleSequences.text
				raise RuntimeError(error)
			alleleSequences = {}
			for xAllele in xAlleleSequences.findall('Allele'):
#					allele = {}
				xOccurrence = xAllele.find('occurrence')
				status = xOccurrence.findtext('status')
				if status=='revoked' or status=='tentative':
					continue
				alleleId =  xAllele.findtext('ID')
				allele =  xAllele.findtext('sequence')

#					allele['firstEncountered'] = xOccurrence.findtext('firstEncountered')
#					allele['status'] = xOccurrence.findtext('status')
				alleleSequences[alleleId] = allele
				# sort the result
				sortedAlleleSequences = OrderedDict(sorted(alleleSequences.iteritems(), key=lambda t: int(t[0])))
					
			with open(flName, 'w') as oFile:
				oFile.write('\n'.join('>{}\n{}'.format(alleleid, seq) for alleleid, seq in sortedAlleleSequences.iteritems()))
			oFile.close()
		
			#create bat if aligned is asked and if no aligned file is present
		if exportOption == 'aligned':
			bat = os.path.join(lociDir,'{0}.bat'.format(loc))
			if not os.path.exists(bat):
				batFile = open(bat, 'w')
				batFile.write('"{0}" -in "{1}" -out "{2}" -maxiters 2'.format(MusclePath,flName,AlflName))
				batFile.close()
				bats.append(bat)
			else:
				if not os.path.exists(AlflName):
					bats.append(bat)
		return bats



	
	
	def DoCalc(dct):
	
		processedDir = os.path.join(bns.Database.Db.Info.Path,"Export")
		if not os.path.exists(processedDir):
			try:os.mkdir(processedDir)
			except:raise RuntimeError("Unsufficient privileges to add subfolder 'Export' in WgsProcessedHomedirectory. Please create this folder manually.")
		lociDir = os.path.join(bns.Database.Db.Info.SourceFilesDir,"Loci")
		if not os.path.exists(lociDir):
			try:os.mkdir(lociDir)
			except:raise RuntimeError("Unsufficient privileges to add subfolder 'Loci' in WgsProcessedHomedirectory. Please create this folder manually.")
		locusToAllelesDict = {}
		curatorDbItf = CuratorDbItf.GetCuratorItf()
		exportOption = exportDict['exportOption']
		locusList = exportDict['locusList']
		commObj = dct['communication']
		bats = []
		p={}
		errorList =[]
		for loc in locusList:
			if not commObj.MustContinue():
				break
			commObj.SetMessage('Fetching alleles for locus: {0}'.format(loc))
			xGetAlleleSequences = ET.Element('GetAlleleSequences')
			ET.SubElement(xGetAlleleSequences, 'Organism').text = curatorDbItf.GetOrganismAbbrev()
			if '_' not in loc:
				raise RuntimeError("Internal error: locus ID '{0}' has an incorrect format".format(self.locusId))
			ET.SubElement(xGetAlleleSequences, 'LocusId').text = loc.split('_')[1] 
			requestXml =  ET.tostring(xGetAlleleSequences)
			
			bats = DownloadCreateFasta(curatorDbItf,loc,exportOption,requestXml, commObj,lociDir,bats)
			#check for running bats and run if less than 4 are running
			p,bats,errorList = CheckRunningBats(p,bats,NrAlgn, errorList)
					
				
			

				
		#after download of all loci, execute remaining bats

		
		while len(p) != 0:
		
			runningBatNames =''
			for batName in p.keys():
				runningBatNames= runningBatNames + batName.rpartition('\\')[2].replace('.bat','') + ', '
			commObj.SetMessage('Aligning alleles ({0} to do) for loci {1}. '.format(len(bats) + len(p),runningBatNames))
			p,bats,errorList = CheckRunningBats(p,bats,NrAlgn,errorList)
			time.sleep(10)
			if not commObj.MustContinue():
				raise RuntimeError("Script has been stopped by user. No fasta files of samples have been exported. {0} loci still need to be aligned.".format(len(bats)+len(p)))

	#check for muscle processes exited with return code different to 0, remove files en initiate new download and execution of bat.
		for error in errorList:
			newErrorList = []
			loc = error.rpartition('\\')[2].replace('.bat','')
			try:
				os.remove(os.path.join(lociDir,'{0}_unaligned.fasta'.format(loc)))
			except:
				pass
			bats = DownloadCreateFasta(curatorDbItf,loc,exportOption,requestXml,commObj,lociDir,bats)
			p,bats,newErrorList = CheckRunningBats(p,bats,NrAlgn,newErrorList)
			while len(p) != 0:

				runningBatNames =''
				for batName in p.keys():
					runningBatNames= runningBatNames + batName.rpartition('\\')[2].replace('.bat','') + ', '
				commObj.SetMessage('Realigning alleles ({0} to do) for loci {1}. '.format(len(bats) + len(p),runningBatNames))
				p,bats,newErrorList = CheckRunningBats(p,bats,NrAlgn,newErrorList)
				time.sleep(10)
				if not commObj.MustContinue():
					raise RuntimeError("Script has been stopped by user. No fasta files of samples have been exported. {0} loci still need to be aligned.".format(len(bats)+len(p)))
			if len(newErrorList)>0:
				errorLoci =""
				for error in newErrorList:
					errorLoci= errorLoci + error.rpartition('\\')[2].replace('.bat','') + ', '
				raise RuntimeError("Not all loci could be aligned after two attempts: {0}. Please retry manually or remove them from the analysis.".format(errorLoci))
		
		commObj.SetMessage('Constructing fasta files for selection.')	
	
	

		#construct locus dicts from correct fasta file
		for loc in locusList:
			alleleSequences = {}
			flName = os.path.join(lociDir,'{0}_{1}.fasta'.format(loc,exportOption))

			with open(flName, 'r') as oFile:
				for line in oFile:
					sequence=""
					if line!='':
						if line.startswith('>'):
							alleleId = line.strip('>\n\r')
							alleleSequences[alleleId] = ""
						else:
							sequence = line.strip('\n\r')
							alleleSequences[alleleId] = alleleSequences[alleleId] + sequence
			oFile.close()

			sortedAlleleSequences = OrderedDict(sorted(alleleSequences.iteritems(), key=lambda t: int(t[0])))
			locusToAllelesDict[loc] = sortedAlleleSequences


		

	
				
		exportDict['locusWAlleles'] = locusToAllelesDict
	
	def DoRefresh(dct):
	
		processedDir = os.path.join(bns.Database.Db.Info.Path,"Export")
		locusWAlleles = exportDict['locusWAlleles']
		locusList = exportDict['locusList']
		exportOption = exportDict['exportOption']
		lociDir = os.path.join(bns.Database.Db.Info.SourceFilesDir,"Loci")
		locusList = exportDict['locusList']

	
		for entry in bns.Database.Db.Selection:
			#get field content
			
			if not bns.Database.Experiment(entry.Key, wgMLSTId).IsPresent():
				continue
			alleleIds = GetCharacterValues(entry.Key,wgMLSTId, locusList)
			fastaDict = OrderedDict()
			for idx,alleleId in enumerate(alleleIds):
				#get the reference allele

				referenceAllele = ''
				if exportOption == 'aligned':

					for key,value in locusWAlleles[locusList[idx]].iteritems():
						if value != '':
							referenceAllele = value
							break
					
				if str(int(alleleId)) == '-1':
					if exportOption == 'unaligned':
						continue
					fastasequence = ['-'] * len(referenceAllele)
					
					fastaDict[locusList[idx]] =''.join(fastasequence)
					continue
					
				
				fastaHeader = '{0}_{1}'.format(locusList[idx],str(int(alleleId)))
				
				try:
					fastaSequence = locusWAlleles[locusList[idx]][str(int(alleleId))]
				except:
					if exportOption == 'aligned':
						fastasequence = ['-'] * len(referenceAllele)
						fastaDict[locusList[idx]] =''.join(fastasequence)
						continue
					else:
						continue

#				if exportOption == 'aligned':
#					seqs = [fastaSequence,referenceAllele]
#					settings = '<Sett AlignAlgorithm="NW" PenaltyOpen="100" PenaltyExtend="5"/>'
#					bns.Sequences.SequenceTools.Align(seqs,settings)
#					#check in which gaps are introduced
#					refAllele = seqs[1]
#					fastaSequence = seqs[0]
#					if refAllele.find('-') != -1:
#						fastaSequence = list(fastaSequence)
#						indices = [i for i, a in reversed(list(enumerate(list(refAllele)))) if a == '-']
#						for index in indices:
#							fastaSequence.pop(index)
#						fastaSequence = ''.join(fastaSequence)
				fastaDict[fastaHeader] = fastaSequence
			#write away the file

			filePath = os.path.join(processedDir,'{0}.fasta'.format(entry.Key))
			ofile = open(filePath,'w')
			for key,value in fastaDict.iteritems():
				ofile.write('>{0}\n{1}\n'.format(key,value))
			ofile.close()
		MessageBox("Success", "All fasta files have been created and can be found in the subfolder'Export' of the databasefolder.", "information")
				
	bns.Windows.BnsWindow(1).StartAsyncCalculation(DoCalc, DoRefresh, async=True)
		
if __name__ == '__main__':
	dlg = SelectMuscleDir()
	if dlg.Show():
		MusclePath = dlg.flName
		while not os.path.exists(MusclePath):
				MessageBox("Error", "No execution file of muscle found, please select a different location.", "exclamation")
				dlg.Show()
				MusclePath = dlg.flName				
		pass
	
	dlg = SelectViewDlg()
	if dlg.Show():
		pass
