"""
Author: Hannes pouseele
Modified by Margo Diricks
Refactored by Jan Deneweth

This script was written to determine the sensitivity and specificity of resistance predictions by the TBseq plugin.
Don't forget to select entries.

Remarks:
-	The character experiment with pDST results should have two information fields: 'Antibiotic class' and 'Antibiotic line'
	(Set by variables charFieldIdClass and charFieldIdLine)
-	For phenotypic result of fluoroquinolones --> results of Ofloxacin, Ciprofloxacin, Levofloxacin and moxifloxacin are
	summarized into one value (minimum of all results --> if strain was resistant to one of the FQs, it is considered resistant to FQ)
-	If a call is unknown (-3), it is regarded as susceptible (1)
"""

import math
import bns
import os
import datetime
import numpy as np
from collections import defaultdict

outputFolder = bns.Database.Db.Info.Path  # Folder were MTBC_genotyping_assessment.txt will be saved (Preset: inside the DB folder)
Dlg = bns.Windows.XmlDlgBuilder
Exp_types = bns.Database.Db.ExperimentTypes

charFieldIdClass = 'Antibiotic class'  # Field name in the character experiment 'phenotypic resistance'; values present in this field are e.g. Amikacin, Capromycin,...
charFieldIdLine = 'Antibiotic line'  # Field name in the character experiment 'phenotypic resistance'; values present in this field are SLD or FLD (second line and first line drug)

# Set names for resistance statuses
# (assuming equal encoding of resistance status in phenotypic resistance character experiment mapping)
RES_STATUS_SUSCEPTIBLE = 1.0
RES_STATUS_NOTCALLED = 0.0  # = entry didn't have phenotypic resistance data for a certain antibiotic in the phenotype experiment
RES_STATUS_MISSING = -1.0  # e.g. if resistance gene is not present in genome
RES_STATUS_QCFAILED = -2.0
RES_STATUS_UNKNOWN = -3.0 # This relates to mutations found at genomic positions known to be related to resistance but which are not included in the resistance database.
#RES_STATUS_REDUCEDSUSCEPTIBILITY = -4.0 #not used for TB
RES_STATUS_RESISTANT = -5.0


# -- DEFINE UTILITY FUNCTIONS --

def Now():
	return datetime.datetime.now().strftime('%c')


def GetCharNames(experType):  # general for all entries
	cst = bns.Characters.CharSetType(experType.Name)  # CharSetType encapsulates a character experiment --> cst = an object which responds to different functions e.g. GetChar (returns name of a character), addChar or RenChar (renames a character), GetCount returns the number of characters inside experiment
	return [cst.GetChar(i) for i in xrange(cst.GetCount())]  # =list of character names from the character experiment


def CheckExper(entry, experType):
	return bns.Database.Experiment(entry, experType).IsPresent()


def GetCharVals(entry, experType):
	cs = bns.Database.Experiment(entry, experType).LoadOrCreate()  # cs = CharSet --> encapsulates the data within a character experiment --> specific for each entry!
	vals = []
	cs.GetData(vals)  # GetData returns the values of the characters and their presence/absence state
	return vals  # list with values of character experiment (e.g. [0, 1, 2,...])


def IsValidCall(call):
	return call in (RES_STATUS_SUSCEPTIBLE, RES_STATUS_RESISTANT, RES_STATUS_UNKNOWN)  # check if call voor een bepaalde antibiotica (vb. 1) een valid getal heeft, maw, een getal dat overeenkomt met susceptible, resistant or unknown (--> can be considered susceptible) --> maw alles behalve missing

	
def CalculateCI95(parameter, cases):
	CI = (math.sqrt(((1 - parameter) * parameter) / cases)) * 1.96
	return CI


def link_filter_zeros(integers, linking_object):
	"""
	Link integers in the list to their values in the linking dict,
	but filter all occurences with linked value equalling zero.

	:param iterable integers: iterable of integers
	:param indexable linking_object: object linking integers to values
	:return list: filtered list with linked values
	"""
	return list(linking_object[x] for x in integers if linking_object[x] != 0)


# -- DEFINE DIALOGS --

class DropListDlg(Dlg.Dialogs):
	def __init__(self):
		Dlg.Dialogs.__init__(self, 'DropListDlg')

		# the values to show in the list control
		exp_list = [exp.DispName for exp in Exp_types]

		# member variables to hold the results
		self.selectedPheno = ""
		self.selectedPrediction = ""

		# the list controls
		self.pheno = Dlg.Drop('pheno', exp_list, 10, 30, canEdit=False)
		self.prediction = Dlg.Drop('prediction', exp_list, 10, 30, canEdit=False)

		# now define the dialog layout
		grid = [
			["Character experiment with pDST results:", self.pheno],
			["Character experiment with in silico prediction results:", self.prediction],
		]
		simple = Dlg.SimpleDialog(grid, onStart=self.onStart, onOk=self.onOk)
		self.AddSimpleDialog("Drop lists", simple)

	def onStart(self, args):
		"""Set the selected items"""
		self.pheno.SetValue(self.selectedPheno or "")
		self.prediction.SetValue(self.selectedPrediction or "")

	def onOk(self, args):
		"""Get the selected items"""
		self.selectedPheno = self.pheno.GetValue()
		self.selectedPrediction = self.prediction.GetValue()


# -- Script Flow: preparatory steps --


# Make sure that entries were selected
selectedEntries = bns.Database.Db.Selection
if len(selectedEntries) == 0:
	raise RuntimeError("No entries selected.")

# Summon the dialog for experiment selection
dlg = DropListDlg()
dlg.Show()

# Get the experiment objects
phenoExper = bns.Database.ExperimentType(dlg.selectedPheno)  # contains all information about phenotypic resistance = object! You can use different functions e.g. phenoExper.name will give you the name of the experiment
predictedExper = bns.Database.ExperimentType(dlg.selectedPrediction)  # contains all information about the predicted resistance (by the BN plugin)

# Get list of characters (antibiotics) in phenotypic and predicted resistance experiment
phenoChars = GetCharNames(phenoExper)
predictedChars = GetCharNames(predictedExper)

# Create a dictionary: predicted chars -> value (E.g. Amikacyn -> 0)
# Register antibiotic classes that can be predicted
classToPredicted = {}
for i, k in enumerate(predictedChars):
	classToPredicted[k] = [i]

# Create various linking dictionaries for phenotypic antibiotic classes
classToPheno = defaultdict(set)  # Holder for summary value of antibiotic classes that contain multiple antibiotics.
classToLine = {}
lineToClass = defaultdict(set)
clsNotInPredicted = list()  # TO CHECK: pretty sure this should be a set; duplicates irrelevant?
ct = bns.Characters.CharType(phenoExper.Name)  # Encapsulates the character information fields from phenotypic resistance experiment
for i, k in enumerate(phenoChars):
	antibiotic_class = ct.FldGet(k, charFieldIdClass)  # antibiotic class (defined at top), corresponding to a character in phenotypic experiment
	line = ct.FldGet(k, charFieldIdLine)
	# Register if the antibiotic can not be predicted (determined earlier)
	if antibiotic_class not in classToPredicted:
		clsNotInPredicted.append(antibiotic_class)
		continue
	# Add antibiotic to phenotypic summary dict
	classToPheno[antibiotic_class].add(i)
	# Register line to the antibiotic class
	if antibiotic_class not in classToLine:
		classToLine[antibiotic_class] = line
	#
	if line:
		# Add antibiotic to line summary dict
		lineToClass[line].add(antibiotic_class)
	# Check whether every class has a unique value (a class can only belong to one line)
	assert classToLine[antibiotic_class] == line, "Mismatch in the chain abx name -> class -> line for abx {}!".format(k)

# Derive information from linking dictionaries
classes_Pheno = classToLine.keys()  # phenotypic resistance experiment
classes_Predicted = classToPredicted.keys()  # predicted resistance experiment
lines = lineToClass.keys()


# -- Script flow: Entry Processing --

# Initialise variables
entryCnt = 0
entryCntPhenoMissing = 0
entryCntPredictedMissing = 0
entryCntNoneMissing = 0

clsCntTP = defaultdict(int)
clsCntFP = defaultdict(int)
clsCntTN = defaultdict(int)
clsCntFN = defaultdict(int)

lineCntTP = defaultdict(int)
lineCntFP = defaultdict(int)
lineCntTN = defaultdict(int)
lineCntFN = defaultdict(int)

clsPhenoProfile = {}
clsPredictedProfile = {}
linePhenoProfile = {}  # e.g. {'SLD': -5.0, 'FLD': 1.0}
linePredictedProfile = {}  # e.g. {'Fluoroquinolone': 5.0, 'Rifampicin': 3.0, 'Isoniazid': 5.0, 'Cefpirome': 5.0, 'Amikacin': 3.0, 'Pyrazinamide': 5.0, 'Ethionamide': -1.0, 'Ethambutol': 5.0, 'Linezolid': 5.0, 'Para-aminosalicylic acid': 3.0, 'Streptomycin': -1.0, 'Kanamycin A': 3.0}

sensClass = {}
CIsensClass = {}
specClass = {}
CIspecClass = {}
sensLine = {}
CIsensLine = {}
specLine = {}
CIspecLine = {}

for entry in selectedEntries:

	# Adjust counts
	entryCnt += 1
	if not CheckExper(entry, phenoExper):
		entryCntPhenoMissing += 1
		continue
	elif not CheckExper(entry, predictedExper):
		entryCntPredictedMissing += 1
		continue
	else:
		entryCntNoneMissing += 1  # If entry has data for pheno and predicted, none of the two is missing --> nonemissing count +1


	# Link AB classes to phenotypic resistances
	rawPhenoProfile = GetCharVals(entry, phenoExper)  # values associated with antibiotics
	for cls, idx in classToPheno.iteritems():
		nonzeros = link_filter_zeros(idx, rawPhenoProfile)
		clsPhenoProfile[cls] = min(nonzeros) if nonzeros else 0

	# Link lines to phenotypic resistances
	for line, idx in lineToClass.iteritems():
		nonzeros = link_filter_zeros(idx, clsPhenoProfile)
		linePhenoProfile[line] = min(nonzeros) if nonzeros else 0

	# Evaluate predicted resistance
	rawPredictedProfile = GetCharVals(entry, predictedExper)
	for cls, idx in classToPredicted.iteritems():
		nonzeros = link_filter_zeros(idx, rawPredictedProfile)
		susceptible_value = min(nonzeros) if nonzeros else 0
		if susceptible_value == RES_STATUS_UNKNOWN:  # if a call is unknown, it should be regarded as susceptible
			susceptible_value = RES_STATUS_SUSCEPTIBLE
		clsPredictedProfile[cls] = susceptible_value

	# Link lines to predicted resistances
	for line, idx in lineToClass.iteritems():
		nonzeros = link_filter_zeros(idx, clsPredictedProfile)
		linePredictedProfile[line] = min(nonzeros) if nonzeros else 0

	# sample-level comparison

	# AB class level comparison per entry
	for cls in classes_Pheno:

		if not IsValidCall(clsPhenoProfile[cls]):
			continue  # continue with the next iteration of the loop (in other words, skip calculation of tp, fp,...)
		elif not IsValidCall(clsPredictedProfile[cls]):
			continue

		tp = (clsPhenoProfile[cls] < 0 and clsPredictedProfile[cls] < 0)  # numbering changed from 1 to -5 for resistance so this needs to be changed?
		fp = (clsPhenoProfile[cls] > 0 and clsPredictedProfile[cls] < 0)
		tn = (clsPhenoProfile[cls] > 0 and clsPredictedProfile[cls] > 0)
		fn = (clsPhenoProfile[cls] < 0 and clsPredictedProfile[cls] > 0)

		clsCntTP[cls] += tp  # = 0 if there are no TP
		clsCntFP[cls] += fp
		clsCntFN[cls] += fn
		clsCntTN[cls] += tn

	# line level comparison per entry
	for line in lines:
		if not IsValidCall(linePhenoProfile[line]): continue
		if not IsValidCall(linePredictedProfile[line]): continue

		tp = (linePhenoProfile[line] < 0 and linePredictedProfile[line] < 0)  # numbering changed from 1 to -5 for resistance so this was changed?
		fp = (linePhenoProfile[line] > 0 and linePredictedProfile[line] < 0)
		tn = (linePhenoProfile[line] > 0 and linePredictedProfile[line] > 0)
		fn = (linePhenoProfile[line] < 0 and linePredictedProfile[line] > 0)

		lineCntTP[line] += tp
		lineCntFP[line] += fp
		lineCntFN[line] += fn
		lineCntTN[line] += tn


# Calculation of sensitivity, specificity and 95% Confidence interval
for cls in classes_Pheno:

	if cls in clsCntTN and clsCntTN[cls] != 0:
		specClass[cls] = float(clsCntTN[cls]) / (clsCntTN[cls] + clsCntFP[cls])
		CIspecClass[cls] = CalculateCI95(specClass[cls], (clsCntTN[cls] + clsCntFP[cls]))
	else:
		specClass[cls] = 0
		CIspecClass[cls] = 0
	if cls in clsCntTP and clsCntTP[cls] != 0:  # To avoid problems with division by zero error
		sensClass[cls] = float(clsCntTP[cls]) / (clsCntTP[cls] + clsCntFN[cls])
		CIsensClass[cls] = CalculateCI95(sensClass[cls], (clsCntTP[cls] + clsCntFN[cls]))
	else:
		sensClass[cls] = 0
		CIsensClass[cls] = 0
for line in lines:
	if line in lineCntTN and lineCntTN[line] != 0:
		specLine[line] = float(lineCntTN[line]) / (lineCntTN[line] + lineCntFP[line])
		CIspecLine[line] = CalculateCI95(specLine[line], (lineCntTN[line] + lineCntFP[line]))
	else:
		specLine[line] = 0
		CIspecLine[line] = 0
	if line in lineCntTP and lineCntTP[line] != 0:  # To avoid problems with division by zero error
		sensLine[line] = float(lineCntTP[line]) / (lineCntTP[line] + lineCntFN[line])
		CIsensLine[line] = CalculateCI95(sensLine[line], (lineCntTP[line] + lineCntFN[line]))
	else:
		sensLine[line] = 0
		CIsensLine[line] = 0


# -- Script flow: output results --

# Set variables
Year = datetime.datetime.now().strftime('%Y')
Month = datetime.datetime.now().strftime('%m')
Day = datetime.datetime.now().strftime('%d')
outputFlName = os.path.join(outputFolder, '{}-{}-{} {}'.format(Year, Month, Day, 'MTBC_genotyping_assessment.txt'))
generic_legend = '''

Legend:
TP: phenotypically and genotypically (predicted) resistant
FP: phenotypically susceptible, genotypically (predicted) resistant
TN: phenotypically and genotypically (predicted) susceptible
FN: phenotypically resistant, genotypically (predicted) susceptible
Sensitivity = (TP/TP+FN)*100 
The Sensitivity (also called true positive rate, recall or probability of detection) measures the proportion of positives that are correctly identified as such 
Specificity = (TN/TN+FP)*100 
The specificity (also called the true negative rate) measures the proportion of negatives that are correctly identified as such 
CI: 95% confidence interval
'''

# Write to file
with open(outputFlName, 'w') as oFile:

	# Write generic info
	oFile.write('{}\t{}\n'.format('Date', Now()))
	oFile.write('\n')
	oFile.write('{}\t{}\n'.format('Number of entries', entryCnt))
	if clsNotInPredicted:
		oFile.write('{}\t{}\n'.format('Antibiotic classes in pDST character experiment for which no resistance prediction can be made with the current resistance knowledge base:', clsNotInPredicted))
	oFile.write('{}\t{}\n'.format('#Entries without pDST data:', entryCntPhenoMissing))
	oFile.write('{}\t{}\n'.format('#Entries without predicted resistance data:', entryCntPredictedMissing))
	oFile.write('{}\t{}\n'.format('#Entries with both pDST and predicted resistance data:', entryCntNoneMissing))
	oFile.write('\n')

	# Write antibiotic class results
	oFile.write('\n\n')
	oFile.write('Comparison of pDST results and in silico resistance predictions \n\n')
	oFile.write('Class matches\n')
	oFile.write('Class\t\tTP\tFP\tTN\tFN\tSensitivity (%)\tCI\tSpecificity (%)\tCI\n')
	for cls in classes_Pheno:  # phenotypic antibiotic classes (all from phenotypic resistance character experiment), for which also predictions can be made in silico with the given resistance knowledge base
		if cls not in clsCntTP:  # remark: keys of clsCntTP/FP/FN are the same (if there was no TP, but 1 FP/FN or TN, the class will be present in clsCntTP met value will be zero)!
			oFile.write('{}\t No data available\n'.format(cls))
		else:
			oFile.write('{}\t\t{}\t{}\t{}\t{}\t{}\t({}, {})\t{}\t({}, {})\n'.format(
					cls,
					clsCntTP[cls],
					clsCntFP[cls],
					clsCntTN[cls],
					clsCntFN[cls],
					('%.1f' % (100.0 * sensClass[cls])),
					('%.1f' % (np.clip((100.0 * (sensClass[cls] - CIsensClass[cls])), 0, 100))),
					('%.1f' % (np.clip((100.0 * (sensClass[cls] + CIsensClass[cls])), 0, 100))),
					('%.1f' % (100.0 * specClass[cls])),
					('%.1f' % (np.clip((100.0 * (specClass[cls] - CIspecClass[cls])), 0, 100))),
					('%.1f' % (np.clip((100.0 * (specClass[cls] + CIspecClass[cls])), 0, 100)))
				)
			)

	# Write line results
	oFile.write('\n\n')
	oFile.write('Line matches\n')
	oFile.write('Line\tTP\tFP\tTN\tFN\tSensitivity (%)\tCI\tSpecificity (%)\tCI\n')
	for line in lines:
		if line not in lineCntTP:
			oFile.write('{}\tNo data available\n'.format(line))
		else:
			oFile.write('{}\t{}\t{}\t{}\t{}\t{}\t({}, {})\t{}\t({}, {})\n'.format(
					line,
					lineCntTP[line],
					lineCntFP[line],
					lineCntTN[line],
					lineCntFN[line],
					('%.1f' % (100.0 * sensLine[line])),
					('%.1f' % (np.clip((100.0 * (sensLine[line] - CIsensLine[line])), 0, 100))),
					('%.1f' % (np.clip((100.0 * (sensLine[line] + CIsensLine[line])), 0, 100))),
					('%.1f' % (100.0 * specLine[line])),
					('%.1f' % (np.clip((100.0 * (specLine[line] - CIspecLine[line])), 0, 100))),
					('%.1f' % (np.clip((100.0 * (specLine[line] + CIspecLine[line])), 0, 100)))
				)
			)

	oFile.write(generic_legend)
