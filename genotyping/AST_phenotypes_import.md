---
title: "AST_phenotypes_import"
date: 2019-06-20T17:06:00+01:00
draft: false
author: "Jan Deneweth"
scriptlanguage: "py"
---


This scripts imports AST phenotypes from the NCBI National Database of Antibiotic Resistant Organisms (NDARO; https://www.ncbi.nlm.nih.gov/pathogens/antimicrobial-resistance/) to a character experiment. Optionally, one can add a mapping of the single antibiotics to their respective classes.
From the NDARO catalog of genomes with AMR genotypes or phenotypes one can download genomes from strains for which, amongst other things, the AST phenotypes have been determined. 

**Get AST phenotypes from NDARO:**
* Go to https://www.ncbi.nlm.nih.gov/pathogens/isolates#/search/
* Select your organism
* Click on the 'filters' button and select 'has AST phenotypes' under 'Property'
* In the table header, then click on 'Choose columns' and add 'Run' and 'AST phenotypes' to the selected columns.
* Click 'OK'
* Click the 'download' button to download the table in csv format, which will serve as input for the script. 

**Import the data in BioNumerics**
* Import the metadata from the downloaded csv file in BioNumerics through the regular Import plugin (except for the AST phenotypes column)
* Optionally, create a mapping file which maps the different antibiotics to their respective classes (the mapping file should contain columns 'subclass' and 'class')
* Run the script in BN
* Browse to the NDARO csv file and mapping file
* Select output experiment, the AST phenotypes field in the csv file, and the NDARO database fields that link metadata and AST phenotypes. 

**Mapping file**
The mapping file allows you to map individual antibiotics to their respective classes, as the genotyping plugins typically use classes. 
If multiple antibiotics are mapped to the same class, a worst case scenario is presumed, i.e. if resistance to one antibiotic in the class is present, resistance for the whole class is presumed. 
The mapping files should contain columns with headers 'subclass' and 'class'.

Note: this script can also be found in TFS: http://prod-tfs2015.applied-maths.local:8080/tfs/Default%20Collection/PythonCode/_versionControl?path=%24%2FPythonCode%2FDEV%2FScriptsInternal%2Fgenotyping%2FAST_phenotypes_import.py&_a=contents


