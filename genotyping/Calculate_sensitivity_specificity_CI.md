---
title: "Calculate_sensitivity_specificity_CI"
date: 2019-01-16T15:06:00+01:00
draft: false
author: "Hannes Pouseele"
scriptlanguage: "py"
---


This script was written to determine the sensitivity and specificity of resistance predictions by the TBseq plugin, but can be used with other genotyping plugins as well.
Don't forget to select entries.

Remarks:
* The character experiment with pDST results should have two information fields: 'Antibiotic class' and 'Antibiotic line' (Set by variables charFieldIdClass and charFieldIdLine)
* For phenotypic result of fluoroquinolones --> results of Ofloxacin, Ciprofloxacin, Levofloxacin and moxifloxacin are summarized into one value (minimum of all results --> if strain was resistant to one of the FQs, it is considered resistant to FQ)
* If a call is unknown (-3), it is regarded as susceptible (1)

