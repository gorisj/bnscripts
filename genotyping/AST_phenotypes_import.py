#//$menu window=main;popup=CustomScripts;insertpopupafter=Scripts;name=Import AST CSV...;
"""
Import AST Phenotypes to a character set from a CSV file

Requestor: Dieter De Coninck
Author: Jan Deneweth
Date: 2019-06-20
"""

import csv

import bns
Dlg = bns.Windows.XmlDlgBuilder


# -- Public Methods --

def parse_ast(ast_str, mapping_dict=None):
	"""
	Parse an AST phenotype string to dictionary
	:param str ast_str: string of AST types
	:param dict mapping_dict: a mapping of subclasses to classes, highest value of subclasses is taken per class (optional)
	:return dict: AST phenotype presence dictionary
	"""
	# Convert the raw data
	ast_str_list = ast_str.split(',')
	ast_raw_list = [ast_raw.split('=') for ast_raw in ast_str_list]
	val_dict = {'S': 0, 'R': 1, 'I': 0.5}
	ast_dict = {
		k: val_dict.get(v, -1)  # 1 for resistant, 0 for susceptible, -1 for others
		for k, v
		in ast_raw_list
	}
	# Remap the classes if applicable
	if mapping_dict:
		remapped_dict = dict()
		for key, val in ast_dict.items():
			group = mapping_dict.get(key, key)
			remapped_dict[group] = max(val, remapped_dict.get(group, -1))
		ast_dict = remapped_dict
	return ast_dict


def dct_to_charexp(dct, entry_key, exper_name):
	"""
	Save a dictionary to a character experiment.
	No prior cleaning of the experiment is performed, and existing values are overwritten.
	:param dict dct: dictionary with
	:param str entry_key: key of the entry
	:param str exper_name: name of the character experiment
	"""
	exper = bns.Database.Experiment(key=entry_key, exper=exper_name).LoadOrCreate()
	assert isinstance(exper, bns.Characters.CharSet)
	for key, value in dct.items():
		char_nr = exper.FindName(key)
		if char_nr < 0:
			char_nr = bns.Characters.CharSetType(exper_name).AddChar(name=key, maxval=1.0)
		exper.SetVal(char_nr, value)
	exper.Save()
	

def entry_by_field_data(fieldname, value):
	"""
	Find an entry by the value of its field.
	Only the first found is returned.
	:param str fieldname: name of the entry infofield
	:param str value: the value the field should equal
	:return bns.Database.Entry, None: the found entry instance, or None if none found
	Note: fieldname 'Key' is interpreted as the entry key.
	"""
	for entry in bns.Database.Db.Entries:
		if _field_get(entry=entry, fieldname=fieldname) == value:
			return entry
	return None
	
	
# -- Internal Methods --

def _field_get(entry, fieldname):
	"""
	Get the value of a field for an entry.
	:param bns.Database.Entry entry: the entry instance
	:param str fieldname: name of the entry infofield
	:return str: the field value
	Note: fieldname 'Key' is interpreted as the entry key.
	"""
	if fieldname == 'Key':
		return entry.Key
	else:
		return entry.Field(fieldname=fieldname).Content


# -- Dialogs --


class ASTImportDlg(Dlg.SimpleDialog):
	
	def __init__(self):
		"""A dialog for requesting and AST import specifications"""
		self.file_data = []
		self.file_fields = []
		self.mapping_dict = None
		
		extensions = [Dlg.Extension('csv', "CSV file"), Dlg.Extension('*', "Any file")]
		extensions[0].AddExtraExtension('tsv')
		extensions[0].AddExtraExtension('txt')

		db_fields = [Dlg.ListItem('Key'), ] + [Dlg.ListItem(fld.DispName) for fld in bns.Database.Db.Fields]
		db_expers = [Dlg.ListItem(exper.Name) for exper in bns.Database.Db.ExperimentTypes if exper.Class == 'CHR']

		# Input data: file, linking field and AST field
		self._file_browse = Dlg.File(Id='ast_file', extensions=extensions, charWidth=30, dlgTitle='Select AST CSV file', OnChange=self._file_change, remembersettings=False)
		self._mapping_browse = Dlg.File(Id='mapping_file', extensions=extensions, charWidth=30, dlgTitle='Select a mapping file', OnChange=self._mapping_change, remembersettings=False)
		self._datalinkfield_drop = Dlg.Drop(Id='datalinkfield_drop', listvalues=[], itemsvisible=6, charWidth=30)
		self._astfield_drop = Dlg.Drop(Id='astfield_drop', listvalues=[], itemsvisible=6, charWidth=30)
		# Database: linking field and character experiment
		self._dblinkfield_drop = Dlg.Drop(Id='dblinkfield_drop', listvalues=db_fields, itemsvisible=6, charWidth=30)
		self._charexp_drop = Dlg.Drop(Id='charexp_drop', listvalues=db_expers, itemsvisible=6, charWidth=30)
		
		grid = [
			['AST CSV File: ', self._file_browse],
			['AST field (file): ', self._astfield_drop],
			['Linking field (file): ', self._datalinkfield_drop],
			['Linking field (db): ', self._dblinkfield_drop],
			['Data experiment (db): ', self._charexp_drop],
			['Mapping file (optional): ', self._mapping_browse],  # To map AST names to others
		]
		
		# Initialise through parent
		super(ASTImportDlg, self).__init__(grid=grid, onOk=self._on_ok)
		
	def _file_change(self, args):
		"""Read the data file and refresh the file-based contents"""
		# Read all data from the file
		self.file_data = []
		self.file_fields = []
		try:
			with open(self._file_browse.GetValue(), 'r') as fh:
				dialect = csv.Sniffer().sniff(sample=fh.readline(), delimiters=('\t', ',', ';'))
				fh.seek(0)
				dictreader = csv.DictReader(f=fh, dialect=dialect)
				self.file_fields = dictreader.fieldnames
				for row in dictreader:
					self.file_data.append(row)
		except Exception as e:
			bns.Util.Program.MessageBox('AST file error', 'Could not read data from file: \r\n'+str(e), 'exclamation')
		# Set the file-associated drop list items
		file_fields = [Dlg.ListItem(field) for field in self.file_fields]
		self._datalinkfield_drop.SetItems(items=file_fields)
		self._astfield_drop.SetItems(items=file_fields)
		
	def _mapping_change(self, args):
		"""Read the mapping file"""
		# Read all data from the file
		self.mapping_dict = None
		mapping_data = []
		mapping_fields = []
		try:
			with open(self._mapping_browse.GetValue(), 'r') as fh:
				dialect = csv.Sniffer().sniff(sample=fh.readline(), delimiters=('\t', ',', ';'))
				fh.seek(0)
				dictreader = csv.DictReader(f=fh, dialect=dialect)
				mapping_fields = dictreader.fieldnames
				for row in dictreader:
					mapping_data.append(row)
		except Exception as e:
			bns.Util.Program.MessageBox('Mapping file error', 'Could not read mapping from file: \r\n'+str(e), 'exclamation')
		# Check the input
		if 'class' not in mapping_fields or 'subclass' not in mapping_fields:
			raise ValueError("Mapping file must have columns 'subclass' and 'class', now has: {0}".format(','.join(mapping_fields)))
		# Convert to mapping dictionary
		self.mapping_dict = dict()
		for row in mapping_data:
			self.mapping_dict[row['subclass']] = row['class']
		
	def _on_ok(self, _):
		"""Upon OK, import data"""
		# Get inputs
		dblinkfield = self._dblinkfield_drop.GetValue()
		datalinkfield = self._datalinkfield_drop.GetValue()
		astfield = self._astfield_drop.GetValue()
		expername = self._charexp_drop.GetValue()
		assert dblinkfield and datalinkfield and astfield and expername, "All '(file)' and '(db)' inputs must be present"
		shared_data = {'link_hit': 0}
		
		def calc(args):
			comm = args['communication']
			# Import data row by row
			for index, row in enumerate(self.file_data):
				link_val = row[datalinkfield]
				entry = entry_by_field_data(fieldname=dblinkfield, value=link_val)
				# Import data for linked entries
				if entry is not None:
					comm.SetMessage("Import AST {0} to entry: {1}".format(index+1, entry.Key))
					ast_val = row[astfield]
					ast_dct = parse_ast(ast_str=ast_val, mapping_dict=self.mapping_dict)
					dct_to_charexp(dct=ast_dct, entry_key=entry.Key, exper_name=expername)
					shared_data['link_hit'] += 1
		
		def refresh(args):
			# Report success rate to user
			bns.Util.Program.MessageBox("Data imported", "Imported data, {0}/{1} could be linked".format(shared_data['link_hit'], len(self.file_data)), "info")
		
		bns.Windows.BnsWindow(1).StartAsyncCalculation(calc, refresh, True)


# -- Run as Script --

def main():
	dlg = Dlg.Dialogs(Id='AST_import_dlg')
	dlg.AddSimpleDialog(title='AST_import_dlg_simple', simple=ASTImportDlg())
	dlg.Show()
	

if __name__ == '__main__':
	main()


#
#
# END OF FILE
